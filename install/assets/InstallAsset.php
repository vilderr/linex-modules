<?php

namespace linex\modules\install\assets;

use yii\web\AssetBundle;

/**
 * Class InstallAsset
 * @package linex\modules\install\assets
 */
class InstallAsset extends AssetBundle
{
    public $sourcePath = '@vendor/linex/modules/install/assets/dist';

    public $css = [
        'css/install.css',
    ];

    public $js = [
        'js/install.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}