var Install = function () {

};

Install.options = {
    langSelector: '#select-language'
};

Install.language = {
    set: function (selector) {
        $(document).on('change', selector, function (e) {
            var value = e.target.value;

            $.ajax({
                'url': '/install/set-language',
                'type': 'GET',
                'cache': false,
                'data': {
                    'lang': value
                }
            });
        });
    }
};

$(function () {
    "use strict";

    var opt = Install.options;

    Install.language.set(opt.langSelector);
});