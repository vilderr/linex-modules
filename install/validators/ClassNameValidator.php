<?php

namespace linex\modules\install\validators;

use Yii;
use yii\validators\Validator;
use linex\modules\install\Module as InstallModule;

/**
 * Class ClassNameValidator
 * @package linex\modules\install\validators
 */
class ClassNameValidator extends Validator
{
    /**
     * @inheritdoc
     * @return null|array
     */
    public function validateValue($value)
    {
        if (class_exists($value) === false) {
            return [
                InstallModule::t('Unable to find specified class.'),
                [],
            ];
        } else {
            return null;
        }
    }
}