<?php

namespace linex\modules\install\controllers;

use linex\modules\install\helpers\InstallHelper;
use linex\modules\install\models\Town;
use Yii;
use yii\base\DynamicModel;
use yii\web\Controller;
use yii\web\Response;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;
use yii\helpers\Url;

use linex\modules\main\Module as MainModule;
use linex\modules\install\Module as InstallModule;
use linex\modules\install\models\Cache;
use linex\modules\install\models\Environment;
use linex\modules\install\models\Requirements;
use linex\modules\install\models\Db;
use linex\modules\install\components\ModuleManager;
use linex\modules\install\models\User;

/**
 * Class DefaultController
 * @package linex\modules\install\controllers
 */
class DefaultController extends Controller
{
    /**
     * Параметры текущей сессии для хранения данных
     * введенных пользователем на каждом шаге
     *
     * @var array
     */
    public $session = [];

    public function init()
    {
        parent::init();

        $this->session['Install'] = \Yii::$app->session->get('Install', []);
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'linex\\modules\\install\\actions\\ErrorAction',
            ],
        ];
    }

    private function _setFinished($action)
    {
        \Yii::$app->session->set('Install', ArrayHelper::merge(
            \Yii::$app->session->get('Install'),
            [
                'Finish' => [
                    $action => true,
                ],
            ]
        ));
    }

    /**
     * Начало процедуры установки
     * Выбор языка
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new DynamicModel(['language']);
        $model->addRule(['language'], 'required');
        $model->setAttributes(['language' => ArrayHelper::getValue(Yii::$app, 'language', 'en-US')]);
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->validate()) {
            $lang_config = [
                'language' => $model->language,
            ];

            if ($this->writeLangFile($lang_config)) {
                Yii::$app->session->set('app-language', $model->language);
                $this->_setFinished('index');

                return $this->redirect(Url::toRoute('requirements'));
            } else {

            }
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('select language'),
        ]);

        return $this->render('index', [
            'model' => $model,
            'langs' => MainModule::getLangArray(),
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionRequirements()
    {
        $post = Yii::$app->request->post();
        $model = new Requirements();
        $valid = $model->validate();

        if ($model->load($post) && $model->validate()) {
            $this->_setFinished('requirements');

            return $this->redirect(Url::toRoute('environment'));
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('check requirements'),
        ]);

        return $this->render('requirements', [
            'model' => $model,
            'valid' => $valid,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionEnvironment()
    {
        $post = Yii::$app->request->post();
        $model = new Environment();

        if ($model->load($post) && $model->validate()) {
            $this->_setFinished('environment');

            return $this->redirect(Url::toRoute('cache'));
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('check environment'),
        ]);

        return $this->render('environment', [
            'model' => $model,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCache()
    {
        $post = Yii::$app->request->post();
        $model = new Cache();
        $model->serverName = Yii::$app->request->serverName;
        if (Yii::$app->request->serverPort !== 80) {
            $model->serverPort = Yii::$app->request->serverPort;
        }

        if ($model->load($post) && $model->validate() && $model->createConfig()) {
            $this->_setFinished('cache');

            return $this->redirect(Url::toRoute('db'));
        }

        $cacheClasses = Cache::getCacheClasses();
        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('cache settings'),
        ]);

        return $this->render('cache', [
            'model'        => $model,
            'cacheClasses' => $cacheClasses,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionDb()
    {
        $post = Yii::$app->request->post();
        $model = new Db();
        $config = $model->tempConfig;
        $model->setAttributes($config);

        if ($model->load($post) && $model->validate()) {
            $config = $model->getAttributes();
            $config['connectionOk'] = false;

            Yii::$app->session->set('db-config', $config);

            if ($model->testConnection()) {
                if (isset($_POST['next'])) {
                    if ($model->writeConfig()) {
                        $this->_setFinished('db');

                        return $this->redirect('modules');
                    }
                } else {
                    Yii::$app->session->setFlash('success', InstallModule::t('Success connection!'));
                }
            }
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('db connection'),
        ]);

        return $this->render('db', [
            'model' => $model,
        ]);

    }

    /**
     * @return array|string
     */
    public function actionModules()
    {
        $manager = new ModuleManager();

        if (Yii::$app->request->isAjax) {
            sleep(2);
            $status = 'OK';
            $get = Yii::$app->request->get();
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($manager->install($get['name'])) {
                $message = $manager->message;
            } else {
                $status = 'ERROR';
                $message = $manager->error;
            }

            return [
                'status'  => $status,
                'message' => $message,
            ];
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('activate modules'),
        ]);

        return $this->render('modules', [
            'manager' => $manager,
        ]);
    }

    /**
     * @return string|Response
     */
    public function actionUser()
    {
        $post = Yii::$app->request->post();
        $db = Yii::$app->db;
        $model = new User(User::getConfig());

        if ($model->load($post) && $model->validate()) {
            $model->setConfig();

            if (InstallHelper::createUser($model, $db))
                return $this->redirect('town');
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('create admin user'),
        ]);

        return $this->render('user', [
            'model' => $model,
        ]);
    }

    public function actionTown()
    {
        $post = Yii::$app->request->post();
        $db = Yii::$app->db;

        $model = new Town(Town::getConfig());

        if ($model->load($post) && $model->validate()) {
            $model->setConfig();

            if (InstallHelper::createTown($model, $db)) {
                return $this->redirect('complete');
            }
        }

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('create default town'),
        ]);

        return $this->render('town', [
            'model' => $model,
        ]);
    }

    public function actionComplete()
    {
        $manager = new ModuleManager();
        $status = "success";

        if ($manager->delete('install')) {
            $message = InstallModule::t('The install module was successfully deleted!');
        } else {
            $status = "error";
            $message = $manager->error;
        }

        Yii::$app->session->setFlash($status, $message);

        $this->view->title = InstallModule::t('Install: {step name}', [
            'step name' => $this->getStepLabel('complete'),
        ]);

        return $this->render('complete');
    }

    /**
     * @param $label string
     *
     * @return mixed
     */
    private function getStepLabel($label)
    {
        return ArrayHelper::getValue($this->stepLabels(), $label, $label);
    }

    /**
     * @return array
     */
    private function stepLabels()
    {
        return [
            'select language'     => InstallModule::t('select language'),
            'check requirements'  => InstallModule::t('check requirements'),
            'check environment'   => InstallModule::t('check environment'),
            'cache settings'      => InstallModule::t('cache settings'),
            'db connection'       => InstallModule::t('db connection'),
            'activate modules'    => InstallModule::t('activate modules'),
            'create admin user'   => InstallModule::t('create admin user'),
            'create default town' => InstallModule::t('create default town'),
            'complete'            => InstallModule::t('complete'),
        ];
    }

    /**
     * @param $lang
     */
    public function actionSetLanguage($lang)
    {
        $lang_config = [
            'language' => $lang,
        ];

        $result = $this->writeLangFile($lang_config);

        Yii::$app->getResponse()->redirect('/install');
    }

    /**
     * @param $dump
     *
     * @return bool
     */
    private function writeLangFile($dump)
    {
        return file_put_contents(
                Yii::getAlias('@app/config/lang-local.php'),
                "<?php\nreturn " . VarDumper::export($dump) . ';'
            ) > 0;

    }
}