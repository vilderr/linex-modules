<?php

namespace linex\modules\install\helpers;

use linex\modules\install\models\Town;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\main\helpers\StringHelper;
use linex\modules\main\models\Subdomain;
use Yii;
use yii\base\Component;
use yii\db\Connection;

use linex\modules\install\models\User;

/**
 * Class InstallHelper
 * @package linex\modules\install\helpers
 */
class InstallHelper extends Component
{
    /**
     * @param User       $model
     * @param Connection $db
     *
     * @return bool
     */
    public static function createUser(User $model, Connection $db)
    {
        $db->createCommand()
            ->insert('{{%user}}', [
                'username'      => $model->username,
                'first_name'    => $model->first_name,
                'last_name'     => $model->last_name,
                'password_hash' => Yii::$app->security->generatePasswordHash($model->password),
                'email'         => $model->email,
                'auth_key'      => '',
                'created_at'    => time(),
                'updated_at'    => time(),
                'added_by'      => 'core',
            ])
            ->execute();

        $userId = intval($db->lastInsertID);
        $assignmentResult = $db->createCommand()
                ->insert(
                    '{{%auth_assignment}}',
                    [
                        'item_name' => 'admin',
                        'user_id'   => $userId,
                    ]
                )
                ->execute() === 1;

        return ($assignmentResult && $userId > 0);
    }

    /**
     * @param Town       $model
     * @param Connection $db
     *
     * @return bool
     */
    public static function createTown(Town $model, Connection $db)
    {
        $db->createCommand()
            ->insert(Subdomain::tableName(), [
                'name'       => $model->name,
                'slug'       => StringHelper::translit($model->name),
                'created_at' => time(),
                'updated_at' => time(),
                'sort'       => ModelHelper::SORT_DEFAULT,
                'active'     => ModelHelper::ACTIVE_DEFAULT,
                'added_by'   => 'core',
            ])->execute();

        return intval($db->lastInsertID) > 0;
    }
}