<?php

namespace linex\modules\install\helpers;

use Yii;
use yii\base\Component;

use Symfony\Component\Process\ProcessBuilder;

/**
 * Class MigrateHelper
 * @package linex\modules\install\helpers
 */
class MigrateHelper extends Component
{
    public $migrationTimeout = 3600;
    public $migrationIdleTimeout = 60;
    public $path = '@vendor/linex/modules/install/migrations';

    public function applyMigrations($down = false)
    {
        $builder = $this->commandBuilder($this->path, '{{%migration}}', $down);

        $process = $builder->getProcess();
        $process
            ->setTimeout($this->migrationTimeout)
            ->setIdleTimeout($this->migrationIdleTimeout);

        return $process;
    }

    private function commandBuilder($path = '', $table = '{{%migration}}', $down = false)
    {
        $builder = new ProcessBuilder();
        $app = Yii::getAlias('@app');

        $builder
            ->setWorkingDirectory($app)
            ->setPrefix($this->getPhpExecutable())
            ->setArguments([
                realpath($app . DIRECTORY_SEPARATOR . 'yii'),
                'migrate' . DIRECTORY_SEPARATOR . ($down ? 'down' : 'up'),
                '--color=0',
                '--interactive=0',
                '--migrationTable=' . $table,
                $down ? 65536 : 0,
            ]);

        if (strlen($path) > 0) {
            $builder->add('--migrationPath=' . $path);
        }

        return $builder;
    }

    /**
     * @return string Returns path to PHP executable based on predefined PHP variable PHP_BINDIR
     */
    private function getPhpExecutable()
    {
        return PHP_BINDIR . '/php';
    }
}