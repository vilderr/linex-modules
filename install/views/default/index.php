<?php
/**
 * @var $this  \yii\web\View
 * @var $model \yii\base\DynamicModel
 * @var $langs array
 */

use yii\helpers\Html;
use kartik\form\ActiveForm;
use linex\modules\install\Module as InstallModule;
use linex\modules\main\Module as MainModule;

?>
<div class="installer">
    <? $form = ActiveForm::begin([
        'type'             => ActiveForm::TYPE_HORIZONTAL,
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType'   => true,
        'validationDelay'  => 300,
        'errorCssClass'    => 'error',
        'successCssClass'  => 'sucess',
        'fieldConfig'      => [
            'template' => "{label}{input}",
        ],
    ]) ?>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <?= $form->field($model, 'language')->dropDownList($langs, ['id' => 'select-language'])->label(InstallModule::t('Language')); ?>
        </div>
    </div>
    <div class="controls">
        <?= Html::submitButton(Yii::t('app','Next'), ['class' => 'btn btn-default btn-lg btn-flat']); ?>
    </div>
    <? ActiveForm::end(); ?>
</div>

