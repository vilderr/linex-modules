<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\install\models\Db
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use linex\modules\install\Module;
use kartik\icons\Icon;

?>
<div class="installer">
    <?
    $form = ActiveForm::begin([
        'id'               => 'database-config',
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType'   => true,
        'validationDelay'  => 300,
        'errorCssClass'    => 'error',
        'successCssClass'  => 'sucess',
        'fieldConfig'      => [
            'template' => "{label}{input}",
        ],
    ]);
    ?>
    <?= $form->field($model, 'db_host')->textInput(['autocomplete' => 'off']); ?>
    <?= $form->field($model, 'db_name')->textInput(['autocomplete' => 'off']); ?>
    <?= $form->field($model, 'username')->textInput(['autocomplete' => 'off']); ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'enableSchemaCache', ['template' => '{input}{label}', 'options' => ['class' => 'form-group one-checkbox']])->checkbox(['class' => 'checkbox'], false); ?>
    <?= $form->field($model, 'schemaCacheDuration')->textInput(['autocomplete' => 'off']) ?>
    <?= $form->field($model, 'schemaCache')->textInput(['autocomplete' => 'off']) ?>

    <div class="controls">
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back'), [Url::toRoute(['index'])], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
        <?= Html::submitButton(Icon::show('refresh') . ' ' . Module::t('Check connection'), ['class' => 'btn btn-default btn-lg btn-flat', 'name' => 'check']); ?>
        <?= Html::submitButton(Yii::t('app', 'Next') . ' ' . Icon::show('angle-right'), ['class' => 'btn btn-default btn-lg btn-flat', 'name' => 'next']); ?>
    </div>
    <? ActiveForm::end(); ?>
</div>
