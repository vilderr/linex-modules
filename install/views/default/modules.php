<?php
/**
 * @var $this    \yii\web\View
 * @var $manager \linex\modules\install\components\ModuleManager
 */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
?>
<div class="installer">
    <?= \linex\modules\install\widgets\modules\Install::widget([
        'modules' => $manager->modules,
        'options' => [
            'class' => 'progress progress-striped active',
        ],
    ]); ?>
    <div class="controls">
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back'), [Url::toRoute('db')], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Next'), [Url::toRoute('user')], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
    </div>
</div>
