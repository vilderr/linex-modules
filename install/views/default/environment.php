<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\install\models\Environment
 * @var $valid bool
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
use linex\modules\install\Module;

?>
<div class="installer">
    <table class="environment table">
        <thead>
        <tr>
            <th><?= Module::t('Value'); ?></th>
            <th class="text-right"><?= Module::t('Result'); ?></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->attributes as $attribute => $value): ?>
            <?
            $hasErrors = !empty($model->getErrors($attribute));
            ?>
            <tr>
                <td><?= $model->getAttributeLabel($attribute) ?></td>
                <td class="text-right<?= $hasErrors ? ' text-danger' : '' ?>">
                    <?= $hasErrors ? implode('<br>', $model->getErrors($attribute)) : 'OK' ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <? $form = ActiveForm::begin([
        'id'          => 'install-environment',
        'fieldConfig' => [
            'options'  => [
                'tag' => null,
            ],
            'template' => "{input}",
        ],
    ]); ?>

    <?= $form->field($model, 'assets')->hiddenInput(); ?>
    <?= $form->field($model, 'upload')->hiddenInput(); ?>
    <?= $form->field($model, 'runtime')->hiddenInput(); ?>
    <?= $form->field($model, 'modules')->hiddenInput(); ?>
    <div class="controls">
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back'), [Url::toRoute(['requirements'])], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
        <?= Html::submitButton(Yii::t('app', 'Next') . ' ' . Icon::show('angle-right'), ['class' => 'btn btn-default btn-lg btn-flat', 'disabled' => ($model->hasErrors()) ? true : false]); ?>
    </div>
    <? ActiveForm::end(); ?>
</div>
