<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\install\models\Town
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;

?>
<div class="installer">
    <? $form = ActiveForm::begin([
        'id'               => 'user-create',
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType'   => true,
        'validationDelay'  => 300,
        'errorCssClass'    => 'error',
        'successCssClass'  => 'success',
        'fieldConfig'      => [
            'template' => "{label}{input}",
        ],
    ]); ?>
    <?= $form->field($model, 'name')->textInput(['autocomplete' => 'off']); ?>

    <div class="controls">
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back'), [Url::toRoute('modules')], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
        <?= Html::submitButton(Yii::t('app', 'Next') . ' ' . Icon::show('angle-right'), ['class' => 'btn btn-default btn-lg btn-flat']); ?>
    </div>

    <? ActiveForm::end(); ?>
</div>
