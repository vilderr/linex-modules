<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\install\models\Requirements
 * @var $valid bool
 */

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\icons\Icon;
use linex\modules\install\Module;
?>
<div class="installer">
    <table class="requirements table">
        <thead>
        <tr>
            <th><?= Module::t('Value'); ?></th>
            <th class="text-right"><?= Module::t('Result'); ?></th>
        </tr>
        </thead>
        <tbody>
        <? foreach ($model->attributes as $attribute => $value): ?>
            <?
            $hasErrors = !empty($errors = $model->getErrors($attribute));
            ?>
            <tr>
                <td><?= $model->getAttributeLabel($attribute) ?></td>
                <td class="text-right<?= $hasErrors ? ' text-danger' : '' ?>">
                    <?= $model->isAttributeBoolean($attribute) ? ($hasErrors ? Module::t('Not installed') : Module::t('Installed')) : $value; ?>
                </td>
            </tr>
        <? endforeach; ?>
        </tbody>
    </table>
    <? $form = ActiveForm::begin([
        'id'          => 'install-requirements',
        'fieldConfig' => [
            'options'  => [
                'tag' => null,
            ],
            'template' => "{input}",
        ],
    ]); ?>

    <?= $form->field($model, 'php_version')->hiddenInput(); ?>
    <?= $form->field($model, 'json')->hiddenInput(); ?>
    <?= $form->field($model, 'pcre')->hiddenInput(); ?>
    <?= $form->field($model, 'dom')->hiddenInput(); ?>
    <div class="controls">
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back'), [Url::toRoute(['index'])], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
        <?= Html::submitButton(Yii::t('app', 'Next') . ' ' . Icon::show('angle-right'), ['class' => 'btn btn-default btn-lg btn-flat', 'disabled' => ($valid) ? false : true]); ?>
    </div>
    <? ActiveForm::end(); ?>
</div>

