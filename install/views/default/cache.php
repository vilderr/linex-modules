<?php
/**
 * @var $this         \yii\web\View
 * @var $model        \linex\modules\install\models\Cache
 * @var $cacheClasses array
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use linex\modules\main\widgets\typehead;

?>
<div class="installer">
    <?
    $form = ActiveForm::begin([
        'id'               => 'final-step',
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'validateOnType'   => true,
        'validationDelay'  => 300,
        'errorCssClass'    => 'error',
        'successCssClass'  => 'success',
    ]);
    ?>
    <?= $form->field($model, 'serverName')->textInput(); ?>
    <?= $form->field($model, 'serverPort')->textInput(); ?>
    <?= $form->field($model, 'cacheClass')->widget(typehead\Widget::className(), [
        'source'     => $model::getCacheClasses(),
        'limit'      => 10,
        'scrollable' => true,
    ]); ?>
    <?= $form->field($model, 'keyPrefix') ?>
    <div class="controls">
        <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back'), [Url::toRoute(['environment'])], ['class' => 'btn btn-default btn-lg btn-flat']); ?>
        <?= Html::submitButton(Yii::t('app', 'Next') . ' ' . Icon::show('angle-right'), ['class' => 'btn btn-default btn-lg btn-flat']); ?>
    </div>
    <? ActiveForm::end(); ?>
</div>
