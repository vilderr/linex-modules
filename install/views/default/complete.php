<?php
use yii\helpers\Html;
use linex\modules\install\Module;
?>
<div class="installer">
    <div class="controls">
        <?= Html::a(Module::t('Go to site'), '/dashboard', ['class' => 'btn btn-default btn-lg btn-flat'])?>
    </div>
</div>
