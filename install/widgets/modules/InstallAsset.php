<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 19.06.17
 * Time: 17:02
 */

namespace linex\modules\install\widgets\modules;


use yii\web\AssetBundle;

class InstallAsset extends AssetBundle
{
    public $sourcePath = '@vendor/linex/modules/install/widgets/modules/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->js = [
            'js/install.js',
        ];
    }
}