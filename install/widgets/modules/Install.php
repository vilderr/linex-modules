<?php

namespace linex\modules\install\widgets\modules;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class Install
 * @package linex\modules\install\widgets\modules
 */
class Install extends Widget
{
    public $modules = [];

    public $options = [];

    public function run()
    {
        if (!empty($this->modules)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'div');

            echo Html::tag('div', '', ['id' => 'message']);
            echo Html::tag($tag, $this->renderContent(), $options);
            echo Html::tag('div', '', ['id' => 'log-message']);

            $this->registerJs();
        }
    }

    private function renderContent()
    {
        return Html::tag('div', '', ['class' => 'progress-bar progress-bar-danger bar', 'role' => 'progressbar', 'aria-valuemin' => 0, 'aria-valuemax' => 100, 'style' => 'width:0%;']);
    }

    private function registerJs()
    {
        $count = 0;
        foreach ($this->modules as $module)
        {
            if (!$module['installed'])
                $count++;
        }
        $view = $this->getView();
        InstallAsset::register($view);
        $items = Json::encode($this->modules);
        $js = '$("div.controls").hide();Modules.Options.total = ' . $count . ';Modules.SetProgress(0);Modules.InstallNext(' . Json::encode($this->modules) . ');';
        $view->registerJs($js);
    }
}