var Modules = {
    Options: {
        total: 0
    },
    SetProgress: function (count, message, result) {
        var o = Modules.Options;

        $('div.bar').css('width', (o.total ? (count * 100 / o.total) : 100) + "%");
        if (!!message) {
            $('#message').html(message);
        }
        if (!!result) {
            $('#log-message').append(result);
        }
    },
    InstallNext: function (modules, counter) {
        var o = Modules.Options;
        var count = counter ? counter : 0;

        if (count >= o.total)
        {
            $('div.progress').hide();
            $('#message').html('Все необходимые модули установлены!');
            $('div.controls').show();
        }

        $.each(modules, function (i, m) {
            if (!m.installed) {
                Modules.SetProgress(count, 'Установка модуля - "' + m.name + '"', false);
                count++;
                $.ajax({
                    url: '/install/modules',
                    type: "GET",
                    cache: false,
                    dataType: 'json',
                    data: {
                        'name': i
                    },
                    success: function (data) {
                        modules[i].installed = true;
                        Modules.SetProgress(count, ' ', data.message + '<br>');
                        if (count < o.total)
                        {
                            Modules.InstallNext(modules, count);
                        }
                        else
                        {
                            $('div.progress').hide();
                            $('div.controls').show();
                        }
                    },
                    error: function (e) {
                        console.log(e.responseText);
                    }
                });

                return false;
            }
        });
    }
};
