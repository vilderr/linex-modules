<?php

namespace linex\modules\install\components;

use Yii;
use yii\base\Component;
use yii\helpers\VarDumper;

use linex\modules\main\components\BaseModule;
use linex\modules\install\helpers\MigrateHelper;
use linex\modules\install\Module as InstallModule;

/**
 * Class ModuleManager
 * @package linex\modules\install\components
 */
class ModuleManager extends Component
{
    public $path = null;
    public $basePath = null;
    public $modulesConfigPath = null;
    public $modules = [];
    public $error = null;
    public $message = null;

    public function init()
    {
        $this->basePath = Yii::getAlias('@app');
        $this->path = Yii::getAlias('@vendor') . '/linex/modules';
        $this->modulesConfigPath = $this->basePath . '/config/modules/';
        $this->getModulesArray();
    }

    public function getModulesArray()
    {
        foreach (new \GlobIterator($this->path . '/*') as $item) {
            if (is_dir($item)) {
                $name = $item->getBaseName();
                $path = $this->path . '/' . $name;
                $files = glob($path . '/' . 'Module.php');
                if (count($files) > 0) {
                    $className = pathinfo($files[0], PATHINFO_FILENAME);
                    $class = 'linex\\modules\\' . $name . '\\' . $className;
                    $module = Yii::createObject(['class' => $class], [$name]);
                    if ($module instanceof BaseModule) {
                        $this->modules[$name] = [
                            'name'      => $module->getName(),
                            'class'     => $class,
                            'installed' => file_exists($this->modulesConfigPath . $name . '.php'),
                        ];
                    }
                }
            }
        }
    }

    public function install($name)
    {
        $class = $this->modules[$name]['class'];
        $module = Yii::createObject(['class' => $class], [$name]);
        $path = $module->getBasePath();
        $migrationPath = $path . DIRECTORY_SEPARATOR . 'migrations';
        $config = $module->getConfig();

        try {
            $this->dumpSettings($name, $config);

            if (is_dir($migrationPath)) {
                $this->migrate($migrationPath);
            }

            $this->message = 'Модуль "' . $this->modules[$name]['name'] . '" установлен';

            return true;

        } catch (\Exception $e) {
            Yii::info($e->getMessage(), 'info');
            $this->error = $e->getMessage();
        }
    }

    public function delete($name)
    {
        $result = @unlink($this->modulesConfigPath . $name . '.php');

        if (false === $result) {
            $this->error = InstallModule::t('The install module has not been removed. Please remove it manually in the "{path}" folder', [
                'path' => $this->modulesConfigPath,
            ]);
        }

        $this->error = InstallModule::t('The install module has not been removed. Please remove it manually in the "{path}" folder', [
            'path' => $this->modulesConfigPath,
        ]);

        return false;
    }

    private function dumpSettings($name, array $config)
    {
        if (!empty($config)) {
            $path = $this->modulesConfigPath . $name . '.php';
            if (false === @file_put_contents($path, "<?php\nreturn " . VarDumper::export($config) . ';')) {
                throw new \Exception('Error write module config settings in ' . $path);
            }
        }
    }

    private function migrate($path)
    {
        $process = (new MigrateHelper(['path' => $path]))->applyMigrations(false);
        $process->run();

        if ($process->getExitCode() > 0) {
            throw new \Exception('Migrations in ' . $path . ' not completed!');
        }
    }
}