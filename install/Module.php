<?php

namespace linex\modules\install;

use Yii;
use yii\base\BootstrapInterface;

class Module extends \yii\base\Module implements BootstrapInterface
{
    public $version = '0.0.1';
    public $id = 'install';
    private $_name = 'Linex Installer';

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/install/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/install/messages',
            'fileMap'          => [
                'modules/install/module' => 'module.php',
            ],
        ];
    }

    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/install/' . $category, $message, $params, $language);
    }

    public function getName()
    {
        return self::t($this->_name);
    }
}