<?php

return [
    'Linex Installer'      => 'Установщик Linex',
    'Install: {step name}' => 'Установка: {step name}',

    'select language'     => 'выбор языка',
    'check requirements'  => 'минимальные требования',
    'check environment'   => 'настройки окружения',
    'cache settings'      => 'настройки кеширования',
    'db connection'       => 'настройка соединения с базой данных',
    'database migration'  => 'миграции базы данных',
    'create admin user'   => 'создание администратора',
    'activate modules'    => 'активация модулей',
    'create default town' => 'город по умолчанию',
    'complete'            => 'завершение',

    'Language' => 'Язык',
    'Folder'   => 'Папка',
    'File'     => 'Файл',

    'Value'              => 'Значение',
    'Result'             => 'Результат',
    'Php Version'        => 'Версия PHP',
    'Json Extension'     => 'Расширение JSON',
    'Pcre Extension'     => 'Расширение PCRE',
    'Dom Extension'      => 'Расширение DOM',
    'Ctype Extension'    => 'Расширение CTYPE',
    'Mbstring Extension' => 'Расширение MBSTRING',
    'SPL Extension'      => 'Расширение SPL',
    'PDO Extension'      => 'Расширение PDO',
    'Installed'          => 'Установлено',
    'Not installed'      => 'Не установлено',

    'assets Path'         => 'Папка "assets"',
    'upload Path'         => 'Папка "upload"',
    'runtime Path'        => 'Папка "runtime"',
    'config/modules Path' => 'Папка "config/modules"',
    'DB config file'      => 'Конфиг БД',

    'Server Name'           => 'Имя сервера',
    'Server Port'           => 'Номер порта',
    'Cache Class'           => 'Компонент кеширования',
    'Key Prefix'            => 'Префикс файлов кеша',
    'Enable Schema Cache'   => 'Включить кеширование схемы таблиц',
    'Schema Cache Duration' => 'Время жизни кеша',
    'Schema Cache'          => 'Компонент кеширования',

    'DB host'     => 'Хост',
    'DB name'     => 'Имя базы данных',
    'DB Username' => 'Логин пользователя базы данных',
    'Password'    => 'Пароль',

    'The {type} does not exist' => '{type} не существует',
    'This is not writable'      => 'Не доступно для записи',
    'Check connection'          => 'Проверить соединение',

    'Linex requires a minimum version of PHP - {version}' => 'Требуется версия PHP - {version} и выше',
    'The {extension} extension must be loaded'            => 'Расширение {extension} должно быть загружено',
    'Unable to find specified class'                      => 'Невозможно найти указанный класс',
    'Success connection!'                                 => 'Соединиение успешно!',
    'Install demo data'                                   => 'Установить демо данные',
    'Migrations not completed!'                           => 'Миграции не завершены',
    'The install module was successfully deleted!'        => 'Установщик успешно удален!',

    'The install module has not been removed. Please remove it manually in the "{path}" folder' => 'Установщик не был удален. Пожалуйста, удалите его в ручную в папке "{path}"',

    'Moskow'     => 'Москва',
    'Go to site' => 'Перейти на сайт',
];