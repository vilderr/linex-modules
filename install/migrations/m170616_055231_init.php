<?php

use yii\db\Migration;

class m170616_055231_init extends Migration
{
    public function up()
    {
        mb_internal_encoding("UTF-8");
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        $this->createTable(
            '{{%session}}',
            [
                'id'     => $this->char(40)->notNull(),
                'expire' => $this->integer(11),
                'data'   => 'BLOB',
            ],
            $tableOptions
        );
        $this->addPrimaryKey('id', '{{%session}}', 'id');
    }

    public function down()
    {
        echo "m170616_055231_init cannot be reverted.\n";

        return false;
    }
}
