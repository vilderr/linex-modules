<?php

namespace linex\modules\install\actions;

use Yii;
use yii\base\Module;

/**
 * Class ErrorAction
 * @package linex\modules\install\actions
 */
class ErrorAction extends \yii\web\ErrorAction
{

    public function run()
    {
        if (Yii::$app->getModule('install') instanceof Module) {
            Yii::$app->getResponse()->redirect('/install');
            Yii::$app->end();
        }

        return parent::run();
    }

}