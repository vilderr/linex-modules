<?php

namespace linex\modules\install\models;

use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

use linex\modules\install\Module as InstallModule;
use linex\modules\install\validators\ClassNameValidator;

/**
 * Class Cache
 * @package linex\modules\install\models
 */
class Cache extends Model
{
    public $serverName = 'localhost';
    public $serverPort = 80;
    public $cacheClass = 'yii\caching\FileCache';
    public $keyPrefix = 'lx';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['serverName', 'serverPort', 'cacheClass', 'keyPrefix'], 'filter', 'filter' => 'trim'],
            [['serverName', 'cacheClass'], 'required'],
            [['cacheClass'], ClassNameValidator::className()],
        ];
    }

    /**
     * @return array
     */
    public static function getCacheClasses()
    {
        return [
            'yii\caching\FileCache',
            'yii\caching\MemCache',
            'yii\caching\XCache',
            'yii\caching\ZendDataCache',
            'yii\caching\ApcCache',
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'serverName' => InstallModule::t('Server Name'),
            'serverPort' => InstallModule::t('Server Port'),
            'cacheClass' => InstallModule::t('Cache Class'),
            'keyPrefix'  => InstallModule::t('Key Prefix'),
        ];
    }

    /**
     * @return bool
     */
    public function createConfig()
    {
        $cacheDump = [
            'class'     => $this->cacheClass,
            'keyPrefix' => $this->keyPrefix,
        ];

        $serverDump = [
            'serverName' => $this->serverName,
            'serverPort' => $this->serverPort,
        ];

        return file_put_contents(
                Yii::getAlias('@app/config/cache-local.php'),
                "<?php\nreturn " . VarDumper::export($cacheDump) . ';'
            ) > 0
            && file_put_contents(Yii::getAlias('@app/config/server-local.php'),
                "<?php\nreturn " . VarDumper::export($serverDump) . ';') > 0;
    }
}