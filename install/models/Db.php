<?php

namespace linex\modules\install\models;

use Yii;
use yii\base\Model;
use yii\helpers\VarDumper;

use linex\modules\install\Module;

/**
 * Class Db
 * @package linex\modules\install\models
 */
class Db extends Model
{
    public $db_host = 'localhost';
    public $db_name = 'linex';
    public $username = 'root';
    public $password = '';
    public $enableSchemaCache = true;
    public $schemaCacheDuration = 86400;
    public $schemaCache = 'cache';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['db_host', 'db_name', 'username', 'schemaCache'], 'required'],
            [['password'], 'string'],
            [['enableSchemaCache'], 'filter', 'filter' => 'boolval'],
            [['enableSchemaCache'], 'boolean'],
            [['schemaCacheDuration'], 'integer'],
            [['schemaCacheDuration'], 'filter', 'filter' => 'intval'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'db_host'             => Module::t('DB host'),
            'db_name'             => Module::t('DB name'),
            'username'            => Module::t('DB Username'),
            'password'            => Module::t('Password'),
            'enableSchemaCache'   => Module::t('Enable Schema Cache'),
            'schemaCacheDuration' => Module::t('Schema Cache Duration'),
            'schemaCache'         => Module::t('Schema Cache'),
        ];
    }

    public function testConnection()
    {
        $result = false;
        $config = $this->createConfig();

        try {
            /** @var \yii\db\Connection $dbComponent */
            $dbComponent = Yii::createObject(
                $config
            );

            $dbComponent->open();
            $result = true;
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('danger', Module::t('Connection error: {message}', [
                'message' => $e->getMessage(),
            ]));
        }

        return $result;
    }

    public function getTempConfig()
    {
        return Yii::$app->session->get('db-config', [
            'db_host'             => 'localhost',
            'db_name'             => 'linex',
            'username'            => 'root',
            'password'            => '',
            'enableSchemaCache'   => true,
            'schemaCacheDuration' => 86400,
            'schemaCache'         => 'cache',
            'connectionOk'        => false,
        ]);
    }

    public function createConfig()
    {
        $config = $this->attributes;
        $config['dsn'] = 'mysql:host=' . $this->tempConfig['db_host'] . ';dbname=' . $this->tempConfig['db_name'];
        $config['class'] = 'yii\db\Connection';
        unset($config['db_name'], $config['db_host'], $config['connectionOk']);

        return $config;
    }

    /**
     * @return bool
     */
    public function writeConfig()
    {
        if (false === (
                file_put_contents(
                    Yii::getAlias('@app/config/db-local.php'),
                    "<?php\nreturn " . VarDumper::export($this->createConfig()) . ';'
                ) > 0
            ))
        {
            Yii::$app->session->setFlash('danger', Module::t('Unable write to config file'));
        }

        return true;
    }
}