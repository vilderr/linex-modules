<?php

namespace linex\modules\install\models;

use yii\base\Model;
use linex\modules\install\Module as InstallModule;

/**
 * Class Migrate
 * @package linex\modules\install\models
 */
class Migrate extends Model
{
    public $install_demo_data;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['install_demo_data'], 'filter', 'filter' => 'boolval'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'install_demo_data' => InstallModule::t('Install demo data'),
        ];
    }
}