<?php

namespace linex\modules\install\models;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\validators\BooleanValidator;

use linex\modules\install\Module as InstallModule;

/**
 * Class Requirements
 * @package linex\modules\install\models
 */
class Requirements extends Model
{
    public $php_version;
    public $json;
    public $pcre;
    public $dom;
    public $ctype;
    public $mbstring;
    public $spl;
    public $pdo;

    private $_minPhpVersion = '5.4';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['php_version', 'json', 'pcre', 'dom', 'ctype', 'mbstring', 'spl', 'pdo'], 'required'],
            [['json', 'pcre', 'dom', 'ctype', 'mbstring', 'spl', 'pdo'], 'boolean'],
            [['php_version', 'json', 'pcre', 'dom', 'ctype', 'mbstring', 'spl', 'pdo'], 'validateRequirement'],
        ];
    }

    public function init()
    {
        $this->setAttributes([
            'php_version' => PHP_VERSION,
            'json'        => extension_loaded('json'),
            'pcre'        => extension_loaded('pcre'),
            'dom'         => extension_loaded('dom'),
            'ctype'       => extension_loaded('ctype'),
            'mbstring'    => extension_loaded('mbstring'),
            'spl'         => extension_loaded('spl'),
            'pdo'         => extension_loaded('pdo'),
        ]);
    }

    public function attributeLabels()
    {
        return [
            'php_version' => InstallModule::t('Php Version'),
            'json'        => InstallModule::t('Json Extension'),
            'pcre'        => InstallModule::t('Pcre Extension'),
            'dom'         => InstallModule::t('Dom Extension'),
            'ctype'       => InstallModule::t('Ctype Extension'),
            'mbstring'    => InstallModule::t('Mbstring Extension'),
            'spl'         => InstallModule::t('SPL Extension'),
            'pdo'         => InstallModule::t('PDO Extension'),
        ];
    }

    public function validateRequirement($attribute)
    {
        $message = null;

        switch ($attribute) {
            case 'php_version':
                if (false === version_compare(PHP_VERSION, $this->_minPhpVersion, '>=')) {
                    $message = InstallModule::t('Linex requires a minimum version of PHP - {version}', ['version' => $this->_minPhpVersion]);
                }
                break;
            case 'json':
            case 'pcre':
            case 'dom':
            case 'ctype':
            case 'mbstring':
            case 'spl':
            case 'pdo':
                if (!extension_loaded($attribute)) {
                    $message = InstallModule::t('The {extension} extension must be loaded', ['extension' => $attribute]);
                }
                break;
        }

        if ($message)
            $this->addError($attribute, $message);
    }

    public function isAttributeBoolean($attribute)
    {
        return ArrayHelper::isIn($attribute, $this->getBooleanAttributes());
    }

    /**
     * @return mixed|null|\yii\validators\Validator
     */
    public function getBooleanAttributes()
    {
        foreach ($this->getValidators() as $validator) {
            if ($validator instanceof BooleanValidator) {
                return $validator->attributes;
            }
        }

        return [];
    }
}