<?php

namespace linex\modules\install\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

use linex\modules\install\Module as InstallModule;

/**
 * Class Environment
 * @package linex\modules\install\models
 */
class Environment extends Model
{
    public $assets;
    public $upload;
    public $runtime;
    public $modules;

    /**
     * inherited
     */
    public function init()
    {
        $this->setAttributes([
            'assets'  => $this->checkPath('assets'),
            'upload'  => $this->checkPath('upload'),
            'runtime' => $this->checkPath('runtime'),
            'modules' => $this->checkPath('modules'),
        ]);
    }

    public function rules()
    {
        return [
            [['assets', 'upload', 'runtime', 'modules'], 'required'],
            [['assets', 'upload', 'runtime', 'modules'], 'boolean'],
            [['assets', 'upload', 'runtime', 'modules'], 'checkPath'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'assets'  => InstallModule::t('assets Path'),
            'upload'  => InstallModule::t('upload Path'),
            'runtime' => InstallModule::t('runtime Path'),
            'modules' => InstallModule::t('config/modules Path'),
        ];
    }

    /**
     * @param $attribute
     *
     * @return bool
     */
    public function checkPath($attribute)
    {
        try {
            $path = $this->getPath($attribute);

            if ($this->isDir($path) && $this->isWritable($path)) {
                return true;
            }
        } catch (Exception $e) {
            $this->addError($attribute, $e->getMessage());
        }

        return false;
    }

    /**
     * @param string $attribute
     *
     * @return bool
     */
    public function checkFile($attribute)
    {
        try {
            $path = $this->getPath($attribute);

            if ($this->isFile($path) && is_writable($path))
                return true;

            return true;
        } catch (Exception $e) {
            $this->addError($attribute, $e->getMessage());

            return false;
        }
    }

    /**
     * @param $attribute
     *
     * @return null|string
     */
    private function getPath($attribute)
    {
        $path = null;
        $ds = DIRECTORY_SEPARATOR;
        $app = Yii::getAlias('@app');
        $webroot = Yii::getAlias('@webroot');
        $configPath = $app . $ds . 'config';

        switch ($attribute) {
            case 'assets':
            case 'upload':
                $path = $webroot . $ds . $attribute . $ds;
                break;
            case 'runtime':
                $path = $app . $ds . $attribute . $ds;
                break;
            case 'modules':
                $path = $configPath . $ds . $attribute . $ds;
                break;
            case 'db':
                $path = $configPath . $ds . $attribute . '.php';
                break;
        }

        return $path;
    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws Exception
     */
    private function isDir($path)
    {
        if (!is_dir($path))
            throw new Exception(InstallModule::t('The {type} does not exist', ['type' => InstallModule::t('Folder')]));

        return true;
    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws Exception
     */
    private function isFile($path)
    {
        if (!is_file($path))
            throw new Exception(InstallModule::t('The {type} does not exist', ['type' => InstallModule::t('File')]));

        return true;
    }

    /**
     * @param string $path
     *
     * @return bool
     * @throws Exception
     */
    private function isWritable($path)
    {
        if (is_writable($path))
            return true;

        throw  new Exception(InstallModule::t('This is not writable'));
    }
}