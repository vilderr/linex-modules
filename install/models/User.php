<?php

namespace linex\modules\install\models;

use Yii;
use yii\base\Model;
use linex\modules\users\Module as UsersModule;

/**
 * Class User
 * @package linex\modules\install\models
 */
class User extends Model
{
    public $username = 'admin';
    public $password = '';
    public $email = '';
    public $first_name;
    public $last_name;

    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email', 'username', 'password', 'first_name', 'last_name'], 'filter', 'filter' => 'trim'],
            [['email', 'username', 'password'], 'required'],
            ['password', 'string', 'min' => 8],
        ];
    }

    public function attributeLabels()
    {
        return [
            'email'      => UsersModule::t('Email'),
            'username'   => UsersModule::t('Username'),
            'password'   => UsersModule::t('Password'),
            'first_name' => UsersModule::t('First Name'),
            'last_name'  => UsersModule::t('Last Name'),
        ];
    }

    public function setConfig()
    {
        Yii::$app->session->set('temp_user', $this->getAttributes());
    }

    public static function getConfig()
    {
        return Yii::$app->session->get('temp_user', [
            'username' => 'admin',
        ]);
    }
}