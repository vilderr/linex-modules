<?php

namespace linex\modules\install\models;

use Yii;
use yii\base\Model;
use linex\modules\install\Module;

/**
 * Class Town
 * @package linex\modules\install\models
 */
class Town extends Model
{
    public $name;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['name', 'string', 'min' => 3],
            ['name', 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
        ];
    }

    /**
     * @return void
     */
    public function setConfig()
    {
        Yii::$app->session->set('temp_town', $this->getAttributes());
    }

    /**
     * @return mixed
     */
    public static function getConfig()
    {
        return Yii::$app->session->get('temp_town', [
            'name' => Module::t('Moskow'),
        ]);
    }
}