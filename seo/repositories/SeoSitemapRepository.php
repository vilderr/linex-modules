<?php

namespace linex\modules\seo\repositories;

use yii\web\NotFoundHttpException;
use linex\modules\seo\models\SeoSitemap;
use linex\modules\seo\Module;

/**
 * Class SitemapRepository
 * @package linex\modules\seo\repositories
 */
final class SeoSitemapRepository
{
    /**
     * @param $id
     *
     * @return SeoSitemap
     * @throws NotFoundHttpException
     */
    public function get($id)
    {
        if (!$sitemap = SeoSitemap::findOne($id)) {
            throw new NotFoundHttpException(Module::t('module', 'Sitemap not found'));
        }

        return $sitemap;
    }

    public function save(SeoSitemap $sitemap)
    {
        if (!$sitemap->save()) {
            throw new \DomainException(Module::t('module', 'Sitemap saving error'));
        }
    }

    public function remove(SeoSitemap $sitemap)
    {
        if (!$sitemap->delete()) {
            throw new \DomainException('Removing error.');
        }
    }
}