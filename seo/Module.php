<?php

namespace linex\modules\seo;

use Yii;
use linex\modules\main\components\BaseModule;

/**
 * seo module definition class
 */
class Module extends BaseModule
{
    /**
     * @param \yii\base\Application $app
     *
     * @return void
     */
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/seo/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/seo/messages',
            'fileMap'          => [
                'modules/seo/module' => 'module.php',
            ],
        ];
    }

    /**
     * @param string $category
     * @param        $message
     * @param array  $params
     * @param null   $language
     *
     * @return string
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/seo/' . $category, $message, $params, $language);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::t('module', 'Module Name');
    }

    public function getConfig()
    {
        return [
            'bootstrap' => [
                'seo',
            ],
            'module' => [
                'class' => 'linex\\modules\\seo\\Module',
            ],
            'rules' => [
                'backend' => [
                    'class' => 'yii\\web\\GroupUrlRule',
                    'prefix' => 'dashboard',
                    'routePrefix' => 'dashboard',
                    'rules' => [
                        'seo' => 'seo/default/index',
                        'seo/sitemap' => 'seo/sitemap/index',
                        'seo/sitemap/<_a:(add|edit|delete|delete-all|run)>' => 'seo/sitemap/<_a>',
                    ],
                ],
            ],
        ];
    }
}