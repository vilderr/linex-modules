<?php

use yii\db\Migration;
use linex\modules\seo\models\SeoSitemap as Sitemap;
use linex\modules\main\models\Subdomain;

class m170731_123129_sitemap_table extends Migration
{
    public $table;

    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Sitemap::tableName(), [
            'id'           => $this->primaryKey(),
            'name'         => $this->string(255)->notNull(),
            'subdomain_id' => $this->integer()->notNull()->unique(),
            'filename'     => $this->string(255)->notNull()->defaultValue('sitemap'),
            'categories'   => $this->integer(1)->notNull()->defaultValue(1),
            'products'     => $this->integer(1)->notNull()->defaultValue(1),
            'tags'         => $this->integer(1)->notNull()->defaultValue(1),
            'created_at'   => $this->integer()->unsigned()->notNull(),
            'updated_at'   => $this->integer()->unsigned()->notNull(),
            'sort'         => $this->integer()->notNull()->defaultValue(500),
            'active'       => $this->integer(1)->notNull()->defaultValue(1),
        ], $tableOptions);

        $this->addForeignKey('{{%fk-seo_sitemap-subdomain}}', Sitemap::tableName(), 'subdomain_id', Subdomain::tableName(), 'id', 'CASCADE');
        $this->createIndex('{{%idx-seo_sitemap-active}}', Sitemap::tableName(), 'active');
    }

    public function down()
    {
        echo "m170731_123129_sitemap_table cannot be reverted.\n";

        return false;
    }
}
