<?php

namespace linex\modules\seo\controllers\backend;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use linex\modules\seo\managers\SeoSitemapManager;
use linex\modules\dashboard\components\DashboardController;
use linex\modules\seo\models\collections\SeoSitemapCollection;
use linex\modules\seo\models\search\SeoSitemapSearch;
use linex\modules\seo\Module;

/**
 * Class SitemapController
 * @package linex\modules\seo\controllers\backend
 */
class SitemapController extends DashboardController
{
    public $manager;

    /**
     * SitemapController constructor.
     *
     * @param string            $id
     * @param \yii\base\Module  $module
     * @param SeoSitemapManager $manager
     * @param array             $config
     */
    public function __construct($id, $module, SeoSitemapManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SeoSitemapSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = Module::t('module', 'Sitemap tuning');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/seo/default/index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $post = Yii::$app->request->post();
        $collection = new SeoSitemapCollection();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $sitemap = $this->manager->create($collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $sitemap->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = Module::t('module', 'New Sitemap');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/seo/default/index'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::t('module', 'Sitemap tuning'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = Yii::t('app', 'Add');

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $post = Yii::$app->request->post();
        $sitemap = $this->manager->repository->get($id);
        $collection = new SeoSitemapCollection($sitemap);


        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($sitemap, $collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $sitemap->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = Module::t('module', 'Edit Sitemap');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/seo/default/index'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::t('module', 'Sitemap tuning'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $sitemap->name;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $sitemap = $this->manager->repository->get($id);
        try {
            $sitemap = $this->manager->repository->get($id);
            $this->manager->repository->remove($sitemap);
        } catch (\DomainException $e) {
        }

        return $this->redirect(Url::toRoute(['index']));
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function actionRun($id)
    {
        $sitemap = $this->manager->repository->get($id);

        $this->view->title = Module::t('module', 'Sitemap generate');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/seo/default/index'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::t('module', 'Sitemap tuning'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $sitemap->name,
            'url'   => Url::toRoute(['edit', 'id' => $sitemap->id]),
        ];
        $this->view->params['breadcrumbs'][] = Module::t('module', 'Generate');

        return $this->render('run', [
            'model' => $sitemap,
        ]);
    }


    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['setting manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }
}