<?php

namespace linex\modules\seo\controllers\backend;

use yii\web\Controller;

/**
 * Class DefaultController
 * Default controller for the `seo` module
 * @package linex\modules\seo\controllers\backend
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $this->view->title = 'Настройки модуля - SEO';

        return $this->render('index');
    }
}
