<?php

namespace linex\modules\seo\widgets\backend\sitemap;

use Yii;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Tag;
use linex\modules\seo\managers\SeoSitemapManager;
use linex\modules\seo\models\SeoSitemap;
use linex\modules\seo\repositories\SeoSitemapRepository;
use XMLWriter;

/**
 * Class Parser
 * @package linex\modules\seo\widgets\backend\sitemap
 */
class Parser
{
    const ALWAYS = 'always';
    const HOURLY = 'hourly';
    const DAILY = 'daily';
    const WEEKLY = 'weekly';
    const MONTHLY = 'monthly';
    const YEARLY = 'yearly';
    const NEVER = 'never';

    /**
     * @var XMLWriter
     */
    private $writer;
    /**
     * @var integer Maximum allowed number of URLs in a single file.
     */
    private $urlsCount = 0;
    private $maxUrls = 50000;
    /**
     * @var string path to the file to be written
     */
    private $filePath;

    /**
     * @var array path of files written
     */
    private $writtenFilePaths = [];

    /**
     * @var integer number of URLs to be kept in memory before writing it to file
     */
    private $bufferSize = 1000;

    /**
     * @var bool if XML should be indented
     */
    private $useIndent = true;

    /**
     * @var array valid values for frequency parameter
     */
    private $validFrequencies = array(
        self::ALWAYS,
        self::HOURLY,
        self::DAILY,
        self::WEEKLY,
        self::MONTHLY,
        self::YEARLY,
        self::NEVER,
    );

    /**
     * @var bool whether to gzip the resulting files or not
     */
    private $useGzip = false;

    private $_ns;
    /**
     * @var SeoSitemap
     */
    private $_model;
    private $_manager;

    public function __construct()
    {
        $sitemapRepository = new SeoSitemapRepository();
        $this->_manager = new SeoSitemapManager($sitemapRepository);
    }

    public function init(&$ns, $filePath, $resetNs = false)
    {
        $dir = dirname($filePath);
        if (!is_dir($dir)) {
            throw new \InvalidArgumentException(
                "Please specify valid file path. Directory not exists. You have specified: {$dir}."
            );
        }

        $this->_ns = &$ns;
        $this->_model = $this->_manager->repository->get($this->_ns['id']);
        $this->filePath = $filePath;

        if ($resetNs) {
            $this->_ns['fileCount'] = 0;
            $this->_ns['last_entity_id'] = 0;
            $this->_ns['counter_all'] = 0;
        }
    }

    public function initEx(&$ns)
    {
        $this->_ns = &$ns;
        $this->_model = $this->_manager->repository->get($this->_ns['id']);
    }

    public function addItems($entity)
    {
        $counter = 0;

        switch ($entity) {
            case 'categories':
                if (!$this->_model->categories)
                    break;

                foreach (Category::find()->where(['>', 'lft', $this->_ns['last_entity_id']])->andWhere(['active' => 1])->orderBy('lft')->asArray()->limit(50000)->all() as $category) {
                    $this->addItem($this->_model->subdomain->host . '/catalog/' . $category['slug_path'], 'url', 'urlset', $category['updated_at']);
                    $this->_ns['last_entity_id'] = $category['lft'];

                    $counter++;
                    $this->_ns['counter_all']++;
                }
                break;
            case 'products':
                if (!$this->_model->products)
                    break;
                foreach (Product::find()->where(['>', 'id', $this->_ns['last_entity_id']])->andWhere(['active' => 1])->orderBy('id')->asArray()->limit(50000)->all() as $product) {
                    $this->addItem($this->_model->subdomain->host . '/catalog/product/' . $product['id'], 'url', 'urlset', $product['updated_at']);
                    $this->_ns['last_entity_id'] = $product['id'];

                    $counter++;
                    $this->_ns['counter_all']++;
                }
                break;

            case 'tags':
                if (!$this->_model->tags)
                    break;
                foreach (Tag::find()->where(['>', 'id', $this->_ns['last_entity_id']])->andWhere(['active' => 1])->orderBy('id')->asArray()->limit(50000)->all() as $tag) {
                    $this->addItem($this->_model->subdomain->host . '/tags/' . $tag['slug'], 'url', 'urlset', $tag['updated_at']);
                    $this->_ns['last_entity_id'] = $tag['id'];

                    $counter++;
                    $this->_ns['counter_all']++;
                }
                break;
            case 'finish':
                foreach ($this->_ns['files'] as $key => $file) {
                    $this->addItem($this->_model->subdomain->host . '/' . pathinfo($file, PATHINFO_BASENAME), 'sitemap', 'sitemapindex', time());
                    $counter++;
                    $this->_ns['counter_all']++;
                }
                break;
        }

        return $counter;
    }

    private function addItem($location, $element, $parentElement, $lastModified = null, $changeFrequency = null, $priority = null)
    {
        if ($this->urlsCount === 0) {
            $this->createNewFile($parentElement);
        }

        if ($this->urlsCount % $this->bufferSize === 0) {
            $this->flush();
        }

        $this->writer->startElement($element);
        $this->validateLocation($location);
        $this->writer->writeElement('loc', $location);

        if ($lastModified !== null) {
            $this->writer->writeElement('lastmod', date('c', $lastModified));
        }

        if ($changeFrequency !== null) {
            if (!in_array($changeFrequency, $this->validFrequencies, true)) {
                throw new \InvalidArgumentException(
                    'Please specify valid changeFrequency. Valid values are: '
                    . implode(', ', $this->validFrequencies)
                    . "You have specified: {$changeFrequency}."
                );
            }

            $this->writer->writeElement('changefreq', $changeFrequency);
        }

        if ($priority !== null) {
            if (!is_numeric($priority) || $priority < 0 || $priority > 1) {
                throw new \InvalidArgumentException(
                    "Please specify valid priority. Valid values range from 0.0 to 1.0. You have specified: {$priority}."
                );
            }
            $this->writer->writeElement('priority', number_format($priority, 1, '.', ','));
        }

        $this->writer->endElement();

        $this->urlsCount++;
    }

    /**
     * Create a new file
     * @throws \RuntimeException if file is not writeable
     */
    public function createNewFile($element)
    {
        $this->_ns['fileCount']++;
        $filePath = $this->getCurrentFilePath();
        if ($this->_ns['entity'] != 3)
            $this->_ns['files'][] = $filePath;

        if (file_exists($filePath)) {
            $filePath = realpath($filePath);
            if (is_writable($filePath)) {
                unlink($filePath);
            } else {
                throw new \RuntimeException("File \"$filePath\" is not writable.");
            }
        }

        $this->writer = new XMLWriter();
        $this->writer->openMemory();
        $this->writer->startDocument('1.0', 'UTF-8');
        $this->writer->setIndent($this->useIndent);
        $this->writer->startElement($element);
        $this->writer->writeAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');
    }

    /**
     * Writes closing tags to current file
     */
    private
    function finishFile()
    {
        if ($this->writer !== null) {
            $this->writer->endElement();
            $this->writer->endDocument();
            $this->flush();
        }
    }

    /**
     * Finish writing
     */
    public function write()
    {
        $this->finishFile();
    }

    /**
     * Flushes buffer into file
     */
    private function flush()
    {
        $filePath = $this->getCurrentFilePath();
        file_put_contents($filePath, $this->writer->flush(true), FILE_APPEND);
    }

    /**
     * @return string
     */
    private function getCurrentFilePath()
    {
        if ($this->_ns['fileCount'] < 2)
            return $this->filePath;

        $parts = pathinfo($this->filePath);

        return $parts['dirname'] . DIRECTORY_SEPARATOR . $parts['filename'] . '_' . $this->_ns['fileCount'] . '.' . $parts['extension'];
    }

    /**
     * Takes a string and validates, if the string
     * is a valid url
     *
     * @param string $location
     *
     * @throws \InvalidArgumentException
     */
    protected function validateLocation($location)
    {
        if (false === filter_var($location, FILTER_VALIDATE_URL)) {
            throw new \InvalidArgumentException(
                "The location must be a valid URL. You have specified: {$location}."
            );
        }
    }

    /**
     * @return void
     */
    public function deleteFiles()
    {
        foreach (new \GlobIterator(Yii::getAlias('@webroot') . '/' . $this->_model->filename . '*.xml') as $item) {
            @unlink($item);
        }
    }
}