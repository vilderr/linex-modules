var running = false;

function DoNext(NS) {
    "use strict";

    var container = $('#distribution-result-container');
    var id = container.attr('data-id');

    var queryString = 'id=' + id + '&Generate=Y';

    if (running) {
        var request = $.ajax({
            type: "POST",
            url: '?' + queryString,
            data: NS
        });

        request.done(function (result) {
            container.html(result);
        });
    }
}

StartGenerate = function () {
    running = document.getElementById('start-button').disabled = true;
    DoNext();
};

EndGenerate = function () {
    running = document.getElementById('start-button').disabled = false;
};