<?php

namespace linex\modules\seo\widgets\backend\sitemap;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use linex\modules\seo\models\SeoSitemap;
use linex\modules\seo\widgets\backend\sitemap\assets\SitemapGenerateAsset;

/**
 * Class SitemapGenerate
 * @package linex\modules\seo\widgets\backend\sitemap
 */
class SitemapGenerate extends Widget
{
    public $model;
    private $_message = '';

    private $_errors = [];

    public function init()
    {
        parent::init();
        if (!$this->hasSeoSitemap()) {
            throw new InvalidConfigException("'Model' must be specified and will be valid 'SeoSitemap' object");
        }
    }

    public function run()
    {
        $view = $this->getView();
        SitemapGenerateAsset::register($view);
        $post = Yii::$app->request->post();

        if (Yii::$app->request->isPost && Yii::$app->request->isAjax) {

            if (isset($post['NS']) && is_array($post['NS'])) {
                $NS = $post['NS'];
            } else {
                $NS = [
                    'id'             => $this->model->id,
                    'entity'         => 0,
                    'last_entity_id' => 1,
                    'fileCount'      => 0,
                    'urlsCount'      => 0,
                    'step_values'    => [
                        0 => 'start',
                        1 => 'categories',
                        2 => 'products',
                        3 => 'tags',
                        4 => 'finish',
                    ],
                    'files'          => [],
                    'finish'         => 0,
                    'step'           => 0,
                    'counter_all'    => 0,
                ];
            }

            $entity = $NS['step_values'][$NS['entity']];
            $parser = new Parser();

            if ($entity == $NS['step_values'][0]) {
                $parser->initEx($NS);
                $parser->deleteFiles();
                $NS['entity'] = 1;
                $this->_message = '<p class="lead">Обрабатываю категории...</p>';
            } elseif ($entity == $NS['step_values'][1]) {
                $parser->init($NS, $this->getFilePath('_categories'));
                $counter = $parser->addItems($entity);
                $parser->write();
                $NS['step']++;
                $this->_message = '<p class="lead">Обрабатываю категории...</p><p>Записано категорий: ' . $NS['counter_all'] . '</p>';

                if (!$counter) {
                    $this->_message = '<p class="lead">Обрабатываю товары...</p>';
                    $NS['entity'] = 2;
                    $NS['step'] = 0;
                }

            } elseif ($entity == $NS['step_values'][2]) {
                $reset = $NS['step'] == 0;
                $parser->init($NS, $this->getFilePath('_products'), $reset);
                $counter = $parser->addItems($entity);
                $parser->write();
                $NS['step']++;
                $this->_message = '<p class="lead">Обрабатываю товары...</p><p>Записано товаров: ' . $NS['counter_all'] . '</p>';

                if (!$counter) {
                    $this->_message = '<p class="lead">Обрабатываю теги...</p>';
                    $NS['entity'] = 3;
                    $NS['step'] = 0;
                }
            } elseif ($entity == $NS['step_values'][3]) {
                $reset = $NS['step'] == 0;
                $parser->init($NS, $this->getFilePath('_tags'), $reset);
                $counter = $parser->addItems($entity);
                $parser->write();
                $NS['step']++;
                $this->_message = '<p class="lead">Обрабатываю теги...</p><p>Записано тегов: ' . $NS['counter_all'] . '</p>';

                if (!$counter) {
                    $this->_message = '<p class="lead">Создаю индексный файл...</p>';
                    $NS['entity'] = 4;
                    $NS['step'] = 0;
                }
            } elseif ($entity == $NS['step_values'][4]) {
                $reset = $NS['step'] == 0;
                $parser->init($NS, $this->getFilePath(), $reset);
                $counter = $parser->addItems($entity);
                $parser->write();
                $NS['finish'] = 1;
            }


            if (count($this->_errors) == 0) {
                if (!$NS['finish']) {
                    echo $this->_message;
                    echo '<script>DoNext(' . Json::encode(["NS" => $NS]) . ');</script>';
                } else {
                    echo '<p class="lead">Создание карты сайта завершено!</p>';
                    echo '<script>EndGenerate();</script>';
                }
            } else {
                echo '<script>EndGenerate();</script>';
            }

            Yii::$app->end();
        }

        return $this->render('generate', [
            'model' => $this->model,
        ]);
    }

    /**
     * @param string $suffix
     *
     * @return string
     */
    private function getFilePath($suffix = '')
    {
        return \Yii::getAlias('@webroot') . DIRECTORY_SEPARATOR . $this->model->filename . $suffix . '.xml';
    }

    /**
     * @return bool
     */
    private function hasSeoSitemap()
    {
        return $this->model instanceof SeoSitemap;
    }
}