<?php

namespace linex\modules\seo\widgets\backend\sitemap\assets;

use yii\web\AssetBundle;

/**
 * Class SitemapGenerateAsset
 * @package linex\modules\seo\widgets\backend\sitemap\assets
 */
class SitemapGenerateAsset extends AssetBundle
{
    public $sourcePath = '@linex/modules/seo/widgets/backend/sitemap/dist';

    public $js = [
        'js/sitemap-generate.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}