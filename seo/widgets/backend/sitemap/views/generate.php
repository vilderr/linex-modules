<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\seo\models\SeoSitemap
 */

use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\seo\Module;

?>
<? //echo'<pre>';print_r($model);echo'</pre>';?>
<div class="panel panel-default flat">
    <div class="panel-body" id="distribution-result-container" data-id="<?= $model->id ?>">
        <p class="lead">Для начала генерации карты сайта нажмите кнопку "Сгенерировать".</p>
        <p>Не запускайте скрипт в нескольких вкладках браузера!</p>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(Module::t('module', 'Generate'), [
            'id'      => 'start-button',
            'class'   => 'btn btn-primary btn-flat',
            'onclick' => 'StartGenerate();',
        ]); ?>
        <div class="pull-right">
            <?= Html::a(Icon::show('pencil') . Yii::t('app', 'Edit'), ['edit', 'id' => $model->id], ['class' => 'btn btn-default btn-flat']); ?>
            <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back to list'), ['index'], ['class' => 'btn btn-default btn-flat']); ?>
        </div>
    </div>
</div>
