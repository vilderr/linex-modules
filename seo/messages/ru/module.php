<?php
return [
    'Module Name'          => 'СЕО',
    'Sitemap saving error' => 'Ошибка записи карты сайта',
    'Sitemap not found'    => 'Настройки карты сайта не найдены',
    'Sitemap Maintenance'  => 'Содержание карты сайта',
    'Add Sitemap'          => 'Добавить карту сайта',
    'New Sitemap'          => 'Новая карта сайта',
    'Edit Sitemap'         => 'Редактирование карты сайта',
    'Sitemap tuning'       => 'Настройка карты сайта',
    'Sitemap generate'     => 'Генерация карты сайта',
    'Generate'             => 'Генерация',
];