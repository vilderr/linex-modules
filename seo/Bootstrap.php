<?php

namespace linex\modules\seo;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     *
     * @return void
     */
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/seo/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/seo/messages',
            'fileMap'          => [
                'modules/seo/module' => 'module.php',
            ],
        ];
    }
}