<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 31.07.17
 * Time: 19:35
 */

namespace linex\modules\seo\models\search;

use yii\data\ActiveDataProvider;
use linex\modules\dashboard\models\BaseModelSearch;
use linex\modules\seo\models\SeoSitemap;

class SeoSitemapSearch extends BaseModelSearch
{
    public $id;
    public $name;
    public $subdomain_id;
    public $filename;
    public $sort;
    public $active;

    public function rules()
    {
        return [
            [['name'], 'string'],
            [['id', 'subdomain_id', 'sort', 'active'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new SeoSitemap())->attributeLabels();
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = SeoSitemap::find()->with('subdomain');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0 = 1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'           => $this->id,
            'subdomain_id' => $this->subdomain_id,
            'sort'         => $this->sort,
            'active'       => $this->active,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'filename', $this->filename]);

        return $dataProvider;
    }
}