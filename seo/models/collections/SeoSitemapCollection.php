<?php

namespace linex\modules\seo\models\collections;

use yii\base\Model;
use linex\modules\main\validators\SlugValidator;
use linex\modules\seo\Module;
use linex\modules\seo\models\SeoSitemap;
use linex\modules\main\models\Subdomain;

/**
 * Class SitemapCollection
 * @package linex\modules\seo\models\collections
 *
 * @property string  $name
 * @property integer $subdomain_id
 * @property string  $filename
 * @property integer $categories
 * @property integer $products
 * @property integer $tags
 * @property integer $sort
 * @property integer $active
 */
class SeoSitemapCollection extends Model
{
    public $name;
    public $subdomain_id;
    public $filename;
    public $categories;
    public $products;
    public $tags;
    public $sort;
    public $active;

    private $_sitemap;
    private $_model;

    /**
     * SeoSitemapCollection constructor.
     *
     * @param SeoSitemap|null $sitemap
     * @param array           $config
     */
    public function __construct(SeoSitemap $sitemap = null, array $config = [])
    {
        $this->_model = (new SeoSitemap())->loadDefaultValues();

        if ($sitemap) {
            $this->name = $sitemap->name;
            $this->subdomain_id = $sitemap->subdomain_id;
            $this->filename = $sitemap->filename;
            $this->categories = $sitemap->categories;
            $this->products = $sitemap->products;
            $this->tags = $sitemap->tags;
            $this->sort = $sitemap->sort;
            $this->active = $sitemap->active;

            $this->_sitemap = $sitemap;
        } else {
            $this->load([$this->formName() => $this->_model->attributes]);
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'subdomain_id', 'filename'], 'required'],
            [['name', 'filename'], 'string', 'max' => 255],
            [['filename'], SlugValidator::class],
            [['subdomain_id', 'filename'], 'unique', 'targetClass' => SeoSitemap::class, 'filter' => $this->_sitemap ? ['<>', 'id', $this->_sitemap->id] : null],
            [['subdomain_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subdomain::className(), 'targetAttribute' => ['subdomain_id' => 'id']],
            [['categories', 'products', 'tags', 'sort', 'active'], 'integer'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return $this->_model->attributeLabels();
    }
}