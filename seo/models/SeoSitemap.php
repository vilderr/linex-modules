<?php

namespace linex\modules\seo\models;

use Yii;
use linex\modules\main\models\Subdomain;
use linex\modules\seo\models\query\SeoSitemapQuery;
use yii\behaviors\TimestampBehavior;
use linex\modules\seo\Module;
use linex\modules\main\Module as MainModule;
use linex\modules\catalog\Module as CatalogModule;

/**
 * This is the model class for table "{{%seo_sitemap}}".
 *
 * @property integer   $id
 * @property string    $name
 * @property integer   $subdomain_id
 * @property string    $filename
 * @property integer   $categories
 * @property integer   $products
 * @property integer   $tags
 * @property integer   $created_at
 * @property integer   $updated_at
 * @property integer   $sort
 * @property integer   $active
 *
 * @property Subdomain $subdomain
 */
class SeoSitemap extends \yii\db\ActiveRecord
{
    /**
     * @param $name
     * @param $subdomain_id
     * @param $filename
     * @param $categories
     * @param $products
     * @param $tags
     * @param $sort
     * @param $active
     *
     * @return static
     */
    public static function create($name, $subdomain_id, $filename, $categories, $products, $tags, $sort, $active)
    {
        return new static([
            'name'         => $name,
            'subdomain_id' => $subdomain_id,
            'filename'     => $filename,
            'categories'   => $categories,
            'products'     => $products,
            'tags'         => $tags,
            'sort'         => $sort,
            'active'       => $active,
        ]);
    }

    /**
     * @param $name
     * @param $subdomain_id
     * @param $filename
     * @param $categories
     * @param $products
     * @param $tags
     * @param $sort
     * @param $active
     */
    public function edit($name, $subdomain_id, $filename, $categories, $products, $tags, $sort, $active)
    {
        $this->name = $name;
        $this->subdomain_id = $subdomain_id;
        $this->filename = $filename;
        $this->categories = $categories;
        $this->products = $products;
        $this->tags = $tags;
        $this->sort = $sort;
        $this->active = $active;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seo_sitemap}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => Yii::t('app', 'ID'),
            'name'         => Yii::t('app', 'Name'),
            'subdomain_id' => MainModule::t('subdomain', 'Subdomain'),
            'filename'     => Yii::t('app', 'Filename'),
            'categories'   => CatalogModule::t('Categories'),
            'products'     => CatalogModule::t('Products'),
            'tags'         => CatalogModule::t('Tags'),
            'created_at'   => Yii::t('app', 'Created At'),
            'updated_at'   => Yii::t('app', 'Updated At'),
            'sort'         => Yii::t('app', 'Sort'),
            'active'       => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubdomain()
    {
        return $this->hasOne(Subdomain::className(), ['id' => 'subdomain_id']);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\seo\models\query\SeoSitemapQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SeoSitemapQuery(get_called_class());
    }
}
