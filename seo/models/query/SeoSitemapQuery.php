<?php

namespace linex\modules\seo\models\query;

/**
 * This is the ActiveQuery class for [[\linex\modules\seo\models\SeoSitemap]].
 *
 * @see \linex\modules\seo\models\SeoSitemap
 */
class SeoSitemapQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @inheritdoc
     * @return \linex\modules\seo\models\SeoSitemap[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\seo\models\SeoSitemap|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
