<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\seo\models\collections\SeoSitemapCollection
 */

use yii\helpers\Html;
use yii\bootstrap\Tabs;
use kartik\form\ActiveForm;
use kartik\icons\Icon;
use linex\modules\seo\Module;

?>
<? $form = ActiveForm::begin([
    'id' => 'sitemap-form',
]); ?>
<?= Tabs::widget([
    'id'          => 'sitemap-form-tabs',
    'linkOptions' => [
        'class' => 'flat',
    ],
    'items'       => [
        [
            'label'   => Yii::t('app', 'Common settings'),
            'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
            'active'  => true,
            'options' => [
                'id' => 'tab-common',
            ],
        ],
        [
            'label'   => Module::t('module', 'Sitemap Maintenance'),
            'content' => $this->render('parts/maintenance', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'tab-maintenance',
            ],
        ],
    ],
]); ?>
<div class="panel-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), [
        'class' => 'btn btn-primary btn-flat',
        'name'  => 'action',
        'value' => 'save',
    ]); ?>
    <?= Html::submitButton(Yii::t('app', 'Apply'), [
        'class' => 'btn btn-default btn-flat',
        'name'  => 'action',
        'value' => 'apply',
    ]); ?>
    <?= Html::a(Icon::show('angle-left') . Yii::t('app', 'Back to list'), \yii\helpers\Url::toRoute('index'), [
        'class' => 'btn btn-default btn-flat pull-right',
    ]) ?>
</div>
<? ActiveForm::end(); ?>

