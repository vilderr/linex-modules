<?php
/**
 * @var $this         \yii\web\View
 * @var $searchModel  \linex\modules\seo\models\search\SeoSitemapSearch
 * @var $dataProvider \yii\data\ArrayDataProvider
 */
use yii\helpers\Html;
use kartik\icons\Icon;
use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;
use linex\modules\seo\models\SeoSitemap;
use linex\modules\dashboard\widgets\filter\Filter;
use linex\modules\seo\Module;
use linex\modules\dashboard\widgets\grid\LinkColumn;
use linex\modules\main\helpers\ModelHelper;

?>
<?= Filter::widget([
    'model' => $searchModel,
]); ?>
<div class="sitemap-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'sitemap-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'attribute' => 'subdomain_id',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute([
                        'run',
                        'id'        => $model->id,
                        'returnUrl' => \yii\helpers\Url::to(['index']),
                    ]));
                },
                'format'    => 'raw',
            ],
            [
                'attribute'     => 'created_at',
                'format'        => 'datetime',
                'filterOptions' => [
                    'style' => 'max-width: 180px',
                ],
            ],
            [
                'attribute' => 'subdomain_id',
                'value'     => function ($model) {
                    return Html::a($model->subdomain->name, \yii\helpers\Url::toRoute([
                        '/dashboard/main/subdomain/edit',
                        'id'        => $model->subdomain->id,
                        'returnUrl' => \yii\helpers\Url::to(['index']),
                    ]));
                },
                'format'    => 'raw',
            ],
            'filename',
            'sort',
            [
                'attribute' => 'active',
                'filter'    => ModelHelper::statusList(),
                'value'     => function (SeoSitemap $model) {
                    return ModelHelper::statusLabel($model->active);
                },
                'format'    => 'raw',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id, 'returnUrl' => \yii\helpers\Url::to(['index'])], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'before' => Html::a(Icon::show('plus') . Module::t('module', 'Add Sitemap'), ['add'], ['class' => 'btn btn-primary btn-flat']),
            ],
        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
