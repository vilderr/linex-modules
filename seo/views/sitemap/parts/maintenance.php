<?php
/**
 * @var $this  \yii\web\View
 * @var $form  \kartik\form\ActiveForm
 * @var $model \linex\modules\seo\models\collections\SeoSitemapCollection
 */
?>
<?= $form->field($model, 'categories')->checkbox(); ?>
<?= $form->field($model, 'products')->checkbox(); ?>
<?= $form->field($model, 'tags')->checkbox(); ?>
