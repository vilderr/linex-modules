<?php
/**
 * @var $this  \yii\web\View
 * @var $form  \kartik\form\ActiveForm
 * @var $model \linex\modules\seo\models\collections\SeoSitemapCollection
 */

use linex\modules\main\models\Subdomain;

?>
<?= $form->field($model, 'name')->textInput(['maxlenght' => true]); ?>
<?= $form->field($model, 'subdomain_id')->dropDownList(Subdomain::getList()); ?>
<?= $form->field($model, 'filename')->textInput(['maxlenght' => true]); ?>
<?= $form->field($model, 'sort')->textInput(); ?>
<?= $form->field($model, 'active')->checkbox(); ?>
