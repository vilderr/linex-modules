<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\seo\models\collections\SeoSitemapCollection
 */
?>
<div id="seo-sitemap-edit">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
