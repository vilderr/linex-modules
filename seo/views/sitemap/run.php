<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\seo\models\SeoSitemap
 */

use linex\modules\seo\widgets\backend\sitemap\SitemapGenerate;

?>
<?= SitemapGenerate::widget([
    'model' => $model,
]); ?>
