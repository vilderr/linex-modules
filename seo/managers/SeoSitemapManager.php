<?php

namespace linex\modules\seo\managers;

use linex\modules\seo\models\collections\SeoSitemapCollection;
use linex\modules\seo\models\SeoSitemap;
use linex\modules\seo\repositories\SeoSitemapRepository;

/**
 * Class SitemapManager
 * @package linex\modules\seo\managers
 */
final class SeoSitemapManager
{
    public $repository;

    public function __construct(SeoSitemapRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SeoSitemapCollection $collection
     *
     * @return SeoSitemap
     */
    public function create(SeoSitemapCollection $collection)
    {
        $sitemap = SeoSitemap::create(
            $collection->name,
            $collection->subdomain_id,
            $collection->filename,
            $collection->categories,
            $collection->products,
            $collection->tags,
            $collection->sort,
            $collection->active
        );

        $this->repository->save($sitemap);

        return $sitemap;
    }

    public function edit(SeoSitemap $sitemap, SeoSitemapCollection $collection)
    {
        $sitemap->edit(
            $collection->name,
            $collection->subdomain_id,
            $collection->filename,
            $collection->categories,
            $collection->products,
            $collection->tags,
            $collection->sort,
            $collection->active
        );

        $this->repository->save($sitemap);
    }
}