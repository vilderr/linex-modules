<?php

namespace linex\modules\main\assets;

use yii\web\AssetBundle;

/**
 * Class Bootstrap4Asset
 * @package linex\modules\main\assets4
 */
class Bootstrap4Asset extends AssetBundle
{
    public $sourcePath = '@vendor/twbs/bootstrap/dist';

    public $css = [];

    public $depends = [
        'app\assets\fonts\ProximaNovaAsset',
        'yii\web\JqueryAsset',
        'linex\modules\main\assets\PopperAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public function init()
    {
        $this->css = [
            'css/bootstrap.css?' . time(),
        ];

        $this->js = [
            'js/bootstrap.js?' . time(),
        ];
    }
}