<?php

namespace linex\modules\main\assets;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package linex\modules\main\assets
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/fortawesome/font-awesome';
    public $css = [ 'css/font-awesome.css',];
}