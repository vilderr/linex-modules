<?php

namespace linex\modules\main\assets;

use yii\web\AssetBundle;

/**
 * Class PopperAsset
 * @package linex\modules\main\assets
 */
class PopperAsset extends AssetBundle
{
    public $sourcePath = '@vendor/linex/modules/main/assets/dist';

    public $publishOptions = [
        'forceCopy' => true,
    ];

    public $js = [
        'js/popper.js',
    ];
}