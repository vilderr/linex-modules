<?php

namespace linex\modules\main\helpers;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use linex\modules\main\Module;

/**
 * Class ModelHelper
 * @package linex\modules\main\helpers
 */
class ModelHelper
{
    const STATUS_ACTIVE = 1;
    const STATUS_DRAFT = 0;

    const SORT_DEFAULT = 500;
    const ACTIVE_DEFAULT = 1;

    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            self::STATUS_ACTIVE => Module::t('Status Active'),
            self::STATUS_DRAFT  => Module::t('Status Draft'),
        ];
    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     *
     * @return string
     */
    public static function statusLabel($status)
    {
        switch ($status) {
            case self::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case self::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}