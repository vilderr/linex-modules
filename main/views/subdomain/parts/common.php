<?php
use yii\helpers\Url;
use linex\modules\dashboard\widgets\Redactor;

/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\main\models\collections\SubdomainCollection
 */
?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true, 'id' => 'subdomain-slug']); ?>
<?= $form->field($model, 'slug', [
    'makeSlug' => "#subdomain-slug",
]); ?>
<?= $form->field($model, 'description')->widget(Redactor::className(), [
    'settings' => [
        'minHeight'        => 300,
        'maxHeight'        => 500,
        'imageManagerJson' => Url::to(['/dashboard/redactor/images-get']),
        'imageUpload'      => Url::to(['/dashboard/redactor/image-upload']),
        'plugins'          => [
            'imagemanager',
            'fullscreen',
        ],
    ],
]); ?>
<?= $form->field($model, 'sort')->textInput(); ?>
<?= $form->field($model, 'active')->checkbox(); ?>
