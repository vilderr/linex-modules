<?php
use yii\helpers\Html;
use linex\modules\dashboard\components\ActiveForm;


/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\main\models\collections\SubdomainCollection
 */
?>
<? $form = ActiveForm::begin([
    'id' => 'subdomain-form',
]); ?>
<?= \yii\bootstrap\Tabs::widget([
    'id'          => 'subdomain-form-tabs',
    'linkOptions' => [
        'class' => 'flat',
    ],
    'items'       => [
        [
            'label'   => Yii::t('app', 'Common settings'),
            'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
            'active'  => true,
            'options' => [
                'id' => 'common',
            ],
        ],
        [
            'label'   => Yii::t('app', 'More'),
            'content' => $this->render('parts/more', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'category',
            ],
        ],
    ],
]);
?>
<div class="panel-footer">
    <?= Html::submitButton(Yii::t('app', 'Save'), [
        'class' => 'btn btn-primary btn-flat',
        'name'  => 'action',
        'value' => 'save',
    ]); ?>
    <?= Html::submitButton(Yii::t('app', 'Apply'), [
        'class' => 'btn btn-default btn-flat',
        'name'  => 'action',
        'value' => 'apply',
    ]); ?>
</div>
<? ActiveForm::end(); ?>
