<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\main\models\collections\SubdomainCollection
 */
?>
<?= $this->render('_form', [
    'model' => $model,
]); ?>