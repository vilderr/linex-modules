<?php
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;
use kartik\icons\Icon;
use linex\modules\dashboard\widgets\grid\LinkColumn;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\main\models\Subdomain;
use linex\modules\dashboard\widgets\filter\Filter;
use linex\modules\main\Module;

/**
 * @var $this          \yii\web\View
 * @var $searchModel   \linex\modules\main\models\search\SubdomainSearch
 * @var $dataProvider  \yii\data\ActiveDataProvider
 */
?>
<?= Filter::widget([
    'model' => $searchModel,
]); ?>
<div class="subdomain-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'subdomain-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'class'     => LinkColumn::class,
                'attribute' => 'name',
            ],
            [
                'attribute'     => 'created_at',
                'format'        => 'datetime',
                'filterOptions' => [
                    'style' => 'max-width: 180px',
                ],
            ],
            'sort',
            [
                'attribute' => 'active',
                'filter'    => ModelHelper::statusList(),
                'value'     => function (Subdomain $model) {
                    return ModelHelper::statusLabel($model->active);
                },
                'format'    => 'raw',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id, 'returnUrl' => \yii\helpers\Url::to(['index'])], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'before' => Html::a(Icon::show('plus') . Module::t('subdomain', 'Add Subdomain'), ['add'], ['class' => 'btn btn-primary btn-flat']),
            ],
        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
