<?php

namespace linex\modules\main\models\search;

use Yii;
use yii\data\ActiveDataProvider;
use linex\modules\dashboard\models\BaseModelSearch;
use linex\modules\main\models\Subdomain;

/**
 * Class SubdomainSearch
 * @package linex\modules\main\models\search
 */
class SubdomainSearch extends BaseModelSearch
{
    public $id;
    public $name;
    public $slug;
    public $sort;
    public $active;

    public function rules()
    {
        return [
            [['name', 'slug'], 'string'],
            [['id', 'sort', 'active'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'slug' => Yii::t('app', 'Slug'),
            'sort' => Yii::t('app', 'Sort'),
            'active' => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = Subdomain::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0 = 1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'sort'   => $this->sort,
            'active' => $this->active,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}