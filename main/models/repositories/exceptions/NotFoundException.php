<?php

namespace linex\modules\main\models\repositories\exceptions;
/**
 * Class NotFoundException
 * @package linex\modules\main\models\repositories\exceptions
 */
class NotFoundException extends \DomainException
{

}