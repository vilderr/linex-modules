<?php

namespace linex\modules\main\models\repositories;

use linex\modules\main\models\Subdomain;
use linex\modules\main\Module;
use linex\modules\main\models\repositories\exceptions\NotFoundException;

/**
 * Class SubdomainRepository
 * @package linex\modules\main\models\repositories
 */
final class SubdomainRepository
{
    /**
     * @param $id
     *
     * @return Subdomain
     */
    public function get($id)
    {
        if (!$product = Subdomain::findOne($id)) {
            throw new NotFoundException(Module::t('Element is not found'));
        }

        return $product;
    }

    /**
     * @param Subdomain $subdomain
     *
     * @return void
     */
    public function save(Subdomain $subdomain)
    {
        if (!$subdomain->save()) {
            throw new \RuntimeException(Module::t('subdomain', 'Subdomain saving error'));
        }
    }

    /**
     * @param Subdomain $subdomain
     *
     * @return void
     */
    public function remove(Subdomain $subdomain)
    {
        if (!$subdomain->delete()) {
            throw new \DomainException('Removing error.');
        }
    }
}