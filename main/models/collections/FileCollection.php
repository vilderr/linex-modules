<?php

namespace linex\modules\main\models\collections;

use yii\base\Model;
use yii\web\UploadedFile;

/**
 * Class FileCollection
 * @package linex\modules\main\models\collections
 */
class FileCollection extends Model
{
    /** @var  UploadedFile[] */
    public $files;

    public function rules()
    {
        return [
            ['files', 'each', 'rule' => ['file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024*1024*10]]
        ];
    }

    public function beforeValidate()
    {
        if (parent::beforeValidate())
        {
            $this->files = UploadedFile::getInstances($this, 'files');
            return true;
        }

        return false;
    }
}