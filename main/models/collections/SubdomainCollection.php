<?php

namespace linex\modules\main\models\collections;

use linex\modules\main\helpers\SubdomainHelper;
use linex\modules\main\models\Subdomain;
use linex\modules\main\validators\SlugValidator;
use yii\base\Model;

/**
 * Class SubdomainCollection
 * @package linex\modules\main\models\collections
 */
class SubdomainCollection extends Model
{
    public $name;
    public $slug;
    public $description;
    public $sort;
    public $active;

    private $_model;
    private $_subdomain;

    /**
     * SubdomainCollection constructor.
     *
     * @param Subdomain $subdomain
     * @param array     $config
     */
    public function __construct(Subdomain $subdomain = null, array $config = [])
    {
        if ($subdomain) {
            $this->name = $subdomain->name;
            $this->slug = $subdomain->slug;
            $this->description = $subdomain->description;
            $this->sort = $subdomain->sort;
            $this->active = $subdomain->active;

            $this->_subdomain = $subdomain;
        } else {
            $this->sort = SubdomainHelper::SORT_DEFAULT;
            $this->active = SubdomainHelper::STATUS_ACTIVE;
        }

        $this->_model = new Subdomain();
        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['description'], 'string'],
            [['sort', 'active'], 'integer'],
            [['name'], 'string', 'max' => 20],
            [['slug'], SlugValidator::class],
            [['slug'], 'unique', 'targetClass' => Subdomain::class, 'filter' => $this->_subdomain ? ['<>', 'id', $this->_subdomain->id] : null],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return $this->_model->attributeLabels();
    }
}