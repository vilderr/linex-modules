<?php

namespace linex\modules\main\models\query;

/**
 * This is the ActiveQuery class for [[\linex\modules\main\models\Subdomain]].
 *
 * @see \linex\modules\main\models\Subdomain
 */
class SubdomainQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @inheritdoc
     * @return \linex\modules\main\models\Subdomain[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\main\models\Subdomain|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
