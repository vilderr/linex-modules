<?php

namespace linex\modules\main\models\managers;

use linex\modules\main\models\collections\SubdomainCollection;
use linex\modules\main\models\repositories\SubdomainRepository;
use linex\modules\main\models\Subdomain;

/**
 * Class SubdomainManager
 * @package linex\modules\main\models\managers
 */
final class SubdomainManager
{
    public $repository;

    public function __construct(SubdomainRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param SubdomainCollection $collection
     *
     * @return Subdomain
     */
    public function create(SubdomainCollection $collection)
    {
        $subdomain = new Subdomain([
            'name'        => $collection->name,
            'slug'        => $collection->slug,
            'description' => $collection->description,
            'sort'        => $collection->sort,
            'active'      => $collection->active,
        ]);

        $this->repository->save($subdomain);

        return $subdomain;
    }

    /**
     * @param Subdomain           $subdomain
     * @param SubdomainCollection $collection
     *
     * @return void
     */
    public function edit(Subdomain $subdomain, SubdomainCollection $collection)
    {

        $subdomain->name = $collection->name;
        $subdomain->slug = $collection->slug;
        $subdomain->description = $collection->description;
        $subdomain->sort = $collection->sort;
        $subdomain->active = $collection->active;

        $this->repository->save($subdomain);
    }
}