<?php

namespace linex\modules\main\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $original_name
 * @property string $external_id
 * @property integer $height
 * @property integer $width
 * @property integer $size
 * @property string $subdir
 */
class File extends \yii\db\ActiveRecord
{
    public static function create(UploadedFile $file)
    {
        return new static([]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['height', 'width', 'size'], 'integer'],
            [['original_name', 'external_id', 'subdir'], 'string', 'max' => 255],
            [['external_id'], 'unique'],
            [['external_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'original_name' => 'Original Name',
            'external_id' => 'External ID',
            'height' => 'Height',
            'width' => 'Width',
            'size' => 'Size',
            'subdir' => 'Subdir',
        ];
    }
}
