<?php

namespace linex\modules\main\models;

use Yii;
use yii\helpers\ArrayHelper;
use yii\behaviors\TimestampBehavior;
use linex\modules\main\Module;
use linex\modules\main\models\query\SubdomainQuery;
use linex\modules\main\behaviors\BanDelete;

/**
 * This is the model class for table "{{%subdomain}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property string  $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sort
 * @property integer $active
 * @property string  $added_by
 */
class Subdomain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%subdomain}}';
    }

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
            'ban'       => [
                'class'   => BanDelete::className(),
                'message' => Yii::t('app', 'Not allowed!'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('app', 'ID'),
            'name'        => Module::t('subdomain', 'Area Name'),
            'slug'        => Module::t('subdomain', 'Slug'),
            'description' => Module::t('subdomain', 'Description'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'sort'        => Yii::t('app', 'Sort'),
            'active'      => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @inheritdoc
     * @return SubdomainQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SubdomainQuery(get_called_class());
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->active()->orderBy('sort')->all(), 'id', 'name');
    }

    public function getHost()
    {
        $scheme = 'http://';
        $subdomain = ($this->id == 1) ? '' : $this->slug . '.';

        return $scheme . $subdomain . Yii::$app->modules['main']->serverName;
    }
}
