<?php

use yii\db\Migration;

class m170619_114448_main extends Migration
{
    public function up()
    {
        mb_internal_encoding("UTF-8");
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        $this->createTable(
            '{{%session}}',
            [
                'id'     => $this->char(40)->notNull(),
                'expire' => $this->integer(11),
                'data'   => 'BLOB',
            ],
            $tableOptions
        );
        $this->addPrimaryKey('id', '{{%session}}', 'id');

        $this->createTable(
            '{{%dynagrid}}',
            [
                'id'        => $this->string(100)->defaultValue(''),
                'filter_id' => $this->string(100),
                'sort_id'   => $this->string(100),
                'data'      => $this->text(),
            ]
        );
        $this->addPrimaryKey(
            'pk-id',
            '{{%dynagrid}}',
            'id'
        );
        $this->createTable(
            '{{%dynagrid_dtl}}',
            [
                'id'          => $this->string(128)->defaultValue(''),
                'category'    => $this->string(10),
                'name'        => $this->string(150),
                'data'        => $this->text(),
                'dynagrid_id' => $this->string(100),
                'UNIQUE `uniq_dtl` (`name`, `category`, `dynagrid_id`)',
            ]
        );
        $this->addPrimaryKey(
            'pk-id',
            '{{%dynagrid_dtl}}',
            'id'
        );
    }

    public function down()
    {
        echo "m170619_114448_main cannot be reverted.\n";

        return false;
    }
}
