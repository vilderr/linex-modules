<?php

use yii\db\Migration;
use linex\modules\main\models\Subdomain;

class m170728_033401_subdomain_table extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Subdomain::tableName(), [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(20)->notNull(),
            'slug'        => $this->string()->notNull()->unique(),
            'description' => $this->text(),
            'created_at'  => $this->integer()->unsigned()->notNull(),
            'updated_at'  => $this->integer()->unsigned()->notNull(),
            'sort'        => $this->integer()->defaultValue(500),
            'active'      => $this->integer(1)->defaultValue(1),
            'added_by'    => $this->string(4)->defaultValue('user'),
        ], $tableOptions);

        $this->createIndex('{{%idx-subdomain-slug}}', Subdomain::tableName(), 'slug', true);
        $this->createIndex('{{%idx-subdomain-active}}', Subdomain::tableName(), 'active');
    }

    public function down()
    {
        echo "m170728_033401_subdomain_table cannot be reverted.\n";

        return false;
    }
}
