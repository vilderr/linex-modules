<?php

use yii\db\Migration;
use linex\modules\main\models\File;

/**
 * Handles the creation of table `file`.
 */
class m170921_112111_create_file_table extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(File::tableName(), [
            'id'            => $this->primaryKey(),
            'original_name' => $this->string(255),
            'external_id'   => $this->string(255)->unique(),
            'height'        => $this->integer(),
            'width'         => $this->integer(),
            'size'          => $this->integer(),
            'subdir'        => $this->string(255),

        ], $tableOptions);

        $this->createIndex('{{%idx-file-external_id}}', File::tableName(), 'external_id', true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo "m170921_112111_create_file_table cannot be reverted.\n";

        return false;
    }
}
