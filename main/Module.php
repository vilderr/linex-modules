<?php

namespace linex\modules\main;

use Yii;
use linex\modules\main\components\BaseModule;
use yii\base\BootstrapInterface;

/**
 * Class MainModule
 * @package linex\modules\main
 */
class Module extends BaseModule
{
    public $version = '0.0.1';
    public $serverName;
    public $serverPort;

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/main/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/main/messages',
            'fileMap'          => [
                'modules/main/module'    => 'module.php',
                'modules/main/subdomain' => 'subdomain.php',
            ],
        ];
    }

    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/main/' . $category, $message, $params, $language);
    }

    public static function getLangArray()
    {
        return [
            'ru-RU' => self::t('module', 'Russian'),
            'en-US' => self::t('module','English'),
        ];
    }

    public function getConfig()
    {
        return [
            'bootstrap' => [
                'main',
            ],
            'module'    => [
                'class'               => 'linex\\modules\\main\\Module',
                'controllerNamespace' => 'linex\\modules\\main\\controllers\\frontend',
                'layoutPath'          => '@app/views/layouts',
                'viewPath'            => '@app/views/modules/main',
            ],
            'rules'     => [
                '/'       => 'main/default/index',
                'backend' => [
                    'class'       => 'yii\\web\\GroupUrlRule',
                    'prefix'      => 'dashboard',
                    'routePrefix' => 'dashboard',
                    'rules'       => [
                        'main'                                  => 'main/default/index',
                        'main/subdomain'                        => 'main/subdomain/index',
                        'main/subdomain/<_a:(add|edit|delete)>' => 'main/subdomain/<_a>',
                    ],
                ],
            ],
        ];
    }

    public function getName()
    {
        return self::t('module','Module Name');
    }
}