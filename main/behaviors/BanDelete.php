<?php

namespace linex\modules\main\behaviors;

use yii\base\Behavior;
use yii\base\Model;
use yii\db\ActiveRecord;

/**
 * Запрет удаления системных записей
 *
 * Class BanDelete
 * @package linex\modules\main\behaviors
 */
class BanDelete extends Behavior
{
    public $message = 'You can not remove system line';

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @param $event
     */
    public function beforeDelete($event)
    {
        if ($this->owner->added_by === 'core') {
            \Yii::$app->session->setFlash('error', $this->message);
            $event->isValid = false;
        } else {
            $event->isValid = true;
        }
    }
}