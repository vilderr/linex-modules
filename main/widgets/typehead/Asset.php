<?php

namespace linex\modules\main\widgets\typehead;

use yii\web\AssetBundle;

/**
 * Class Asset
 * @package linex\modules\main\widgets\typehead
 */
class Asset extends AssetBundle
{
    public $sourcePath = '@vendor/linex/modules/main/widgets/typehead/dist';

    public $css = [
        'css/typeahead.css',
    ];

    public $js = [
        'js/typeahead.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}