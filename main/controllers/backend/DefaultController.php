<?php

namespace linex\modules\main\controllers\backend;

use linex\modules\dashboard\components\DashboardController;
use linex\modules\main\Module;

/**
 * Class DefaultController
 * @package linex\modules\main\controllers\backend
 */
class DefaultController extends DashboardController
{
    public function actionIndex()
    {
        $this->view->title = Module::getInstance()->getName();
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index');
    }
}