<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 28.07.17
 * Time: 9:08
 */

namespace linex\modules\main\controllers\backend;

use linex\modules\main\models\collections\SubdomainCollection;
use linex\modules\main\models\search\SubdomainSearch;
use Yii;
use linex\modules\main\models\managers\SubdomainManager;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use linex\modules\dashboard\components\DashboardController;
use linex\modules\main\Module;

/**
 * Class SubdomainController
 * @package linex\modules\main\controllers\backend
 */
class SubdomainController extends DashboardController
{
    public $name;
    public $manager;

    /**
     * SubdomainController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param SubdomainManager $manager
     * @param array            $config
     */
    public function __construct($id, $module, SubdomainManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
        $this->name = Module::t('subdomain', 'Subdomains');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['setting manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SubdomainSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = Module::t('subdomain', 'List of subdomains');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/main'),
        ];
        $this->view->params['breadcrumbs'][] = $this->name;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $post = Yii::$app->request->post();
        $collection = new SubdomainCollection();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $subdomain = $this->manager->create($collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $subdomain->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = Module::t('subdomain', 'Add Subdomain');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/main'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->name,
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = Module::t('subdomain', 'New Subdomain');

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $subdomain = $this->manager->repository->get($id);
        $collection = new SubdomainCollection($subdomain);
        $post = Yii::$app->request->post();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($subdomain, $collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $subdomain->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = Module::t('subdomain', 'Subdomain editing');
        $this->view->params['breadcrumbs'][] = [
            'label' => Module::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/main'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->name,
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $collection->name;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        try {
            $subdomain = $this->manager->repository->get($id);
            $this->manager->repository->remove($subdomain);
        } catch (\DomainException $e) {
        }

        return $this->redirect(Url::toRoute(['index']));
    }
}