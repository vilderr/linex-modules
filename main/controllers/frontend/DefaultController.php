<?php

namespace linex\modules\main\controllers\frontend;

use yii\web\Controller;

/**
 * Class DefaultController
 * @package linex\modules\main\controllers\frontend
 */
class DefaultController extends Controller
{
    public $layout = 'mainpage';

    public function actionIndex()
    {
        $this->view->title = 'Главная';
        return $this->render('index');
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
}