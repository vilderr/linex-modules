<?php

namespace linex\modules\main;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package linex\modules\main
 */
class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/main/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/main/messages',
            'fileMap'          => [
                'modules/main/module' => 'module.php',
            ],
        ];
    }
}