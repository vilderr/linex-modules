<?php

namespace linex\modules\main\components;

use yii\base\Module;
use yii\base\BootstrapInterface;

/**
 * Class BaseModule
 * @package linex\modules\main\components
 */
abstract class BaseModule extends Module implements BootstrapInterface
{
    abstract function getName();
    abstract function getConfig();
}