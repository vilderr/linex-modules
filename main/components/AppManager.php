<?php

namespace linex\modules\main\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\base\Exception;

class AppManager extends Component
{
    private $baseConfig = [];
    private $config = [];
    public $basePath = null;
    public $modulesPath = null;
    public $modulesConfigPath = null;

    private $cacheFilePath = null;
    private $cacheFileName = 'linex_settings';

    public $configSections = [
        'modules',
        'rules',
        'bootstrap',
        'params',
        'component',
    ];

    public function init()
    {
        $this->initPaths();
    }

    public function merge(array $config = [])
    {
        $this->baseConfig = $config;

        return $this->getSettings();
    }

    private function initPaths()
    {
        $this->basePath = \Yii::getAlias('@app');
        $this->modulesPath = \Yii::getAlias('@vendor') . '/linex/modules';
        $this->modulesConfigPath = $this->basePath . '/config/modules';
    }

    private function getSettings()
    {
        return $this->prepareSettings();
    }

    private function prepareSettings()
    {
        $settings = [];

        foreach (new \GlobIterator($this->modulesConfigPath . '/*.php') as $item) {
            if (is_dir($this->modulesPath . '/' . $item->getBaseName('.php')) === false) {
                continue;
            }

            $moduleConfig = require $item->getRealPath();

            foreach ($this->configSections as $section) {
                switch ($section) {
                    case 'modules':
                        if (!empty($moduleConfig['module'])) {
                            $settings['modules'] = ArrayHelper::merge(
                                isset($settings['modules']) ? $settings['modules'] : [],
                                [$item->getBaseName('.php') => $moduleConfig['module']]
                            );
                        }
                        break;
                    default:
                        // Стандартное слитие:
                        if (isset($moduleConfig[$section]) && !empty($moduleConfig[$section])) {
                            $settings[$section] = ArrayHelper::merge(
                                isset($settings[$section]) ? $settings[$section] : [],
                                $moduleConfig[$section]
                            );
                        }
                        break;
                }
            }
        }

        return ArrayHelper::merge(
            $this->mergeSettings($settings),
            is_file($this->basePath . '/config/lang-local.php') ? require $this->basePath . '/config/lang-local.php' : []
        );
    }

    private function mergeSettings($settings = [])
    {
        $this->config = ArrayHelper::merge(
            $this->baseConfig,
            [
                'bootstrap'  => ArrayHelper::merge(
                    isset($this->config['bootstrap'])
                        ? $this->config['bootstrap']
                        : [],
                    isset($settings['bootstrap'])
                        ? $settings['bootstrap']
                        : []
                ),
                'modules'    => ArrayHelper::merge(
                    isset($this->config['modules'])
                        ? $this->config['modules']
                        : [],
                    isset($settings['modules'])
                        ? $settings['modules']
                        : []
                ),
                'components' => ArrayHelper::merge(
                    isset($this->config['components'])
                        ? $this->config['components']
                        : [],
                    isset($settings['component'])
                        ? $settings['component']
                        : []
                ),
                'params'     => ArrayHelper::merge(
                    isset($this->config['params'])
                        ? $this->config['params']
                        : [],
                    isset($settings['params'])
                        ? $settings['params']
                        : []
                ),
            ]
        );


        if (!array_key_exists('rules', $settings)) {
            $settings['rules'] = [];
        }

        if (isset($this->config['components']['urlManager']['rules'])) {
            // Фикс для настроек маршрутизации:
            $this->config['components']['urlManager']['rules'] = ArrayHelper::merge(
                $this->config['components']['urlManager']['rules'],
                $settings['rules']
            );
        }

        return $this->config;
    }
}