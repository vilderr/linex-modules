<?php

namespace linex\modules\main\components;

use Composer\Script\Event;

/**
 * Class Composer
 * @package linex\modules\main\components
 */
final class Composer
{
    /**
     * @param Event $event
     */
    public static function preInstall(Event $event)
    {
        echo "Linex install\n";
    }

    /**
     * @param Event $event
     */
    public static function preUpdate(Event $event)
    {
        echo "Linex update\n";
    }
}