<?php
return [
    'Subdomains'         => 'Поддомены',
    'Subdomain'          => 'Поддомен',
    'List of subdomains' => 'Спиcок поддоменов',
    'Add Subdomain'      => 'Добавить поддомен',
    'New Subdomain'      => 'Новый поддомен',
    'Subdomain editing'  => 'Редактирование поддомена',

    'Area Name'   => 'Название региона (города)',
    'Slug'        => 'Поддомен',
    'Description' => 'Описание',

    'Subdomain saving error' => 'Ошибка сохранения поддомена',
];