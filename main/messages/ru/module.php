<?php
return [
    'Module Name'      => 'Главный модуль',
    'Run'              => 'Запустить',
    'From'             => 'От',
    'To'               => 'До',

    'Value'         => 'Значение',
    'Result'        => 'Результат',
    'Installed'     => 'Установлено',
    'Not installed' => 'Не установлено',

    'Russian' => 'Русский',
    'English' => 'English',

    'Status Active' => 'Да',
    'Status Draft'  => 'Нет',
];