<?php
return [
    'Module Name' => 'Пользователи',

    'List of users'    => 'Список пользователей',
    'Rbac roles'       => 'Роли пользователей',
    'Rbac permissions' => 'Разрешения пользователей',

    'Authorization' => 'Авторизация',
    'Access denied' => 'Доступ запрещен',

    'Add User'        => 'Добавить пользователя',
    'User edit'       => 'Редактирование пользователя',
    'Add role'        => 'Добавить роль',
    'Edit role'       => 'Редактирование роли',
    'Add permission'  => 'Добавить разрешение',
    'Edit permission' => 'Редактирование разрешение',

    'Username'    => 'Логин',
    'Password'    => 'Пароль',
    'Email'       => 'Электронная почта',
    'First Name'  => 'Имя',
    'Last Name'   => 'Фамилия',
    'Status'      => 'Статус',
    'Created At'  => 'Дата создания',
    'Updated At'  => 'Дата изменения',
    'Remember Me' => 'Запомнить меня',
    'Sign In'     => 'Войти',
    'Sign out'    => 'Выйти',
    'Profile'     => 'Профиль',
    'Roles'       => 'Роли',
    'Permissions' => 'Разрешения',

    'Status Active'  => 'Активен',
    'Status Deleted' => 'Удален',

    'Rbac Name'        => 'Название',
    'Rbac Old Name'    => 'Старое название',
    'Rbac Type'        => 'Тип',
    'Rbac Description' => 'Описание',
    'Rbac Biz Rule'    => 'Правила',
    'Rbac Children'    => 'Роли и разрешения',
];