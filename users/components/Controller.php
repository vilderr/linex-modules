<?php

namespace linex\modules\users\components;

use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;

use linex\modules\dashboard\components\DashboardController;

/**
 * Class RbacController
 * @package linex\modules\users\components
 */
class Controller extends DashboardController
{
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['user manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }
}