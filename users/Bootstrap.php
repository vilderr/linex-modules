<?php

namespace linex\modules\users;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package linex\modules\users
 */
class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/users/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/users/messages',
            'fileMap'          => [
                'modules/users/module' => 'module.php',
            ],
        ];
    }
}