<?php

use yii\db\Migration;
use linex\modules\users\models\User;

class m170619_075246_user extends Migration
{
    public function init()
    {
        //Yii::$app->language = Yii::$app->session->get('app-language');
    }

    public function up()
    {
        mb_internal_encoding("UTF-8");
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        $this->createTable(
            '{{%auth_rule}}',
            [
                'name'       => $this->string(64)->notNull(),
                'data'       => $this->text(),
                'created_at' => $this->integer(11),
                'updated_at' => $this->integer(11),
            ],
            $tableOptions
        );
        $this->addPrimaryKey('name', '{{%auth_rule}}', 'name');

        $this->createTable(
            '{{%auth_item}}',
            [
                'name'        => $this->string(64),
                'type'        => $this->smallInteger(1),
                'description' => $this->text(),
                'rule_name'   => $this->string(64),
                'data'        => $this->text(),
                'created_at'  => $this->integer(11)->notNull(),
                'updated_at'  => $this->integer(11)->notNull(),
            ],
            $tableOptions
        );

        $this->addPrimaryKey('name', '{{%auth_item}}', 'name');
        $this->createIndex('idx_auth_item_rule_name', '{{%auth_item}}', 'rule_name');
        $this->createIndex('idx_auth_item_type', '{{%auth_item}}', 'type');
        $this->addForeignKey(
            'fk_auth_item_rule',
            '{{%auth_item}}',
            'rule_name',
            '{{%auth_rule}}',
            'name',
            null,
            'CASCADE'
        );

        $this->createTable(
            '{{%auth_item_child}}',
            [
                'parent' => $this->string(64)->notNull(),
                'child'  => $this->string(64)->notNull(),
            ],
            $tableOptions
        );

        $this->addPrimaryKey('pk_auth_item_child', '{{%auth_item_child}}', ['parent', 'child']);
        $this->createIndex('idx_auth_item_child', '{{%auth_item_child}}', 'child');
        $this->addForeignKey(
            'fk_auth_item_child_parent',
            '{{%auth_item_child}}',
            'parent',
            '{{%auth_item}}',
            'name',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_auth_item_child_child',
            '{{%auth_item_child}}',
            'child',
            '{{%auth_item}}',
            'name',
            'CASCADE',
            'CASCADE'
        );

        $this->createTable(
            '{{%auth_assignment}}',
            [
                'item_name'  => $this->string(64)->notNull(),
                'user_id'    => $this->string(64)->notNull(),
                'created_at' => $this->integer(11),
                'updated_at' => $this->integer(11),
                'rule_name'  => $this->string(64),
                'data'       => $this->text(),
            ],
            $tableOptions
        );
        $this->addPrimaryKey('pk_auth_assignment', '{{%auth_assignment}}', ['item_name', 'user_id']);
        $this->createIndex('idx_auth_assignment_rule_name', '{{%auth_assignment}}', 'rule_name');
        $this->addForeignKey(
            'fk_auth_assignment_item_name',
            '{{%auth_assignment}}',
            'item_name',
            '{{%auth_item}}',
            'name',
            'CASCADE',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_auth_assignment_rule_name',
            '{{%auth_assignment}}',
            'rule_name',
            '{{%auth_rule}}',
            'name',
            null,
            'CASCADE'
        );

        $this->createTable(
            User::tableName(),
            [
                'id'                    => $this->primaryKey(),
                'username'              => $this->string(255)->notNull()->unique(),
                'auth_key'              => $this->binary(32),
                'password_hash'         => $this->string(255)->notNull(),
                'password_reset_token'  => $this->binary(32),
                'email'                 => $this->string(255)->notNull(),
                'status'                => $this->smallInteger(2)->notNull()->defaultValue(10),
                'created_at'            => $this->integer()->notNull(),
                'updated_at'            => $this->integer()->notNull(),
                'first_name'            => $this->string(255)->notNull(),
                'last_name'             => $this->string(255)->notNull(),
                'username_is_temporary' => $this->smallInteger(1)->notNull()->defaultValue(0),
                'added_by'              => $this->string(4)->defaultValue('user'),
            ],
            $tableOptions
        );

        $this->batchInsert(
            '{{%auth_item}}',
            ['name', 'type', 'description', 'created_at', 'updated_at'],
            [
                ['admin', '1', Yii::t('app', 'Administrator'), time(), time()],
                ['manager', '1', Yii::t('app', 'Content Manager'), time(), time()],
                ['administrate', '2', Yii::t('app', 'Administrate panel'), time(), time()],
                ['user manage', '2', Yii::t('app', 'User management'), time(), time()],
                ['cache manage', '2', Yii::t('app', 'Cache management'), time(), time()],
                ['content manage', '2', Yii::t('app', 'Content management'), time(), time()],
                ['setting manage', '2', Yii::t('app', 'Setting management'), time(), time()],
                ['catalog manage', '2', Yii::t('app', 'Catalog management'), time(), time()],
            ]
        );

        $this->batchInsert(
            '{{%auth_item_child}}',
            ['parent', 'child'],
            [
                ['manager', 'administrate'],
                ['manager', 'content manage'],
                ['admin', 'administrate'],
                ['admin', 'user manage'],
                ['admin', 'cache manage'],
                ['admin', 'content manage'],
                ['admin', 'setting manage'],
                ['admin', 'catalog manage'],
            ]
        );
    }

    public function down()
    {
        echo "m170619_075246_user cannot be reverted.\n";

        return false;
    }
}
