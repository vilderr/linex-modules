<?php
/**
 * @var $this          \yii\web\View
 * @var $permissions   \yii\data\ArrayDataProvider
 * @var $isRules       bool
 */
use yii\helpers\Html;

use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;
use kartik\icons\Icon;

use linex\modules\main\Module as MainModule;
use linex\modules\users\Module as UsersModule;
use linex\modules\dashboard\widgets\RemoveAllButton;

?>
<div class="rbac-permissions">
    <?=
    DynaGrid::widget([
        'options'            => [
            'id' => 'rbac-permissions-grid',
        ],
        'columns'            => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'attribute' => 'name',
                'label'     => Yii::t('app', 'Name'),
                'value'     => function ($model) {
                    return Html::a($model->name, ['edit', 'id' => $model->name]);
                },
                'format'    => 'raw',
                'options'   => [
                    'width' => '30%',
                ],
            ],
            [
                'attribute' => 'description',
                'label'     => Yii::t('app', 'Description'),
            ],
            [
                'attribute' => 'ruleName',
                'visible'   => $isRules,
            ],
            [
                'attribute' => 'createdAt',
                'label'     => Yii::t('app', 'Created At'),
                'value'     => function ($data) {
                    return date("Y-m-d H:i:s", $data->createdAt);
                },
                'options'   => [
                    'width' => '200px',
                ],
            ],
            [
                'attribute' => 'updatedAt',
                'label'     => Yii::t('app', 'Updated At'),
                'value'     => function ($data) {
                    return date("Y-m-d H:i:s", $data->updatedAt);
                },
                'options'   => [
                    'width' => '200px',
                ],
            ],
            [
                'class'    => 'kartik\grid\ActionColumn',
                'template' => '<div class="btn-group">{update}{delete}</div>',
                'buttons'  => [
                    'update' => function ($url, $model) {
                        return Html::a(Icon::show('pencil'), ['edit', 'id' => $model->name], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a(Icon::show('trash'), ['delete', 'id' => $model->name], ['class' => 'btn btn-danger btn-sm', 'title' => Yii::t('app', 'Delete'), 'data-method' => 'post']);
                    },
                ],
            ],
        ],
        'theme'              => 'panel-default',
        'gridOptions'        => [
            'dataProvider' => $permissions,
            'hover'        => true,
            'panel'        => [
                'before' => Html::a(Icon::show('plus') . UsersModule::t('Add permission'), ['add'], ['class' => 'btn btn-primary btn-flat']),
                'after'  => RemoveAllButton::widget([
                    'url'          => '/dashboard/users/rbac/permissions/delete-all',
                    'gridSelector' => '.grid-view',
                    'htmlOptions'  => [
                        'class' => 'btn btn-danger btn-flat',
                    ],
                ]),
            ],
        ],
        'allowSortSetting'   => false,
        'allowThemeSetting'  => false,
        'allowFilterSetting' => false,
    ]);
    ?>
</div>