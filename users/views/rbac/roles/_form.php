<?php
/**
 * @var $this        \yii\web\View
 * @var $model       \linex\modules\users\models\backend\AuthItem
 * @var $items       array
 * @var $children    array
 */

use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\icons\Icon;

use linex\modules\main\Module as MainModule;
use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\dashboard\widgets\MultiSelect;

?>
<?php $form = ActiveForm::begin([
    'id'         => 'edit-rbac-role-form',
    'type'       => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>
    <div class="panel-body">
        <?= $form->field($model, 'name')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'description')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'oldname', ['template' => '{input}'])->input('hidden'); ?>
        <?= $form->field($model, 'type', ['template' => '{input}'])->input('hidden'); ?>
        <?= MultiSelect::widget([
            'items'         => $items,
            'selectedItems' => $children,
            'ajax'          => false,
            'name'          => 'AuthItem[children][]',
            'label'         => $model->getAttributeLabel('children'),
            'defaultLabel'  => Yii::t('app', 'Choose item'),
        ]); ?>
    </div>
    <div class="panel-footer">
        <?=
        Html::a(
            Icon::show('arrow-circle-left') . Yii::t('app', 'Back'),
            Yii::$app->request->get('returnUrl', ['index']),
            ['class' => 'btn btn-danger btn-flat']
        )
        ?>
        <?=
        Html::submitButton(
            Icon::show('save') . Yii::t('app', 'Save'),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'back',
            ]
        )
        ?>
        <?= Html::submitButton(
            Icon::show('save') . Yii::t('app', 'Apply'),
            [
                'class' => 'btn btn-warning btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        ); ?>
    </div>
<?php ActiveForm::end(); ?>