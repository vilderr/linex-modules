<?php
/**
 * @var $model       \linex\modules\users\models\backend\AuthItem
 * @var $rules       array
 * @var $items       array
 * @var $children    array
 */
?>
<div class="row rbac-role-edit">
    <div class="col-md-7">
        <div class="panel panel-default flat widget">
            <?= $this->render('_form', [
                'model'    => $model,
                'items'    => $items,
                'children' => $children,
            ]) ?>
        </div>
    </div>
</div>