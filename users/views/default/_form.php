<?php
/**
 * @var $model       \linex\modules\users\models\User
 * @var $assignments \yii\rbac\Assignment[]
 */

use yii\helpers\Html;

use kartik\form\ActiveForm;
use kartik\icons\Icon;

use linex\modules\users\Module as UsersModule;
use linex\modules\dashboard\widgets\MultiSelect;

?>
<?php $form = ActiveForm::begin([
    'id'         => 'edit-user-form',
    'type'       => ActiveForm::TYPE_HORIZONTAL,
    'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
]); ?>
<div class="panel panel-default flat">
    <div class="panel-body">
        <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'autocomplete' => 'off']) ?>
        <?= $form->field($model, 'first_name')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'last_name')->textInput(['maxlength' => 255]) ?>
        <?= $form->field($model, 'password')->textInput(['maxlength' => 255])->passwordInput() ?>
        <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => 255]) ?>
        <?= MultiSelect::widget([
            'items'         => \yii\helpers\ArrayHelper::map(
                \Yii::$app->getAuthManager()->getRoles(),
                'name',
                function ($item) {
                    return $item->name . (strlen($item->description) > 0
                            ? ' [' . $item->description . ']'
                            : '');
                }
            ),
            'selectedItems' => $model->isNewRecord ? [] : $assignments,
            'ajax'          => false,
            'name'          => 'AuthAssignment[]',
            'label'         => UsersModule::t('Roles'),
            'defaultLabel'  => Yii::t('app', 'Choose item'),
        ]);
        ?>
    </div>
    <div class="panel-footer">
        <?=
        Html::a(
            Icon::show('arrow-circle-left') . Yii::t('app', 'Back'),
            Yii::$app->request->get('returnUrl', ['index']),
            ['class' => 'btn btn-danger btn-flat']
        )
        ?>
        <?= Html::submitButton(
            Icon::show('save') . Yii::t('app', 'Save'),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'back',
            ]
        ); ?>
        <?=
        Html::submitButton(
            Icon::show('save') . Yii::t('app', 'Apply'),
            [
                'class' => 'btn btn-warning btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        )
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
