<?php
/**
 * @var $model \linex\modules\users\models\User
 * @var $assignments \yii\rbac\Assignment[]
 */
?>
<div class="row add-user">
    <div class="col-md-6">
        <?= $this->render('_form', [
            'model'       => $model,
            'assignments' => $assignments,
        ]); ?>
    </div>
</div>
