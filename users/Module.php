<?php

namespace linex\modules\users;

use Yii;
use yii\base\BootstrapInterface;

use linex\modules\main\components\BaseModule;

/**
 * Class UserModule
 * @package linex\modules\users
 */
class Module extends BaseModule
{
    public $version = '0.0.1';
    /**
     * Duration of login session for users in seconds.
     * By default 30 days.
     * @var int
     */
    public static $loginSessionDuration = 2592000;
    /**
     * Expiration time in seconds for user password reset generated token.
     * @var int
     */
    public $passwordResetTokenExpire = 3600;

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/users/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/users/messages',
            'fileMap'          => [
                'modules/users/module' => 'module.php',
            ],
        ];
    }

    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/users/' . $category, $message, $params, $language);
    }

    public function getConfig()
    {
        return [
            'bootstrap' => [
                'users',
            ],
            'module' => [
                'class' => 'linex\\modules\\users\\Module',
                'controllerNamespace' => 'linex\\modules\\users\\controllers\\frontend',
                'layoutPath'          => '@app/views/layouts',
                'viewPath'            => '@app/views/modules/users',
            ],
            'component' => [
                'authManager' => [
                    'class' => 'linex\\modules\\users\\components\\CachedDbRbacManager',
                    'cache' => 'cache',
                ],
                'user' => [
                    'class' => '\\yii\\web\\User',
                    'identityClass' => 'linex\\modules\\users\\models\\User',
                    'enableAutoLogin' => true,
                    'loginUrl' => [
                        'login',
                    ],
                ],
            ],
            'rules' => [
                'backend' => [
                    'class' => 'yii\\web\\GroupUrlRule',
                    'prefix' => 'dashboard',
                    'routePrefix' => 'dashboard',
                    'rules' => [
                        'users' => 'users/default/index',
                        'users/<_a:(add|edit|delete|delete-all)>' => 'users/default/<_a>',
                        'users/rbac/<_c:(roles|permissions)>' => 'users/rbac/<_c>/index',
                        'users/rbac/<_c:(roles|permissions)>/<_a:(add|edit|delete|delete-all)>' => 'users/rbac/<_c>/<_a>',
                        'user/<_a:(login|logout)>' => 'users/login/<_a>',
                    ],
                ],
            ],
        ];
    }

    public function getName()
    {
        return self::t('Module Name');
    }
}