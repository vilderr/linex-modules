<?php

namespace linex\modules\users\controllers\backend;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;

use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\users\Module as UserModule;
use linex\modules\users\models\forms\LoginForm;

/**
 * Class LoginController
 * @package linex\modules\user\controllers\backend
 */
class LoginController extends Controller
{
    public $layout = '@vendor/linex/modules/dashboard/views/layouts/empty';

    public function behaviors()
    {
        return [
            'verbs' => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actionLogin()
    {
        if (Yii::$app->user->can(DashboardModule::$administratePermission)) {
            Yii::$app->getResponse()->redirect(Yii::$app->user->getReturnUrl('/dashboard'));
        }

        $model = new LoginForm();
        $model->setScenario(LoginForm::SCENARIO_DASHBOARD);
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->login()) {
            return $this->goBack('/dashboard');
        }

        $this->view->title = UserModule::t('Authorization');

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/dashboard/user/login');
    }
}