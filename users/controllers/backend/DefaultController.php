<?php

namespace linex\modules\users\controllers\backend;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;
use yii\web\NotFoundHttpException;

use linex\modules\users\Module as UsersModule;
use linex\modules\main\Module as MainModule;
use linex\modules\users\models\backend\Search;
use linex\modules\users\models\User;
use linex\modules\users\components\Controller;

/**
 * Class Defaultcontroller
 * @package linex\modules\users\controllers\backend
 */
class DefaultController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new Search();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = UsersModule::t('List of users');
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $model = new User();
        $model->setScenario('adminSignup');
        $model->generateAuthKey();

        $assignments = Yii::$app->authManager->getAssignments(null);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            $postAssignments = Yii::$app->request->post('AuthAssignment', []);
            $errors = [];

            foreach ($assignments as $assignment) {
                $key = array_search($assignment->roleName, $postAssignments);
                if ($key === false) {
                    Yii::$app->authManager->revoke(new Item(['name' => $assignment->roleName]), $model->id);
                } else {
                    unset($postAssignments[$key]);
                }
            }
            foreach ($postAssignments as $assignment) {
                try {
                    Yii::$app->authManager->assign(new Item(['name' => $assignment]), $model->id);
                } catch (\Exception $e) {
                    $errors[] = 'Cannot assign "' . $assignment . '" to user';
                }
            }
            if (count($errors) > 0) {
                Yii::$app->getSession()->setFlash('error', implode('<br />', $errors));
            }
            Yii::$app->session->setFlash('success', MainModule::t('The data was successfully saved!'));

            $returnUrl = Yii::$app->request->get('returnUrl', ['index']);
            switch (Yii::$app->request->post('action', 'save')) {
                case 'back':
                    return $this->redirect($returnUrl);
                default:
                    return $this->redirect(
                        Url::toRoute(
                            [
                                'edit',
                                'id'        => $model->id,
                                'returnUrl' => $returnUrl,
                            ]
                        )
                    );
            }
        }

        $this->view->title = UsersModule::t('Add User');
        $this->view->params['breadcrumbs'][] = [
            'label' => UsersModule::getInstance()->getName(),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model'       => $model,
            'assignments' => ArrayHelper::map($assignments, 'roleName', 'roleName'),
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'admin';
        $assignments = Yii::$app->authManager->getAssignments($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();

            $postAssignments = Yii::$app->request->post('AuthAssignment', []);
            $errors = [];

            foreach ($assignments as $assignment) {
                $key = array_search($assignment->roleName, $postAssignments);
                if ($key === false) {
                    Yii::$app->authManager->revoke(new Item(['name' => $assignment->roleName]), $model->id);
                } else {
                    unset($postAssignments[$key]);
                }
            }
            foreach ($postAssignments as $assignment) {
                try {
                    Yii::$app->authManager->assign(new Item(['name' => $assignment]), $model->id);
                } catch (\Exception $e) {
                    $errors[] = 'Cannot assign "' . $assignment . '" to user';
                }
            }
            if (count($errors) > 0) {
                Yii::$app->getSession()->setFlash('error', implode('<br />', $errors));
            }

            Yii::$app->session->setFlash('success', MainModule::t('The data was successfully saved!'));

            $returnUrl = Yii::$app->request->get('returnUrl', ['index']);

            switch (Yii::$app->request->post('action', 'save')) {
                case 'back':
                    return $this->redirect($returnUrl);
                default:
                    return $this->redirect(
                        Url::toRoute(
                            [
                                'edit',
                                'id'        => $model->id,
                                'returnUrl' => $returnUrl,
                            ]
                        )
                    );
            }
        }

        $this->view->title = UsersModule::t('User edit');
        $this->view->params['breadcrumbs'][] = [
            'label' => UsersModule::getInstance()->getName(),
            'url'   => Url::toRoute('index'),
        ];

        $this->view->params['breadcrumbs'][] = $model->username;

        return $this->render('edit', [
            'model'       => $model,
            'assignments' => ArrayHelper::map($assignments, 'roleName', 'roleName'),
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * @param $id
     *
     * @return User
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException;
    }
}