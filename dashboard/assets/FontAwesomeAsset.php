<?php

namespace linex\modules\dashboard\assets;

use yii\web\AssetBundle;

/**
 * Class FontAwesomeAsset
 * @package linex\modules\dashboard\assets
 */
class FontAwesomeAsset extends AssetBundle
{
    public $sourcePath = '@vendor/fortawesome/font-awesome';
    public $css = [ 'css/font-awesome.css',];
}