<?php

namespace linex\modules\dashboard\assets;

use yii\web\AssetBundle;

/**
 * Class DashboardAsset
 * @package linex\modules\dashboard\assets
 */
class DashboardAsset extends AssetBundle
{
    public $sourcePath = '@vendor/linex/modules/dashboard/assets/dist';

    public $css = [
        'css/admin.css',
        'css/skins/default.css',
    ];

    public $js = [
        'js/admin.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/plugins/amaran/jquery.amaran.min.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'linex\modules\dashboard\assets\FontAwesomeAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}