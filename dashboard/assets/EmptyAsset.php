<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 27.06.17
 * Time: 21:35
 */

namespace linex\modules\dashboard\assets;


use yii\web\AssetBundle;

/**
 * Class EmptyAsset
 * @package linex\modules\dashboard\assets
 */
class EmptyAsset extends AssetBundle
{
    public $sourcePath = '@vendor/linex/modules/dashboard/assets/dist';

    public function init()
    {
        parent::init();

        $this->css = [
            YII_DEBUG ? 'css/empty.css?' . time() : 'css/empty.min.css?' . time(),
        ];
    }

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
        '\kartik\icons\FontAwesomeAsset',
    ];
}