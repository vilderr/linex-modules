<?php

use yii\db\Migration;

use linex\modules\dashboard\models\DashboardMenu;

class m170627_175226_dashboard extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        mb_internal_encoding("UTF-8");
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        // Backend menu data
        $this->createTable(
            DashboardMenu::tableName(),
            [
                'id'         => $this->primaryKey(),
                'parent_id'  => $this->integer()->defaultValue(0),
                'name'       => $this->string(255)->notNull(),
                'route'      => $this->string(255),
                'icon'       => $this->string(255),
                'sort'       => $this->integer()->defaultValue(100),
                'rbac_check' => $this->string(255),
                'added_by'   => $this->string(4)->defaultValue('user'),
            ],
            $tableOptions
        );

        $this->insert(
            DashboardMenu::tableName(),
            [
                'parent_id'  => 0,
                'name'       => Yii::t('app', 'Users', [], 'ru-RU'),
                'route'      => '',
                'icon'       => 'user',
                'rbac_check' => 'user manage',
                'added_by'   => 'core',
            ]
        );
        $lastId = $this->db->lastInsertID;
        $this->batchInsert(
            DashboardMenu::tableName(),
            ['parent_id', 'name', 'route', 'icon', 'rbac_check', 'added_by'],
            [
                [$lastId, Yii::t('app', 'Users list'), 'dashboard/users/default/index', 'users', 'user manage', 'core'],
            ]
        );
        $this->insert(
            DashboardMenu::tableName(),
            [
                'parent_id'  => $lastId,
                'name'       => Yii::t('app', 'Rbac'),
                'route'      => '',
                'icon'       => 'lock',
                'rbac_check' => 'user manage',
                'added_by'   => 'core',
            ]
        );
        $lastId = $this->db->lastInsertID;
        $this->batchInsert(
            DashboardMenu::tableName(),
            ['parent_id', 'name', 'route', 'icon', 'rbac_check', 'added_by'],
            [
                [$lastId, Yii::t('app', 'Roles'), 'dashboard/users/rbac/roles/index', 'angle-right', 'user manage', 'core'],
                [$lastId, Yii::t('app', 'Permissions'), 'dashboard/users/rbac/permissions/index', 'angle-right', 'user manage', 'core'],
            ]
        );

        $this->insert(
            DashboardMenu::tableName(),
            [
                'parent_id'  => 0,
                'name'       => Yii::t('app', 'Settings'),
                'route'      => '',
                'icon'       => 'cogs',
                'rbac_check' => 'setting manage',
                'added_by'   => 'core',
            ]
        );
        $lastId = $this->db->lastInsertID;
        $this->batchInsert(
            DashboardMenu::tableName(),
            ['parent_id', 'name', 'route', 'icon', 'rbac_check', 'added_by'],
            [
                [$lastId, Yii::t('app', 'Backend menu'), 'dashboard/backend-menu/index', 'bars', 'setting manage', 'core'],
                [$lastId, Yii::t('app', 'Subdomains'), 'dashboard/main/subdomain/index', 'map-marker', 'setting manage', 'core'],
            ]
        );

        $this->insert(
            DashboardMenu::tableName(),
            [
                'parent_id'  => 0,
                'name'       => Yii::t('app', 'Content'),
                'route'      => '',
                'icon'       => 'contao',
                'rbac_check' => 'content manage',
                'added_by'   => 'core',
            ]
        );
        $lastId = $this->db->lastInsertID;
        $this->batchInsert(
            DashboardMenu::tableName(),
            ['parent_id', 'name', 'route', 'icon', 'rbac_check', 'added_by'],
            [
                [$lastId, Yii::t('app', 'Pages'), 'dashboard/pages/default/index', 'file-text-o', 'content manage', 'core'],
            ]
        );

        $this->insert(
            DashboardMenu::tableName(),
            [
                'parent_id'  => 0,
                'name'       => Yii::t('app', 'Catalog'),
                'route'      => '',
                'icon'       => 'shopping-bag',
                'rbac_check' => 'catalog manage',
                'added_by'   => 'core',
            ]
        );
        $lastId = $this->db->lastInsertID;
        $this->batchInsert(
            DashboardMenu::tableName(),
            ['parent_id', 'name', 'route', 'icon', 'rbac_check', 'added_by'],
            [
                [$lastId, Yii::t('app', 'Goods'), 'dashboard/catalog/product/index', 'angle-right', 'catalog manage', 'core'],
                [$lastId, Yii::t('app', 'Categories'), 'dashboard/catalog/category/index', 'angle-right', 'catalog manage', 'core'],
                [$lastId, Yii::t('app', 'Props'), 'dashboard/catalog/property/index', 'angle-right', 'catalog manage', 'core'],
                [$lastId, Yii::t('app', 'Tags'), 'dashboard/catalog/tag/index', 'tags', 'catalog manage', 'core'],
                [$lastId, Yii::t('app', 'Import'), 'dashboard/catalog/import/index', 'download', 'catalog manage', 'core'],
                [$lastId, Yii::t('app', 'Distribution'), 'dashboard/catalog/distribution/index', 'angle-right', 'catalog manage', 'core'],
                [$lastId, Yii::t('app', 'Facet'), 'dashboard/catalog/facet/index', 'angle-right', 'catalog manage', 'core'],
            ]
        );

        $this->insert(
            DashboardMenu::tableName(),
            [
                'parent_id'  => 0,
                'name'       => Yii::t('app', 'Seo'),
                'route'      => '',
                'icon'       => 'sellsy',
                'rbac_check' => 'setting manage',
                'added_by'   => 'core',
            ]
        );
        $lastId = $this->db->lastInsertID;
        $this->batchInsert(
            DashboardMenu::tableName(),
            ['parent_id', 'name', 'route', 'icon', 'rbac_check', 'added_by'],
            [
                [$lastId, Yii::t('app', 'Settings'), 'dashboard/seo/default/index', 'angle-right', 'setting manage', 'core'],
                [$lastId, Yii::t('app', 'Sitemap'), 'dashboard/seo/sitemap/index', 'angle-right', 'setting manage', 'core'],
            ]
        );
    }

    public function down()
    {
        echo "m170627_175226_dashboard cannot be reverted.\n";

        return false;
    }
}
