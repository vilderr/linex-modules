<?php
/**
 * @var $this    \yii\web\View
 * @var $content string
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use kartik\icons\Icon;

use linex\modules\dashboard;
use linex\modules\main\Module as MainModule;
use linex\modules\users\Module as UsersModule;
use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\main\widgets\alert;
use linex\modules\dashboard\widgets\FlushCacheButton;

dashboard\assets\DashboardAsset::register($this);
?>
<? $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-default sidebar-mini">
<?php $this->beginBody(); ?>
<div class="wrapper">
    <header class="main-header">
        <a href="/dashboard" class="logo">
            <span class="logo-mini">LX</span>
            <span class="logo-lg"><?= Yii::$app->name; ?></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <?= \kartik\icons\Icon::show('bars') ?>
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li>
                        <?= Html::a(Icon::show('contao') . 'Перейти на сайт', '/'); ?>
                    </li>
                    <? if (Yii::$app->user->can('cache manage')): ?>
                        <li>
                            <?= FlushCacheButton::widget(
                                [
                                    'url'         => Url::to(['/dashboard/flush-cache']),
                                    'htmlOptions' => [
                                        'class' => '',
                                        'title' => DashboardModule::t('Flush cache'),
                                    ],
                                    'label'       => Icon::show('trash-o'),
                                    'onSuccess'   => 'function(data) {
                                    $.amaran({message: data, position: "top right"});
                                }',
                                ]
                            ) ?>
                        </li>
                    <? endif; ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <span class="hidden-xs"><?= Yii::$app->user->getIdentity()->displayName; ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-footer">
                                <div>
                                    <a href="#"
                                       class="btn btn-default btn-flat btn-block"><?= UsersModule::t('Profile'); ?></a>
                                </div>
                                <div>
                                    <?= Html::a(
                                        UsersModule::t('Sign out'),
                                        ['/dashboard/user/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat btn-block']
                                    ) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <?=
            dashboard\widgets\DashboardMenu::widget([
                'options'         => [
                    'class' => 'sidebar-menu',
                ],
                'submenuTemplate' => '<ul class="treeview-menu">{items}</ul>',
                'items'           => dashboard\models\DashboardMenu::getAllMenu(),
            ]);
            ?>
        </section>
    </aside>
    <div class="content-wrapper">
        <section class="content-header">
            <h1><?= $this->title; ?></h1>
            <?= Breadcrumbs::widget([
                'options'  => [
                    'class' => 'breadcrumbs',
                ],
                'homeLink' => [
                    'label' => DashboardModule::t('Dashboard'),
                    'url'   => Url::toRoute(['/dashboard/default/index']),
                ],
                'links'    => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>
        <section class="content">
            <?= alert\Widget::widget(['options' => ['class' => 'flat']]) ?>
            <?= $content; ?>
        </section>
    </div>
    <footer class="main-footer"></footer>
    <? \yii\bootstrap\Modal::begin(
        [
            'id'     => 'delete-confirmation',
            'footer' =>
                Html::button(
                    Yii::t('app', 'Delete'),
                    [
                        'class'        => 'btn btn-danger btn-flat',
                        'data-action'  => 'confirm',
                        'data-dismiss' => 'modal',
                    ]
                )
                . Html::button(
                    Yii::t('app', 'Cancel'),
                    [
                        'class'        => 'btn btn-default btn-flat',
                        'data-dismiss' => 'modal',
                    ]
                ),
            'header' => DashboardModule::t('Are you sure you want to delete this object?'),
        ]
    )
    ?>
    <div class="alert flat alert-danger">
        <i class="fa fa-exclamation-triangle fa-lg"></i>
        <?= DashboardModule::t('All data will be lost') ?>
    </div>
    <?php \yii\bootstrap\Modal::end() ?>
</div>
<?php $this->endBody(); ?>
</body>
</html>
<? $this->endPage(); ?>
