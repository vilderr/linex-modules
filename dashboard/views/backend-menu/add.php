<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\dashboard\models\DashboardMenu
 */
?>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default flat">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>