<?php

namespace linex\modules\dashboard;

use Yii;
use yii\base\BootstrapInterface;
use linex\modules\main\components\BaseModule;

/**
 * Class Module
 * @package linex\modules\dashboard
 */
class Module extends BaseModule implements BootstrapInterface
{
    public $version = '0.0.1';
    public $layout = 'main';
    public static $administratePermission = 'administrate';

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/dashboard/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/dashboard/messages',
            'fileMap'          => [
                'modules/dashboard/module' => 'module.php',
            ],
        ];
    }

    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/dashboard/' . $category, $message, $params, $language);
    }

    public function getConfig()
    {
        return [
            'bootstrap' => [
                'dashboard',
            ],
            'module'    => [
                'class'   => 'linex\\modules\\dashboard\\Module',
                'modules' => [
                    'main'    => [
                        'class'               => 'linex\\modules\\main\\Module',
                        'controllerNamespace' => 'linex\\modules\\main\\controllers\\backend',
                        'viewPath'            => '@vendor/linex/modules/main/views',
                    ],
                    'users'   => [
                        'class'               => 'linex\\modules\\users\\Module',
                        'controllerNamespace' => 'linex\\modules\\users\\controllers\\backend',
                        'viewPath'            => '@vendor/linex/modules/users/views',
                    ],
                    'pages'   => [
                        'class'               => 'linex\\modules\\pages\\Module',
                        'controllerNamespace' => 'linex\\modules\\pages\\controllers\\backend',
                        'viewPath'            => '@vendor/linex/modules/pages/views',
                    ],
                    'catalog' => [
                        'class'               => 'linex\\modules\\catalog\\Module',
                        'controllerNamespace' => 'linex\\modules\\catalog\\controllers\\backend',
                        'viewPath'            => '@vendor/linex/modules/catalog/views',
                    ],
                    'seo'     => [
                        'class'               => 'linex\\modules\\seo\\Module',
                        'controllerNamespace' => 'linex\\modules\\seo\\controllers\\backend',
                        'viewPath'            => '@vendor/linex/modules/seo/views',
                    ],
                ],
            ],
            'rules'     => [
                'backend' => [
                    'class'       => 'yii\\web\\GroupUrlRule',
                    'prefix'      => 'dashboard',
                    'routePrefix' => 'dashboard',
                    'rules'       => [
                        ''                                                                                       => 'default/index',
                        '<_a:(flush-cache|make-slug)>'                                                           => 'default/<_a>',
                        '<_c:(backend-menu)>'                                                                    => '<_c>/index',
                        '<_c:(backend-menu|redactor)>/<_a:(add|edit|delete|delete-all|images-get|image-upload)>' => '<_c>/<_a>',
                    ],
                ],
            ],
        ];
    }

    public function getName()
    {
        return self::t('Module Name');
    }
}