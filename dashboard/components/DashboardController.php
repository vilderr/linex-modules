<?php

namespace linex\modules\dashboard\components;

use Yii;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class DashboardController extends Controller
{
    public function init()
    {
        Yii::$app->user->loginUrl = '/dashboard/user/login';
    }

    public function behaviors()
    {
        return [
            'administrate' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['administrate'],
                    ],
                ],
            ],
            'verbs'        => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'     => ['post'],
                    'delete-all' => ['post'],
                ],
            ],
        ];
    }
}