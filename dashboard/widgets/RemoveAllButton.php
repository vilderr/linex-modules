<?php

namespace linex\modules\dashboard\widgets;

use yii\base\Widget;
use yii\helpers\Html;
use yii\base\InvalidParamException;

use kartik\icons\Icon;

use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\dashboard\widgets\assets\RemoveAllButton as Asset;

/**
 * Class RemoveAllButton
 * @package linex\modules\dashboard\widgets
 */
class RemoveAllButton extends Widget
{
    public $url;
    public $gridSelector;
    public $htmlOptions = [];
    public $modalSelector = '#delete-confirmation';

    public function init()
    {
        if (!isset($this->url, $this->gridSelector)) {
            throw new InvalidParamException('Attribute \'url\' or \'gridSelector\' is not set');
        }

        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = 'deleteItems';
        }
        Html::addCssClass($this->htmlOptions, 'btn');
    }

    /**
     * @return string
     */
    public function run()
    {
        $this->registerScript();

        return $this->renderButton();
    }

    /**
     * @return string
     */
    protected function renderButton()
    {
        return Html::button(
            Icon::show('trash-o') . ' ' .
            DashboardModule::t('Delete selected'),
            $this->htmlOptions
        );
    }

    protected function registerScript()
    {
        $view = $this->getView();
        Asset::register($view);

        $this->view->registerJs("
            jQuery('#{$this->htmlOptions['id']}').on('click', function() {
                var items =  $('{$this->gridSelector}').yiiGridView('getSelectedRows');
                if (items.length) {
                    jQuery('{$this->modalSelector}').attr('data-url', '{$this->url}').attr('data-items', items).modal('show');
                }
                return false;
            });
        ");
    }
}