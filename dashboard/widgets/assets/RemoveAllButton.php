<?php

namespace linex\modules\dashboard\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class RemoveAllButton
 * @package linex\modules\dashboard\widgets\assets
 */
class RemoveAllButton extends AssetBundle
{
    public $sourcePath = '@linex/modules/dashboard/widgets/dist';

    public $js = [
        'js/removeallbutton.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];
}