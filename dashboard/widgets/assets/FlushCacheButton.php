<?php

namespace linex\modules\dashboard\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class FlushCacheButton
 * @package linex\modules\dashboard\widgets\assets
 */
class FlushCacheButton extends AssetBundle
{
    public $sourcePath = '@linex/modules/dashboard/widgets/dist';

    public $js = [
        'js/flushcache.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}