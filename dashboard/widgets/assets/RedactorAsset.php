<?php

namespace linex\modules\dashboard\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class RedactorAsset
 * @package linex\modules\dashboard\widgets\assets
 */
class RedactorAsset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@linex/modules/dashboard/widgets/dist';

    /**
     * @var string Redactor language
     */
    public $language;

    /**
     * @var array Redactor plugins array
     */
    public $plugins = [];

    /**
     * @inheritdoc
     */
    public $css = [
        'css/redactor.css',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        'js/redactor.js',
    ];

    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];

    /**
     * Register asset bundle language files and plugins.
     */
    public function registerAssetFiles($view)
    {
        if ($this->language !== null) {
            $this->js[] = 'lang/' . $this->language . '.js';
        }
        if (!empty($this->plugins)) {
            foreach ($this->plugins as $plugin) {
                if ($plugin === 'clips') {
                    $this->css[] = 'plugins/' . $plugin . '/' . $plugin . '.css';
                }
                $this->js[] = 'plugins/' . $plugin . '/' . $plugin . '.js';
            }
        }
        parent::registerAssetFiles($view);
    }
}