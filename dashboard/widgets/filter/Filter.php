<?php

namespace linex\modules\dashboard\widgets\filter;

use Yii;
use yii\base\Model;
use yii\base\Widget;
use yii\base\InvalidConfigException;

/**
 * Class Filter
 * @package linex\modules\dashboard\widgets\filter
 */
class Filter extends Widget
{
    public $model;

    private $_errors = [];
    private $_request;

    public function init()
    {
        if (!$this->hasModel()) {
            throw new InvalidConfigException("'Model' must be specified and will be valid object");
        }
    }

    public function run()
    {
        $this->_request = Yii::$app->request;

        return $this->render('filter', [
            'model' => $this->model,
        ]);
    }


    protected function hasModel()
    {
        return $this->model instanceof Model;
    }
}