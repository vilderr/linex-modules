<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\main\models\search\SubdomainSearch
 */

use yii\helpers\Html;
use linex\modules\dashboard\components\ActiveForm;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\main\models\Subdomain;

?>
<div class="row">
    <div class="col-lg-8 col-md-9">
        <div class="panel panel-default flat">
            <div class="panel-heading">
                <h4 class="panel-title"><?= Yii::t('app', 'Filter'); ?></h4>
            </div>
            <? $form = ActiveForm::begin([
                'id'         => $model->id,
                'type'       => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                'method'     => 'get',
            ]); ?>
            <div class="panel-body">
                <?
                foreach (array_keys($model->attributes) as $attribute) {
                    switch ($attribute) {
                        case 'active':
                            echo $form->field($model, $attribute)->dropDownList(ModelHelper::statusList(), ['prompt' => Yii::t('app', 'Anyone')]);
                            break;
                        case 'subdomain_id':
                            echo $form->field($model, $attribute)->dropDownList(Subdomain::getList(), ['prompt' => Yii::t('app', 'Anyone')]);
                            break;
                        default:
                            echo $form->field($model, $attribute)->textInput();
                    }
                }
                ?>
            </div>
            <div class="panel-footer">
                <?=
                Html::submitButton(
                    Yii::t('app', 'Apply'),
                    [
                        'class' => 'btn btn-info btn-flat',
                        'name'  => 'action',
                        'value' => 'apply',
                    ]
                )
                ?>
                <?=
                Html::a(Yii::t('app', 'Reset'), ['index'], ['class' => 'btn btn-default btn-flat']); ?>
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>


