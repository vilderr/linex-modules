<?php
return [
    'Module Name' => 'Панель администратора',

    'Dashboard'               => 'Рабочий стол',
    'Dashboard menu settings' => 'Настройки главного меню',
    'Cache is flushed'        => 'Кеш очищен',
    'Assets are flushed'      => 'Ассеты очищены',
    'Parent level'            => 'Верхний уровень',
    '{item-name}: items'      => '{item-name}: пункты меню',
    'Up to parent'            => 'На уровень выше',

    'Flush cache'     => 'Очистить кеш',
    'Delete selected' => 'Удалить выбранные',
    'Add menu item'   => 'Добавить пункт меню',
    'Edit menu item'  => 'Редактировать пункт меню',

    'Are you sure you want to delete this object?' => 'Вы действительно хотите удлаить выбранные объекты?',
    'All data will be lost'                        => 'Данные будут безврозратно удалены',
    'Choose item'                                  => 'Выберите значение',

    'ID'          => 'Ид',
    'Name'        => 'Название',
    'Description' => 'Описание',
    'Slug'        => 'Символьный код',
    'Route'       => 'Роутинг',
    'Icon'        => 'Иконка',
    'Sort'        => 'Сортировка',
    'Permission'  => 'Разрешения',
    'Active'      => 'Активность',

    '{icon} Save'  => '{icon} Сохранить',
    '{icon} Apply' => '{icon} Применить',
];