<?php

namespace linex\modules\dashboard;

use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/dashboard/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/dashboard/messages',
            'fileMap'          => [
                'modules/dashboard/module' => 'module.php',
            ],
        ];
    }
}