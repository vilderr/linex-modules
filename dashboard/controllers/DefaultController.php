<?php

namespace linex\modules\dashboard\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use linex\modules\main\helpers\StringHelper;

use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\dashboard\components\DashboardController;
use linex\modules\dashboard\actions\FlushCache;

/**
 * Class DefaultController
 * @package linex\modules\dashboard\controllers
 */
class DefaultController extends DashboardController
{
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['flush-cache'],
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['flush-cache'],
                        'roles'   => ['cache manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }

    /**
     * @return array
     */
    public function actions()
    {
        return [
            'flush-cache' => [
                'class' => FlushCache::className(),
            ],
        ];
    }

    public function actionIndex()
    {
        $this->view->title = DashboardModule::getInstance()->getName();

        return $this->render('index');
    }

    public function actionMakeSlug($str)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return StringHelper::translit($str);
    }
}