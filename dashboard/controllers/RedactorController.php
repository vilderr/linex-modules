<?php

namespace linex\modules\dashboard\controllers;

use linex\modules\dashboard\components\DashboardController;
use linex\modules\dashboard\widgets\actions\RedactorGetAction as GetAction;

/**
 * Class RedactorController
 * @package linex\modules\dashboard\controllers
 */
class RedactorController extends DashboardController
{
    /**
     * @return array
     */
    public function actions()
    {
        return [
            'image-upload' => [
                'class' => 'linex\modules\dashboard\widgets\actions\RedactorUploadAction',
                'url'   => '/upload/redactor',
                'path'  => '@uploads/redactor' // Or absolute path to directory where files are stored.
            ],
            'images-get'   => [
                'class' => 'linex\modules\dashboard\widgets\actions\RedactorGetAction',
                'url'   => '/upload/redactor',
                'path'  => '@uploads/redactor', // Or absolute path to directory where files are stored.
                'type'  => GetAction::TYPE_IMAGES,
            ],
        ];
    }
}