<?php

namespace linex\modules\dashboard\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use linex\modules\dashboard\components\DashboardController;
use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\dashboard\models\DashboardMenu;

/**
 * Class BackendMenuController
 * @package linex\modules\dashboard\controllers
 */
class BackendMenuController extends DashboardController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['setting manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }

    /**
     * @param int $parent_id
     *
     * @return string
     */
    public function actionIndex($parent_id = 0)
    {
        if (($model = DashboardMenu::findOne($parent_id)) === null) {
            $model = new DashboardMenu();
            $model->id = 0;
            $model->name = DashboardModule::t('Parent level');
            $model->loadDefaultValues();
        }

        $dataProvider = $model->getProvider();
        $navChain = DashboardMenu::getNavChain($model);

        $this->view->title = DashboardModule::t('Dashboard menu settings');
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->view->title,
            'url'   => empty($navChain) ? null : Url::to(['/dashboard/backend-menu']),
        ];

        foreach ($navChain as $chain) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $chain->name,
                'url'   => ($model->id != $chain->id) ? Url::to(['/dashboard/backend-menu', 'parent_id' => $chain->id]) : null,
            ];
        }

        return $this->render(
            'index',
            [
                'dataProvider' => $dataProvider,
                'model'        => $model,
                'navChain'     => $navChain,
            ]
        );
    }


    /**
     * @param int $parent_id
     *
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionAdd($parent_id = 0)
    {
        $parent_id = intval($parent_id);
        if ($parent_id > 0) {
            $rootModel = $this->findModel($parent_id);
        }

        $model = new DashboardMenu();
        $model->loadDefaultValues();
        $model->parent_id = $parent_id;
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->validate()) {
            if ($model->save()) {
                $returnUrl = Yii::$app->request->get('returnUrl', [
                    Url::toRoute('index'),
                    'parent_id' => $model->parent_id,
                ]);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'back':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect(
                            Url::toRoute(
                                [
                                    'edit',
                                    'id'        => $model->id,
                                    'returnUrl' => $returnUrl,
                                ]
                            )
                        );
                }
            } else {
                throw new ServerErrorHttpException;
            }
        }

        $this->view->title = DashboardModule::t('Add menu item');

        $this->view->params['breadcrumbs'][] = [
            'label' => DashboardModule::t('Dashboard menu settings'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionEdit($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->validate()) {
            if ($model->save()) {
                $returnUrl = Yii::$app->request->get('returnUrl', [
                    Url::toRoute([
                        'index',
                        'parent_id' => $model->parent_id,
                    ]),
                ]);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'back':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect(
                            Url::toRoute(
                                [
                                    'edit',
                                    'id'        => $model->id,
                                    'returnUrl' => $returnUrl,
                                ]
                            )
                        );
                }
            } else {
                throw new ServerErrorHttpException;
            }
        }

        $this->view->title = DashboardModule::t('Edit menu item');

        $this->view->params['breadcrumbs'][] = [
            'label' => DashboardModule::t('Dashboard menu settings'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('edit', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'The data was successfully deleted!'));
        }

        return $this->redirect(Url::to(['index', 'parent_id' => $model->parent_id]));
    }

    /**
     * @param $parent_id
     *
     * @return \yii\web\Response
     */
    public function actionDeleteAll($parent_id)
    {
        $items = Yii::$app->request->post('items', []);
        if (!empty($items)) {
            $items = DashboardMenu::find()->where(['in', 'id', $items])->all();
            foreach ($items as $item) {
                $item->delete();
            }
        }

        return $this->redirect(['index', 'parent_id' => $parent_id]);
    }

    /**
     * @param $id
     *
     * @return DashboardMenu
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if (($model = DashboardMenu::findById($id)) === null) {
            throw new NotFoundHttpException;
        }

        return $model;
    }
}