<?php

namespace linex\modules\catalog\repositories;

use linex\modules\catalog\repositories\exceptions\NotFoundException;
use linex\modules\catalog\models\Tag;

/**
 * Class TagRepository
 * @package linex\modules\catalog\repositories
 */
final class TagRepository
{
    /**
     * @param $id
     *
     * @return Tag
     */
    public function get($id)
    {
        if (!$tag = Tag::findOne($id)) {
            throw new NotFoundException('Tag is not found.');
        }

        return $tag;
    }

    /**
     * @param $name
     *
     * @return Tag
     */
    public function findByName($name)
    {
        return Tag::findOne(['name' => $name]);
    }

    /**
     * @param $slug
     *
     * @return Tag
     */
    public function findBySlug($slug)
    {
        return Tag::findOne(['slug' => $slug]);
    }

    /**
     * @param Tag $tag
     */
    public function save(Tag $tag)
    {
        if (!$tag->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }

    /**
     * @param Tag $tag
     */
    public function remove(Tag $tag)
    {
        if (!$tag->delete()) {
            throw new \RuntimeException('Removing error.');
        }
    }
}