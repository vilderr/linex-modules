<?php

namespace linex\modules\catalog\repositories;

use linex\modules\catalog\models\PropertyValue;
use linex\modules\catalog\repositories\exceptions\NotFoundException;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class PropertyValueRepository
 * @package linex\modules\catalog\repositories
 */
class PropertyValueRepository
{
    /**
     * @param $id
     *
     * @return PropertyValue
     */
    public function get($id)
    {
        if (!$product = PropertyValue::findOne($id)) {
            throw new NotFoundException(CatalogModule::t('Element is not found'));
        }

        return $product;
    }

    /**
     * @param PropertyValue $value
     */
    public function save(PropertyValue $value)
    {
        if (!$value->save()) {
            \Yii::info(print_r($value->getErrors(), true), 'info');
            throw new \RuntimeException(CatalogModule::t('Saving error'));
        }
    }

    /**
     * @param PropertyValue $value
     */
    public function remove(PropertyValue $value)
    {
        if (!$value->delete()) {
            throw new \RuntimeException(CatalogModule::t('Removing error'));
        }
    }
}