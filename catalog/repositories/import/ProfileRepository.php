<?php

namespace linex\modules\catalog\repositories\import;

use linex\modules\catalog\models\import\Profile;
use linex\modules\catalog\repositories\exceptions\NotFoundException;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class ImportProfileRepository
 * @package linex\modules\catalog\repositories\import
 */
class ProfileRepository
{
    /**
     * @param $id
     *
     * @return Profile
     */
    public function get($id)
    {
        if (!$profile = Profile::findOne($id)) {
            throw new NotFoundException(CatalogModule::t('Element is not found'));
        }

        return $profile;
    }

    /**
     * @param Profile $profile
     *
     * @return void
     */
    public function save(Profile $profile)
    {
        if (!$profile->save()) {
            throw new \RuntimeException(CatalogModule::t('Saving error'));
        }
    }

    /**
     * @param Profile $profile
     *
     * @return void
     */
    public function remove(Profile $profile)
    {
        if (!$profile->delete()) {
            throw new \RuntimeException(CatalogModule::t('Removing error'));
        }
    }
}