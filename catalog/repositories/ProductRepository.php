<?php

namespace linex\modules\catalog\repositories;

use linex\modules\catalog\models\Product;
use linex\modules\catalog\Module as CatalogModule;
use yii\web\NotFoundHttpException;

/**
 * Class ProductRepository
 * @package linex\modules\catalog\repositories
 */
final class ProductRepository extends BaseProductRepository
{
    /**
     * @param $id
     *
     * @return Product
     * @throws NotFoundHttpException
     */
    public function get($id)
    {
        if (!$product = Product::findOne($id)) {
            throw new NotFoundHttpException(CatalogModule::t('Element is not found'));
        }

        return $product;
    }

    /**
     * @param $id
     *
     * @return array|null|\yii\db\ActiveRecord
     * @throws NotFoundHttpException
     */
    public function getWithCategory($id)
    {
        if (!$product = Product::find()->where(['id' => $id])->with('category')->one()) {
            throw new NotFoundHttpException(CatalogModule::t('Element is not found'));
        }

        return $product;
    }

    /**
     * @param Product $product
     */
    public function save(Product $product)
    {
        if (!$product->save()) {
            throw new \RuntimeException(CatalogModule::t('Saving error'));
        }
    }

    /**
     * @param Product $product
     */
    public function remove(Product $product)
    {
        if (!$product->delete()) {
            throw new \RuntimeException(CatalogModule::t('Removing error'));
        }
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function existsByCategory($id)
    {
        return Product::find()->andWhere(['category_id' => $id])->exists();
    }
}