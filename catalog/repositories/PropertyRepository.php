<?php

namespace linex\modules\catalog\repositories;

use linex\modules\catalog\models\Property;
use linex\modules\catalog\repositories\exceptions\NotFoundException;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class PropertyRepository
 * @package linex\modules\catalog\repositories
 */
final class PropertyRepository
{
    /**
     * @param $id
     *
     * @return Property
     */
    public function get($id)
    {
        if (!$product = Property::findOne($id)) {
            throw new NotFoundException(CatalogModule::t('Element is not found'));
        }

        return $product;
    }

    /**
     * @param Property $property
     *
     * @return void
     */
    public function save(Property $property)
    {
        if (!$property->save()) {
            throw new \RuntimeException(CatalogModule::t('Saving error'));
        }
    }

    /**
     * @param Property $property
     */
    public function remove(Property $property)
    {
        if (!$property->delete()) {
            throw new \RuntimeException(CatalogModule::t('Removing error'));
        }
    }
}