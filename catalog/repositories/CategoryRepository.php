<?php

namespace linex\modules\catalog\repositories;

use linex\modules\catalog\models\Category;
use linex\modules\catalog\repositories\exceptions\NotFoundException;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class CategoryRepository
 * @package linex\modules\catalog\repositories
 */
final class CategoryRepository
{
    /**
     * @param $id
     *
     * @return Category
     */
    public function get($id)
    {
        if (!$category = Category::findOne($id)) {
            throw new NotFoundException(CatalogModule::t('Category is not found'));
        }

        return $category;
    }

    /**
     * @param Category $category
     */
    public function save(Category $category)
    {
        if (!$category->save()) {
            throw new \RuntimeException(CatalogModule::t('Saving error'));
        }
    }

    /**
     * @param Category $category
     */
    public function remove(Category $category)
    {
        if (!$category->deleteWithChildren()) {
            throw new \RuntimeException(CatalogModule::t('Removing error'));
        }
    }
}