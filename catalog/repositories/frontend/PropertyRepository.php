<?php

namespace linex\modules\catalog\repositories\frontend;

use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\CategoryProperty;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class PropertyRepository
{
    public $items = null;

    public function normalizeForCategory(Category $category, array $properties)
    {
        $result = [];

        $dbItems = ArrayHelper::getColumn($this->getItems($category), 'slug');

        foreach ($properties['sef'] as $CODE => $property) {
            if (!ArrayHelper::isIn($CODE, $dbItems))
            {
                throw new NotFoundHttpException();
            }
        }

        //echo '<pre>';print_r($dbItems);echo '</pre>';

        return $result;
    }

    /**
     * @param Category $category
     *
     * @return array
     */
    private function getItems(Category $category)
    {
        if ($this->items === null) {
            $this->items = CategoryProperty::getArray($category);
        }

        return $this->items;
    }
}