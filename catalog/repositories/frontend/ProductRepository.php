<?php

namespace linex\modules\catalog\repositories\frontend;

use Yii;
use yii\db\ActiveQuery;
use yii\data\ActiveDataProvider;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\ProductValue;

/**
 * Class ProductRepository
 * @package linex\modules\catalog\repositories\frontend
 */
class ProductRepository
{
    public function getList(Category $category = null, array $properties = [], array $fields = [])
    {
        if (!$category) {
            $category = Category::findOne(['id' => 1]);
        }

        $query = Product::find()->alias('P');
        $query->joinWith('category C', false);
        $query->where([
            'and',
            ['P.active' => ModelHelper::STATUS_ACTIVE],
            ['C.global_active' => ModelHelper::STATUS_ACTIVE],
            ['>=', 'C.lft', $category->lft],
            ['<=', 'C.rgt', $category->rgt],
        ]);

        foreach ($properties['all'] as $PID => $value) {
            $alias = 'PV' . $PID;
            $query->innerJoin(ProductValue::tableName() . $alias, $alias . '.product_id = P.id');
            $query->andWhere([$alias . '.property_id' => $PID, $alias . '.value_id' => $value['id']]);
        }

        return $this->getProvider($query, $category, $properties);
    }

    /**
     * @param ActiveQuery $query
     *
     * @return ActiveDataProvider
     */
    private function getProvider(ActiveQuery $query, Category $category, array $properties)
    {
        return new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'route'           => \Yii::$app->controller->route,
                'params'          => [
                    'code'  => $category->slug,
                    'sef'   => $properties['sef'],
                    'query' => $properties['query'],
                ],
                'page' => intval(Yii::$app->request->get('page')),
                'forcePageParam'  => false,
                'defaultPageSize' => 30,
                'pageSizeLimit'   => [15, 100],
            ],
        ]);
    }
}