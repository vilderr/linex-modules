<?php

namespace linex\modules\catalog\repositories\distribution;

use linex\modules\catalog\models\distribution\Part;
use linex\modules\catalog\repositories\exceptions\NotFoundException;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class PartRepository
 * @package linex\modules\catalog\repositories\distribution
 */
final class PartRepository
{
    /**
     * @param $id
     *
     * @return Part
     */
    public function get($id)
    {
        if (!$part = Part::findOne($id)) {
            throw new NotFoundException(CatalogModule::t('Element is not found'));
        }

        return $part;
    }
}