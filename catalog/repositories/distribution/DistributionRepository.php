<?php

namespace linex\modules\catalog\repositories\distribution;

use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\repositories\exceptions\NotFoundException;

/**
 * Class DistributionRepository
 * @package linex\modules\catalog\repositories\distribution
 */
final class DistributionRepository
{
    /**
     * @param $id
     *
     * @return Distribution
     */
    public function get($id)
    {
        if (!$distribution = Distribution::findOne($id)) {
            throw new NotFoundException(CatalogModule::t('Element is not found'));
        }

        return $distribution;
    }

    /**
     * @param Distribution $distribution
     *
     * @return void
     */
    public function save(Distribution $distribution)
    {
        if (!$distribution->save()) {
            throw new \RuntimeException(CatalogModule::t('Saving error'));
        }
    }

    /**
     * @param Distribution $distribution
     */
    public function remove(Distribution $distribution)
    {
        if (!$distribution->delete()) {
            throw new \RuntimeException(CatalogModule::t('Removing error'));
        }
    }
}