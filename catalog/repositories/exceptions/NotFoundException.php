<?php

namespace linex\modules\catalog\repositories\exceptions;
/**
 * Class NotFoundException
 * @package linex\modules\catalog\repositories\exceptions
 */
class NotFoundException extends \DomainException
{

}