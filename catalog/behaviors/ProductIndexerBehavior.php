<?php
namespace linex\modules\catalog\behaviors;

use linex\modules\catalog\models\Product;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use linex\modules\catalog\models\propertyindex\Indexer;

/**
 * Class ProductIndexerBehavior
 * @package linex\modules\catalog\behaviors
 */
class ProductIndexerBehavior extends Behavior
{
    /** @var Indexer */
    protected $indexer;

    public function init()
    {
        $this->indexer = new Indexer();
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'updateProductIndex'
        ];
    }

    public function updateProductIndex(Event $event)
    {
        /** @var Product $product */
        $product = $event->sender;
        $this->indexer->updateProductIndex($product);
    }
}