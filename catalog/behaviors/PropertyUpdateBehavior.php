<?php

namespace linex\modules\catalog\behaviors;

use linex\modules\catalog\models\CategoryProperty;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\propertyindex\ProductIndex;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;

/**
 * Class PropertyUpdateBehavior
 * @package linex\modules\catalog\behaviors
 */
class PropertyUpdateBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE => 'onAfterUpdate'
        ];
    }

    public function onAfterUpdate(Event $event)
    {
        $model = $event->sender;

        if (!$model->filtrable)
        {
            CategoryProperty::updateAll(['filtrable' => $model->filtrable], ['property_id' => $model->id]);
            ProductIndex::deleteAll(['property_id' => $model->id]);
        }
    }
}