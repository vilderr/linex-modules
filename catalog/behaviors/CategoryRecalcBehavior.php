<?php

namespace linex\modules\catalog\behaviors;

use linex\modules\catalog\models\Product;
use Yii;
use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\db\Query;
use linex\modules\catalog\models\Category;

/**
 * Класс обработчик для событий добавления/обновления/удаления категорий товаров
 *
 * @package linex\modules\catalog\behaviors
 */
class CategoryRecalcBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_DELETE => 'onBeforeDelete',
            ActiveRecord::EVENT_AFTER_UPDATE  => 'onAfterUpdate',
        ];
    }

    public function onBeforeDelete(Event $event)
    {
        /** @var Category $category */
        $category = $event->sender;

        return Product::deleteAll(['category_id' => $category->id]);
    }

    /**
     * @param Event $event
     */
    public function onAfterUpdate(Event $event)
    {
        /** @var Category $category */
        $category = $event->sender;

        $this->recalcGlobalActive($category);
    }

    /**
     * Пересчет глобальной активности категории и ее вложенных подкатегорий
     *
     * @param Category $category
     */
    private function recalcGlobalActive(Category $category)
    {
        Yii::$app->db->createCommand()->update(Category::tableName(), ['global_active' => 1], 'lft >= :lft AND rgt <= :rgt', [':lft' => $category->lft, ':rgt' => $category->rgt])->execute();

        $q = new Query();
        $q
            ->select([
                'lft',
                'rgt',
            ])
            ->from(Category::tableName())
            ->where('lft >= :lft', [':lft' => $category->lft])
            ->andWhere('rgt <= :rgt', [':rgt' => $category->rgt])
            ->andWhere(['active' => 0])
            ->orderBy('lft');

        $upd = [];
        $prev_rgt = 0;
        foreach ($q->each() as $child) {
            /** @var Category $child */
            if ($child['rgt'] > $prev_rgt) {
                $prev_rgt = $child['rgt'];
                $upd[] = '(lft >= ' . $child['lft'] . ' AND rgt <= ' . $child['rgt'] . ')';
            }
        }

        if (count($upd) > 0) {
            Yii::$app->db->createCommand()->update(Category::tableName(), ['global_active' => 0], implode(' OR ', $upd))->execute();
        }
    }
}