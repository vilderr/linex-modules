<?php

namespace linex\modules\catalog\behaviors;


use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\Event;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use linex\modules\catalog\entities\Meta;

/**
 * Class MetaBehavior
 * @package linex\modules\catalog\behaviors
 */
class MetaBehavior extends Behavior
{
    public $attribute = 'meta';
    public $jsonAttribute = 'meta_json';

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND    => 'onAfterFind',
            ActiveRecord::EVENT_BEFORE_INSERT => 'onBeforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeSave',
        ];
    }

    public function onAfterFind(Event $event)
    {
        $model = $event->sender;
        $meta = Json::decode($model->getAttribute($this->jsonAttribute));
        $model->{$this->attribute} = new Meta(
            ArrayHelper::getValue($meta, 'h1'),
            ArrayHelper::getValue($meta, 'title'),
            ArrayHelper::getValue($meta, 'description'),
            ArrayHelper::getValue($meta, 'keywords')
        );
    }

    public function onBeforeSave(Event $event)
    {
        $model = $event->sender;
        $model->setAttribute('meta_json', Json::encode([
            'h1'          => $model->{$this->attribute}->h1,
            'title'       => $model->{$this->attribute}->title,
            'description' => $model->{$this->attribute}->description,
            'keywords'    => $model->{$this->attribute}->keywords,
        ]));
    }
}