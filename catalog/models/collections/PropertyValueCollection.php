<?php

namespace linex\modules\catalog\models\collections;

use yii\base\Model;
use linex\modules\main\validators\SlugValidator;
use linex\modules\catalog\models\PropertyValue;

/**
 * Class PropertyValueCollection
 * @package linex\modules\catalog\models\collections
 */
class PropertyValueCollection extends Model
{
    public $property_id;
    public $name;
    public $slug;
    public $xml_id;
    public $sort;
    public $active;

    private $_value;

    /**
     * PropertyValueCollection constructor.
     *
     * @param PropertyValue|null $value
     * @param array              $config
     */
    public function __construct(PropertyValue $value = null, array $config = [])
    {
        if ($value) {
            $this->property_id = $value->property_id;
            $this->name = $value->name;
            $this->slug = $value->slug;
            $this->xml_id = $value->xml_id;
            $this->sort = $value->sort;
            $this->active = $value->active;

            $this->_value = $value;
        } else {
            $this->sort = PropertyValue::DEFAULT_SORT;
            $this->active = PropertyValue::DEFAULT_ACTIVE;
        }
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['property_id', 'name', 'slug', 'xml_id'], 'required'],
            [['name', 'slug', 'xml_id'], 'string', 'max' => 255],
            [['slug', 'xml_id'], SlugValidator::class],
            [['slug', 'xml_id'], 'unique', 'targetClass' => PropertyValue::class, 'filter' => $this->_value ? ['<>', 'id', $this->_value->id] : null],
            [['sort'], 'integer'],
            [['active'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new PropertyValue())->attributeLabels();
    }
}