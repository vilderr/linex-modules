<?php

namespace linex\modules\catalog\models\collections;

use linex\modules\catalog\models\Tag;
use linex\modules\main\helpers\ModelHelper;
use yii\base\Model;
use linex\modules\main\validators\SlugValidator;

/**
 * Class TagCollection
 * @package linex\modules\catalog\models\collections
 */
class TagCollection extends Model
{
    public $name;
    public $slug;
    public $sort;
    public $active;

    private $_tag;
    private $_model;

    public function __construct(Tag $tag = null, array $config = [])
    {
        $this->_model = (new Tag())->loadDefaultValues();

        if ($tag) {
            $this->name = $tag->name;
            $this->slug = $tag->slug;
            $this->sort = $tag->sort;
            $this->active = $tag->active;

            $this->_tag = $tag;
        } else {
            $this->load([$this->formName() => $this->_model->attributes]);
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Tag())->attributeLabels();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug'], 'string', 'max' => 255],
            ['slug', SlugValidator::class],
            [['name', 'slug'], 'unique', 'targetClass' => Tag::class, 'filter' => $this->_tag ? ['<>', 'id', $this->_tag->id] : null],
            [['sort', 'active'], 'integer'],
        ];
    }
}