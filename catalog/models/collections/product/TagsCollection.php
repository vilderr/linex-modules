<?php

namespace linex\modules\catalog\models\collections\product;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Tag;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class TagsCollection
 * @package linex\modules\catalog\models\collections\product
 */
class TagsCollection extends Model
{
    public $values = [];

    /**
     * TagsCollection constructor.
     *
     * @param Product|null $product
     * @param array        $config
     */
    public function __construct(Product $product = null, $config = [])
    {
        if ($product) {
            $this->values = ArrayHelper::getColumn($product->tagAssignments, 'tag_id');
        }
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['values', 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'values' => CatalogModule::t('Tags'),
        ];
    }

    /**
     * @return array
     */
    public function tagsList()
    {
        return ArrayHelper::map(Tag::find()->orderBy('name')->asArray()->all(), 'id', 'name');
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->values = array_filter((array)$this->values);

        return parent::beforeValidate();
    }
}