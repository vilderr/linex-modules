<?php

namespace linex\modules\catalog\models\collections\distribution;

use yii\base\Model;
use yii\helpers\ArrayHelper;
use linex\modules\catalog\models\distribution\Part;
use linex\modules\catalog\models\Tag;

/**
 * Class PartCollection
 * @package linex\modules\catalog\models\collections\distribution
 */
class PartCollection extends Model
{
    public $sort;
    public $active;

    public $filter_name;
    public $filter_category_id;
    public $filter_active;
    public $filter_current_props;
    public $filter_current_section;
    public $filter_price_from;
    public $filter_price_to;
    public $operation_category_id;
    public $operation_active;
    public $operation_tags;

    /**
     * PartCollection constructor.
     *
     * @param Part|null $part
     * @param array     $config
     */
    public function __construct(Part $part = null, array $config = [])
    {
        if ($part) {
            $this->filter_name = $part->filter_name;
            $this->filter_category_id = $part->filter_category_id;
            $this->filter_active = $part->filter_active;
            $this->filter_current_props = $part->filter_current_props;
            $this->filter_current_section = $part->filter_current_section;
            $this->filter_price_from = $part->filter_price_from;
            $this->filter_price_to = $part->filter_price_to;
            $this->operation_category_id = $part->operation_category_id;
            $this->operation_active = $part->operation_active;
            $this->operation_tags = $part->operation_tags;
            $this->sort = $part->sort;
            $this->active = $part->active;
        } else {
            $model = (new Part())->loadDefaultValues();
            $this->load([$this->formName() => $model->attributes]);
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['filter_name', 'filter_current_props', 'filter_current_section'], 'string'],
            [['filter_category_id', 'filter_active', 'filter_price_from', 'filter_price_to', 'operation_category_id', 'operation_active', 'sort', 'active'], 'integer'],
            [['operation_tags'], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Part())->attributeLabels();
    }

    /**
     * @return array
     */
    public function tagsList()
    {
        return ArrayHelper::map(Tag::find()->orderBy('name')->asArray()->all(), 'id', 'name');
    }
}