<?php

namespace linex\modules\catalog\models\collections\distribution;

use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\models\distribution\Part;
use linex\modules\catalog\models\collections\BaseCollection;

/**
 * Class DistributionCollection
 * @package linex\modules\catalog\models\collections\distribution
 */
class DistributionCollection extends BaseCollection
{
    public $name;
    public $description;
    public $sort;
    public $active;

    public $distribution = null;

    /**
     * DistributionCollection constructor.
     *
     * @param Distribution|null $distribution
     * @param array             $config
     */
    public function __construct(Distribution $distribution = null, array $config = [])
    {
        if ($distribution) {
            $this->name = $distribution->name;
            $this->description = $distribution->description;
            $this->sort = $distribution->sort;
            $this->active = $distribution->active;

            $this->parts = array_map(function (Part $part) {
                return new PartCollection($part);
            }, $distribution->getParts()->indexBy('id')->all());

            $this->distribution = $distribution;
        } else {
            $this->sort = Distribution::DEFAULT_SORT;
            $this->active = Distribution::DEFAULT_ACTIVE;
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return (new Distribution())->rules();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Distribution())->attributeLabels();
    }

    /**
     * @return array
     */
    public function internalCollections()
    {
        if ($this->name) {
            return ['parts'];
        }

        return [];
    }
}