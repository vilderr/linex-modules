<?php

namespace linex\modules\catalog\models\collections;

use yii\helpers\ArrayHelper;
use yii\base\Model;
use linex\modules\catalog\models\Property;

/**
 * Class ProductValueCollection
 * @package linex\modules\catalog\models\collections
 */
class ProductValueCollection extends Model
{
    public $items = [];

    public $property;

    /**
     * ProductValueCollection constructor.
     *
     * @param Property $property
     * @param array    $values
     * @param array    $config
     */
    public function __construct(Property $property, $values = [], $config = [])
    {
        if (is_array($values)) {
            $this->items = $values;
        }

        $this->property = $property;
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['items', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'items' => $this->property->name,
        ];
    }

    public function variants()
    {
        return ArrayHelper::map($this->property->values, 'id', 'name');
    }
}