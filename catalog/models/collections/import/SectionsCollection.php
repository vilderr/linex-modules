<?php

namespace linex\modules\catalog\models\collections\import;

use yii\helpers\ArrayHelper;
use linex\modules\catalog\models\import\Profile;
use yii\base\Model;
use linex\modules\catalog\Module as CatalogModule;
use yii\httpclient\Client;

/**
 * Class SectionsCollection
 * @package linex\modules\catalog\models\collections\import
 */
class SectionsCollection extends Model
{
    public $values = [];

    private $_profile;

    public function __construct(Profile $profile, array $config = [])
    {
        $this->values = ArrayHelper::getColumn($profile->sections, 'value');
        $this->_profile = $profile;

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            ['values', 'each', 'rule' => ['integer']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'values' => CatalogModule::t('Categories'),
        ];
    }
}