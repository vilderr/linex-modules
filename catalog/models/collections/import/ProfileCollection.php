<?php

namespace linex\modules\catalog\models\collections\import;

use yii\base\Model;
use linex\modules\catalog\models\collections\BaseCollection;
use linex\modules\catalog\models\import\Profile;

/**
 * Class ImportProfileCollection
 * @package linex\modules\catalog\models\collections\import
 */
class ProfileCollection extends BaseCollection
{
    public $name;
    public $sections_url;
    public $products_url;
    public $api_key;
    public $sort;
    public $active;

    private $_profile;

    /**
     * ImportProfileCollection constructor.
     *
     * @param Profile|null $profile
     * @param array        $config
     */
    public function __construct(Profile $profile = null, array $config = [])
    {
        if ($profile) {
            $this->name = $profile->name;
            $this->sections_url = $profile->sections_url;
            $this->products_url = $profile->products_url;
            $this->api_key = $profile->api_key;
            $this->sort = $profile->sort;
            $this->active = $profile->active;
            $this->sections = new SectionsCollection($profile);

            $this->_profile = $profile;
        } else {
            $this->sections_url = 'baseproducts.ru/rest/import/sections';
            $this->products_url = 'baseproducts.ru/rest/import/products';
            $this->sort = Profile::DEFAULT_SORT;
            $this->active = Profile::DEFAULT_ACTIVE;
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return (new Profile())->rules();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Profile())->attributeLabels();
    }

    public function isNewRecord()
    {
        return null === $this->_profile;
    }

    public function internalCollections()
    {
        return ['sections'];
    }
}