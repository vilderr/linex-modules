<?php

namespace linex\modules\catalog\models\collections;

use linex\modules\catalog\models\CategoryProperty;
use Yii;
use yii\helpers\ArrayHelper;

use linex\modules\catalog\models\Category;
use linex\modules\main\validators\SlugValidator;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\dashboard\Module as DashboardModule;

/**
 * Class CategoryCollecter
 * @package linex\modules\catalog\models\collections
 */
class CategoryCollection extends BaseCollection
{
    public $name;
    public $slug;
    public $xml_id;
    public $description;
    public $sort;
    public $active;
    public $global_active;
    public $created_by;
    public $parent_id;

    private $_category;

    public $categoryExist = true;

    /**
     * CategoryCollection constructor.
     *
     * @param Category      $parent
     * @param Category|null $category
     * @param array         $config
     */
    public function __construct(Category $parent, Category $category = null, array $config = [])
    {
        if ($category) {
            $this->name = $category->name;
            $this->slug = $category->slug;
            $this->xml_id = $category->xml_id;
            $this->description = $category->description;
            $this->sort = $category->sort;
            $this->active = $category->active;
            $this->created_by = $category->created_by;
            $this->_category = $category;
            $this->properties = new CategoryPropertyCollection($category);
        } else {
            $this->sort = Category::DEFAULT_SORT;
            $this->active = Category::DEFAULT_ACTIVE;
            $this->global_active = $parent->global_active ? 1 : 0;
            $this->created_by = Yii::$app->user->id;
            $this->properties = new CategoryPropertyCollection($parent);

            $this->categoryExist = false;
        }

        $this->parent_id = $parent->id;

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'required'],
            [['name', 'slug', 'xml_id'], 'string', 'max' => 255],
            [['description'], 'string'],
            [['slug', 'xml_id'], SlugValidator::class],
            [['name', 'slug'], 'unique', 'targetClass' => Category::class, 'filter' => $this->_category ? ['<>', 'id', $this->_category->id] : null],
            [['sort', 'parent_id'], 'integer'],
            ['parent_id', 'exist', 'targetClass' => Category::class, 'targetAttribute' => 'id'],
            [['active', 'global_active'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'name'        => DashboardModule::t('Name'),
            'slug'        => DashboardModule::t('Slug'),
            'xml_id'      => CatalogModule::t('Xml Id'),
            'description' => DashboardModule::t('Description'),
            'parent_id'   => CatalogModule::t('Parent category'),
            'sort'        => DashboardModule::t('Sort'),
            'active'      => DashboardModule::t('Active'),
        ];
    }

    /**
     * @return array
     */
    public function categoriesList()
    {
        return ArrayHelper::map(Category::find()->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 0 ? str_repeat('-- ', $category['depth']) . ' ' : '') . $category['name'];
        });
    }

    /**
     * @return array
     */
    public function internalCollections()
    {
        return [
            'properties',
        ];
    }
}