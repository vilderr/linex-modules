<?php

namespace linex\modules\catalog\models\collections;

use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\CategoryProperty;
use linex\modules\catalog\models\Property;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class CategoryPropertyCollection
 * @package linex\modules\catalog\models\collections
 */
class CategoryPropertyCollection extends Model
{
    public $values = [];

    public function __construct(Category $category = null, array $config = [])
    {
        $this->values = ArrayHelper::getColumn(CategoryProperty::getArray($category, true), function ($row) {
            return $row['property_id'];
        });

        parent::__construct($config);
    }

    public function variants()
    {
        return ArrayHelper::map(Property::find()->orderBy('sort')->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            ['values', 'each', 'rule' => ['integer']],
        ];
    }
}