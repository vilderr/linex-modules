<?php

namespace linex\modules\catalog\models\collections;

use linex\modules\main\models\collections\FileCollection;
use Yii;
use yii\helpers\ArrayHelper;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\collections\product\TagsCollection;
use linex\modules\main\validators\SlugValidator;

/**
 * Class ProductCollection
 *
 * @package linex\modules\catalog\models\collections
 *
 * @property ProductValueCollection[] $values
 */
class ProductCollection extends BaseCollection
{
    public $category_id;
    public $name;
    public $xml_id;
    public $tmp_id;
    public $description;
    public $current_props;
    public $current_section;
    public $url;
    public $price;
    public $old_price;
    public $discount;
    public $sort;
    public $active;
    public $created_by;

    private $_product;

    /**
     * ProductCollection constructor.
     *
     * @param Product|null $product
     * @param array        $config
     */
    public function __construct(Product $product = null, array $config = [])
    {
        if ($product) {
            $this->category_id = $product->category_id;
            $this->name = $product->name;
            $this->xml_id = $product->xml_id;
            $this->tmp_id = $product->tmp_id;
            $this->description = $product->description;
            $this->current_props = $product->current_props;
            $this->current_section = $product->current_section;
            $this->url = $product->url;
            $this->price = $product->price;
            $this->old_price = $product->old_price;
            $this->discount = $product->discount;
            $this->sort = $product->sort;
            $this->active = $product->active;
            $this->created_by = $product->created_by;

            $this->tags = new TagsCollection($product);
            $this->image = new FileCollection();
            $this->values = array_map(function (Property $property) use ($product) {
                return new ProductValueCollection($property, ArrayHelper::getColumn($product->getPropertyValues($property->id), 'value_id'));
            }, Property::find()->orderBy('sort')->indexBy('xml_id')->all());

            $this->_product = $product;
        } else {
            $this->sort = Product::DEFAULT_SORT;
            $this->active = Product::DEFAULT_ACTIVE;
            $this->created_by = Yii::$app->user->id;
            $this->category_id = 1;

            $this->tags = new TagsCollection();
            $this->image = new FileCollection();
            $this->values = array_map(function (Property $property) {
                return new ProductValueCollection($property);
            }, Property::find()->orderBy('sort')->indexBy('xml_id')->all());
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'price'], 'required'],
            [['name', 'xml_id'], 'string', 'max' => 255],
            [['description', 'current_props', 'current_section', 'url'], 'string'],
            [['xml_id'], SlugValidator::class],
            [['xml_id'], 'unique', 'targetClass' => Product::class, 'filter' => $this->_product ? ['<>', 'id', $this->_product->id] : null],
            [['category_id', 'sort', 'price', 'old_price', 'discount'], 'integer'],
            [['active'], 'boolean'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['tmp_id'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Product())->attributeLabels();
    }

    /**
     * @return array
     */
    public function internalCollections()
    {
        return ['values', 'tags', 'image'];
    }

    /**
     * @return array
     */
    public function categoriesList()
    {
        return ArrayHelper::map(Category::find()->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 0 ? str_repeat('-- ', $category['depth']) . ' ' : '') . $category['name'];
        });
    }
}