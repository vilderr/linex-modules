<?php

namespace linex\modules\catalog\models\collections;

use Yii;
use yii\base\Model;
use linex\modules\catalog\models\Property;
use linex\modules\main\validators\SlugValidator;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class PropertyCollection
 * @package linex\modules\catalog\models\collections
 */
class PropertyCollection extends Model
{
    public $name;
    public $slug;
    public $xml_id;
    public $sort;
    public $active;
    public $filtrable;
    public $sef;

    private $_property;

    /**
     * PropertyCollection constructor.
     *
     * @param Property|null $property
     * @param array         $config
     */
    public function __construct(Property $property = null, array $config = [])
    {
        if ($property) {
            $this->name = $property->name;
            $this->slug = $property->slug;
            $this->xml_id = $property->xml_id;
            $this->sort = $property->sort;
            $this->active = $property->active;
            $this->filtrable = $property->filtrable;
            $this->sef = $property->sef;

            $this->_property = $property;
        } else {
            $this->sort = Property::DEFAULT_SORT;
            $this->active = Property::DEFAULT_ACTIVE;
            $this->filtrable = Property::DEFAULT_FILTRABLE;
            $this->sef = Property::DEFAULT_SEF;
        }

        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'xml_id'], 'required'],
            [['name', 'slug', 'xml_id'], 'string', 'max' => 255],
            [['slug', 'xml_id'], SlugValidator::class],
            [['slug', 'xml_id'], 'unique', 'targetClass' => Property::class, 'filter' => $this->_property ? ['<>', 'id', $this->_property->id] : null],
            [['sort'], 'integer'],
            [['active', 'filtrable', 'sef'], 'boolean'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name'      => Yii::t('app', 'Name'),
            'slug'      => Yii::t('app', 'Slug'),
            'xml_id'    => CatalogModule::t('Xml Id'),
            'sort'      => Yii::t('app', 'Sort'),
            'active'    => Yii::t('app', 'Active'),
            'filtrable' => CatalogModule::t('Use in smart filter'),
            'sef'       => CatalogModule::t('Turn in sef url'),
        ];
    }
}