<?php

namespace linex\modules\catalog\models\distribution;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use linex\modules\catalog\models\distribution\behaviors\PartsBehavior;
use linex\modules\catalog\models\EventTrait;
use linex\modules\catalog\models\query\DistributionQuery;

/**
 * This is the model class for table "{{%distribution}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sort
 * @property integer $active
 */
class Distribution extends \yii\db\ActiveRecord
{
    use EventTrait;

    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 1;

    public $_newParts = [];

    /**
     * @param $name
     * @param $description
     * @param $sort
     * @param $active
     *
     * @return static
     */
    public static function create($name, $description, $sort, $active)
    {
        return new static([
            'name'        => $name,
            'description' => $description,
            'sort'        => $sort,
            'active'      => $active,
        ]);
    }

    /**
     * @param $name
     * @param $description
     * @param $sort
     * @param $active
     *
     * @return void
     */
    public function edit($name, $description, $sort, $active)
    {
        $this->name = $name;
        $this->description = $description;
        $this->sort = $sort;
        $this->active = $active;
    }

    // ***** PARTS *****

    /**
     * @param $collections
     */
    public function assignParts($collections)
    {
        $parts = $this->getParts()->indexBy('id')->all();

        foreach ($collections as $id => $c) {
            if (ArrayHelper::keyExists($id, $parts)) {
                $part = Part::findOne($id);
                $part->edit(
                    $c->sort,
                    $c->active,
                    $c->filter_name,
                    $c->filter_category_id,
                    $c->filter_active,
                    $c->filter_current_props,
                    $c->filter_current_section,
                    $c->filter_price_from,
                    $c->filter_price_to,
                    $c->operation_category_id,
                    $c->operation_active,
                    $c->operation_tags
                );
            } else {
                $part = Part::create(
                    $c->sort,
                    $c->active,
                    $c->filter_name,
                    $c->filter_category_id,
                    $c->filter_active,
                    $c->filter_current_props,
                    $c->filter_current_section,
                    $c->filter_price_from,
                    $c->filter_price_to,
                    $c->operation_category_id,
                    $c->operation_active,
                    $c->operation_tags
                );
            }

            $this->_newParts[] = $part;
        }

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParts()
    {
        return $this->hasMany(Part::class, ['distribution_id' => 'id'])->orderBy('sort');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%distribution}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort', 'active'], 'integer'],
            [['name', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'name'        => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'created_at'  => Yii::t('app', 'Created At'),
            'updated_at'  => Yii::t('app', 'Updated At'),
            'sort'        => Yii::t('app', 'Sort'),
            'active'      => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::className(),
            'parts'     => PartsBehavior::className(),
            'relations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['parts'],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return DistributionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DistributionQuery(get_called_class());
    }
}
