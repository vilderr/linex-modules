<?php

namespace linex\modules\catalog\models\distribution;

use Yii;
use linex\modules\catalog\models\distribution\behaviors\PartBehavior;
use linex\modules\catalog\models\query\DistributionPartQuery;
use linex\modules\main\Module as MainModule;
use linex\modules\catalog\Module as CatalogModule;

/**
 * This is the model class for table "{{%distribution_part}}".
 *
 * @property integer      $id
 * @property integer      $distribution_id
 * @property string       $filter_json
 * @property string       $operation_json
 * @property integer      $sort
 * @property integer      $active
 *
 * @property Distribution $distribution
 */
class Part extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 1;

    public $filter = [];
    public $operation = [];

    public $filter_name;
    public $filter_category_id;
    public $filter_active;
    public $filter_current_props;
    public $filter_current_section;
    public $filter_price_from;
    public $filter_price_to;
    public $operation_category_id;
    public $operation_active;
    public $operation_tags;

    /**
     * @param $sort
     * @param $active
     * @param $filter_name
     * @param $filter_category_id
     * @param $filter_active
     * @param $filter_current_props
     * @param $filter_current_section
     * @param $filter_price_from
     * @param $filter_price_to
     * @param $operation_category_id
     * @param $operation_active
     * @param $operation_tags
     *
     * @return static
     */
    public static function create($sort, $active, $filter_name, $filter_category_id, $filter_active, $filter_current_props, $filter_current_section, $filter_price_from, $filter_price_to, $operation_category_id, $operation_active, $operation_tags)
    {
        return new static([
            'sort'   => $sort,
            'active' => $active,

            'filter_name'            => $filter_name,
            'filter_category_id'     => $filter_category_id,
            'filter_active'          => $filter_active,
            'filter_current_props'   => $filter_current_props,
            'filter_current_section' => $filter_current_section,
            'filter_price_from'      => $filter_price_from,
            'filter_price_to'        => $filter_price_to,
            'operation_category_id'  => $operation_category_id,
            'operation_active'       => $operation_active,
            'operation_tags'         => $operation_tags,
        ]);
    }

    /**
     * @param $sort
     * @param $active
     * @param $filter_name
     * @param $filter_category_id
     * @param $filter_active
     * @param $filter_current_props
     * @param $filter_current_section
     * @param $filter_price_from
     * @param $filter_price_to
     * @param $operation_category_id
     * @param $operation_active
     * @param $operation_tags
     *
     * @return void
     */
    public function edit($sort, $active, $filter_name, $filter_category_id, $filter_active, $filter_current_props, $filter_current_section, $filter_price_from, $filter_price_to, $operation_category_id, $operation_active, $operation_tags)
    {

        $this->sort = $sort;
        $this->active = $active;

        $this->filter_name = $filter_name;
        $this->filter_category_id = $filter_category_id;
        $this->filter_active = $filter_active;
        $this->filter_current_props = $filter_current_props;
        $this->filter_current_section = $filter_current_section;
        $this->filter_price_from = $filter_price_from;
        $this->filter_price_to = $filter_price_to;
        $this->operation_category_id = $operation_category_id;
        $this->operation_active = $operation_active;
        $this->operation_tags = $operation_tags;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%distribution_part}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['distribution_id'], 'required'],
            [['distribution_id', 'sort', 'active'], 'integer'],
            [['filter_json', 'operation_json'], 'string'],
            [['distribution_id'], 'exist', 'skipOnError' => true, 'targetClass' => Distribution::className(), 'targetAttribute' => ['distribution_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                     => 'ID',
            'distribution_id'        => 'Distribution ID',
            'sort'                   => Yii::t('app', 'Sort'),
            'active'                 => Yii::t('app', 'Active'),
            'filter_name'            => Yii::t('app', 'Name'),
            'filter_category_id'     => CatalogModule::t('Category ID'),
            'filter_active'          => Yii::t('app', 'Active'),
            'filter_current_props'   => CatalogModule::t('Current Props'),
            'filter_current_section' => CatalogModule::t('Current Section'),
            'filter_price_from'      => CatalogModule::t('Price From'),
            'filter_price_to'        => CatalogModule::t('Price To'),
            'operation_category_id'  => CatalogModule::t('Target Category ID'),
            'operation_active'       => Yii::t('app', 'Active'),
            'operation_tags'         => CatalogModule::t('Tags'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDistribution()
    {
        return $this->hasOne(Distribution::className(), ['id' => 'distribution_id']);
    }

    /**
     * @inheritdoc
     * @return DistributionPartQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DistributionPartQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'part' => PartBehavior::className(),
        ];
    }
}
