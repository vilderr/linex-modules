<?php

namespace linex\modules\catalog\models\distribution\behaviors;

use linex\modules\catalog\models\distribution\Part;
use yii\db\ActiveRecord;
use yii\base\Event;
use yii\helpers\Json;

/**
 * Class PartBehavior
 * @package linex\modules\catalog\models\distribution\behaviors
 */
class PartBehavior extends \yii\base\Behavior
{
    public $filterAttribute = 'filter';
    public $jsonFilterAttribute = 'filter_json';
    public $operationAttribute = 'operation';
    public $jsonOperationAttribute = 'operation_json';

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND    => 'onInit',
            ActiveRecord::EVENT_BEFORE_INSERT => 'onBeforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeSave',
        ];
    }

    /**
     * @param Event $event
     *
     * @return void
     */
    public function onInit(Event $event)
    {
        /**
         * @var $model Part
         */
        $model = $event->sender;
        $dataFilter = Json::decode($model->getAttribute($this->jsonFilterAttribute));
        $dataOperation = Json::decode($model->getAttribute($this->jsonOperationAttribute));

        $model->filter_name = $dataFilter['filter_name'];
        $model->filter_category_id = $dataFilter['filter_category_id'];
        $model->filter_active = $dataFilter['filter_active'];
        $model->filter_current_props = $dataFilter['filter_current_props'];
        $model->filter_current_section = $dataFilter['filter_current_section'];
        $model->filter_price_from = $dataFilter['filter_price_from'];
        $model->filter_price_to = $dataFilter['filter_price_to'];
        $model->operation_category_id = $dataOperation['operation_category_id'];
        $model->operation_active = $dataOperation['operation_active'];
        $model->operation_tags = $dataOperation['operation_tags'];
    }

    /**
     * @param Event $event
     *
     * @return void
     */
    public function onBeforeSave(Event $event)
    {
        /**
         * @var $model Part
         */
        $model = $event->sender;

        $model->setAttributes([
            $this->jsonFilterAttribute    => Json::encode([
                'filter_name'            => $model->filter_name,
                'filter_category_id'     => $model->filter_category_id,
                'filter_active'          => $model->filter_active,
                'filter_current_props'   => $model->filter_current_props,
                'filter_current_section' => $model->filter_current_section,
                'filter_price_from'      => $model->filter_price_from,
                'filter_price_to'        => $model->filter_price_to,
            ]),
            $this->jsonOperationAttribute => Json::encode([
                'operation_category_id' => $model->operation_category_id,
                'operation_active'      => $model->operation_active,
                'operation_tags'        => $model->operation_tags,
            ]),
        ]);
    }
}