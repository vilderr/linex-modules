<?php

namespace linex\modules\catalog\models\distribution\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\Event;

/**
 * Class PartsBehavior
 * @package linex\modules\catalog\models\distribution\behaviors
 */
class PartsBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'onBeforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'onBeforeSave',
        ];
    }

    /**
     * @param Event $event
     *
     * @return void
     */
    public function onBeforeSave(Event $event)
    {
        $model = $event->sender;
        $model->parts = $model->_newParts;
    }
}