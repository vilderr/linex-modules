<?php

namespace linex\modules\catalog\models\product;

use Yii;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Tag;

/**
 * This is the model class for table "{{%product_tag_assignments}}".
 *
 * @property integer $product_id
 * @property integer $tag_id
 *
 * @property Product $product
 * @property Tag     $tag
 */
class TagAssignment extends \yii\db\ActiveRecord
{
    /**
     * @param $tagId
     *
     * @return static
     */
    public static function create($tagId)
    {
        return new static([
            'tag_id' => $tagId,
        ]);
    }

    /**
     * @param $id
     *
     * @return bool
     */
    public function isForTag($id)
    {
        return $this->tag_id == $id;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_tag_assignments}}';
    }
}
