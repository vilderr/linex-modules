<?php

namespace linex\modules\catalog\models\query;

/**
 * This is the ActiveQuery class for [[\linex\modules\catalog\models\distribution\DistributionPart]].
 *
 * @see \linex\modules\catalog\models\distribution\Part
 */
class DistributionPartQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function forDistribution($id)
    {
        return $this->andWhere('[[distribution_id]]=' . $id);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\distribution\Part[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\distribution\Part|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
