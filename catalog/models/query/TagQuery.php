<?php

namespace linex\modules\catalog\models\query;

/**
 * This is the ActiveQuery class for [[\linex\modules\catalog\models\ProductTag]].
 *
 * @see \linex\modules\catalog\models\Tag
 */
class TagQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\Tag[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\Tag|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
