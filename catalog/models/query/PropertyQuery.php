<?php

namespace linex\modules\catalog\models\query;

use linex\modules\catalog\models\Category;
use yii\db\ActiveQuery;

/**
 * Class PropertyQuery
 * @package linex\modules\catalog\models\query
 */
class PropertyQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function filtrable()
    {
        return $this->andWhere('[[filtrable]]=1');
    }

    public function withCategoryValues(Category $category)
    {
        return $this->joinWith('categoryValues')->where(['category_id' => $category->id]);
    }
}