<?php

namespace linex\modules\catalog\models\query;

/**
 * This is the ActiveQuery class for [[\linex\modules\catalog\models\PropertyValue]].
 *
 * @see \linex\modules\catalog\models\PropertyValue
 */
class PropertyValueQuery extends \yii\db\ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\PropertyValue[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\PropertyValue|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
