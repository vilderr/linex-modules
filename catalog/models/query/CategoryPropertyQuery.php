<?php

namespace linex\modules\catalog\models\query;

use linex\modules\catalog\models\Property;
use yii\db\ActiveQuery;

/**
 * Class CategoryPropertyQuery
 * @package linex\modules\catalog\models\query
 */
class CategoryPropertyQuery extends ActiveQuery
{
    public function forProperty(Property $property)
    {
        return $this->andWhere('[[property_id]]=' . $property->id);
    }
}