<?php
namespace linex\modules\catalog\models\query;

use yii\db\ActiveQuery;

/**
 * Class ProductQuery
 * @package linex\modules\catalog\models\query
 */
class ProductQuery extends ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @param $id
     *
     * @return $this
     */
    public function valuesForProperty($id)
    {
        return $this->andWhere('[[property_id]]=' . $id);
    }
}