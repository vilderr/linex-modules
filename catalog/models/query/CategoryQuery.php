<?php

namespace linex\modules\catalog\models\query;

use yii\db\ActiveQuery;
use paulzi\nestedsets\NestedSetsQueryTrait;

/**
 * Class CategoryQuery
 * @package linex\modules\catalog\models\query
 */
class CategoryQuery extends ActiveQuery
{
    use NestedSetsQueryTrait;
}