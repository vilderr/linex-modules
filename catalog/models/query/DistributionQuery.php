<?php

namespace linex\modules\catalog\models\query;

/**
 * This is the ActiveQuery class for [[\linex\modules\catalog\models\distribution\Distribution]].
 *
 * @see \linex\modules\catalog\models\distribution\Distribution
 */
class DistributionQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\distribution\Distribution[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\distribution\Distribution|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
