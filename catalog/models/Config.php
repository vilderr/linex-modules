<?php

namespace linex\modules\catalog\models;
use yii\helpers\ArrayHelper;

/**
 * Class Config
 * @package linex\modules\catalog\models
 */
class Config extends \linex\modules\main\models\Config
{
    /**
     * @return array|\linex\modules\main\models\Config[]
     */
    public function getSettings()
    {
        return ArrayHelper::index(self::find()->forModule('catalog')->all(),null, 'group');
    }
}