<?php

namespace linex\modules\catalog\models;

use Yii;
use linex\modules\catalog\models\query\PropertyValueQuery;
use linex\modules\catalog\Module as CatalogModule;

/**
 * This is the model class for table "{{%catalog_property_value}}".
 *
 * @property integer  $id
 * @property integer  $property_id
 * @property string   $name
 * @property string   $slug
 * @property string   $xml_id
 * @property integer  $sort
 * @property integer  $active
 *
 * @property Property $property
 */
class PropertyValue extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 1;

    /**
     * @param $property_id
     * @param $name
     * @param $slug
     * @param $xml_id
     * @param $sort
     * @param $active
     *
     * @return static
     */
    public static function create($property_id, $name, $slug, $xml_id, $sort, $active)
    {
        return new static([
            'property_id' => $property_id,
            'name'        => $name,
            'slug'        => $slug,
            'xml_id'      => $xml_id,
            'sort'        => $sort,
            'active'      => $active,
        ]);
    }

    /**
     * @param $name
     * @param $slug
     * @param $xml_id
     * @param $sort
     * @param $active
     *
     * @return void
     */
    public function edit($name, $slug, $xml_id, $sort, $active)
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->xml_id = $xml_id;
        $this->sort = $sort;
        $this->active = $active;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_property_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'sort', 'active'], 'integer'],
            [['name', 'slug', 'xml_id'], 'required'],
            [['name', 'slug', 'xml_id'], 'string', 'max' => 255],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => 'ID',
            'property_id' => CatalogModule::t('Property ID'),
            'name'        => Yii::t('app', 'Name'),
            'slug'        => Yii::t('app', 'Slug'),
            'xml_id'      => Yii::t('app', 'Xml ID'),
            'sort'        => Yii::t('app', 'Sort'),
            'active'      => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @param      $property_id
     * @param      $slug
     * @param bool $active
     *
     * @return array|PropertyValue|null
     */
    public static function getPropertyValue($property_id, $slug, $active = true)
    {
        $query =  static::find()->where(['property_id' => $property_id, 'slug' => $slug]);
        if ($active)
        {
            $query->active();
        }

        return $query->asArray()->one();
    }

    /**
     * @inheritdoc
     * @return \linex\modules\catalog\models\query\PropertyValueQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PropertyValueQuery(get_called_class());
    }
}
