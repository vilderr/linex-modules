<?php

namespace linex\modules\catalog\models;

use Yii;
use linex\modules\catalog\models\query\TagQuery;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%product_tag}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $sort
 * @property integer $active
 */
class Tag extends \yii\db\ActiveRecord
{
    public static function create($name, $slug, $sort, $active)
    {
        return new static([
            'name'   => $name,
            'slug'   => $slug,
            'sort'   => $sort,
            'active' => $active,
        ]);
    }

    public function edit($name, $slug, $sort, $active)
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->sort = $sort;
        $this->active = $active;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_tag}}';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => 'ID',
            'name'       => Yii::t('app', 'Name'),
            'slug'       => Yii::t('app', 'Slug'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'sort'       => Yii::t('app', 'Sort'),
            'active'     => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::class,
        ];
    }

    /**
     * @inheritdoc
     * @return TagQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TagQuery(get_called_class());
    }
}
