<?php

namespace linex\modules\catalog\models;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use linex\modules\catalog\behaviors\CategoryRecalcBehavior;
use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use paulzi\nestedsets\NestedSetsBehavior;
use linex\modules\catalog\models\query\CategoryQuery;


/**
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property string  $slug_path
 * @property string  $xml_id
 * @property string  $description
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $sort
 * @property integer $active
 * @property integer $global_active
 * @property integer $created_by
 *
 * Class Category
 * @package linex\modules\catalog\models
 */
class Category extends ActiveRecord
{
    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 1;

    public $parent_id;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%catalog_category}}';
    }

    /**
     * @param $name
     * @param $slug
     * @param $xml_id
     * @param $description
     * @param $sort
     * @param $active
     * @param $global_active
     * @param $created_by
     * @param $parent_id
     *
     * @return static
     */
    public static function create($name, $slug, $xml_id, $description, $sort, $active, $global_active, $created_by, $parent_id)
    {
        return new static([
            'name'          => $name,
            'slug'          => $slug,
            'xml_id'        => $xml_id,
            'description'   => $description,
            'sort'          => $sort,
            'active'        => $active,
            'global_active' => $global_active,
            'created_by'    => $created_by,
            'parent_id'     => $parent_id,
        ]);
    }

    /**
     * @param $name
     * @param $slug
     * @param $xml_id
     * @param $description
     * @param $sort
     * @param $active
     * @param $global_active
     * @param $parent_id
     *
     * @return void
     */
    public function edit($name, $slug, $xml_id, $description, $sort, $active, $global_active, $parent_id)
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->xml_id = $xml_id;
        $this->description = $description;
        $this->sort = $sort;
        $this->active = $active;
        $this->global_active = $global_active;
        $this->parent_id = $parent_id;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name'        => Yii::t('app', 'Name'),
            'slug'        => Yii::t('app', 'Slug'),
            'xml_id'      => Yii::t('app', 'Xml Id'),
            'description' => Yii::t('app', 'Description'),
            'sort'        => Yii::t('app', 'Sort'),
            'active'      => Yii::t('app', 'Active'),
        ];
    }

    /**
     * @return CategoryQuery
     */
    public static function find()
    {
        return new CategoryQuery(static::class);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->slug_path = $this->makeSlugPath();

        return parent::beforeSave($insert);
    }

    /**
     * @return string
     */
    private function makeSlugPath()
    {
        $parent_model = self::findOne($this->parent_id);
        if ($parent_model !== null) {
            if ($parent_model->slug === 'root') {
                return $this->slug;
            } else {
                return $parent_model->slug_path . '/' . $this->slug;
            }
        }

        return '';
    }

    /**
     * @param $slug
     *
     * @return static
     */
    public static function findBySlug($slug)
    {
        return self::findOne(['slug' => $slug, 'active' => 1]);
    }

    /**
     * @param $slugPath
     *
     * @return static
     */
    public static function findBySlugPath($slugPath)
    {
        return self::findOne(['slug_path' => $slugPath]);
    }

    public function getProperties()
    {
        return $this->hasMany(CategoryProperty::class, ['category_id' => 'id']);
    }

    public function revokeProperties()
    {
        $this->properties = [];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::className(),
            'tree'      => NestedSetsBehavior::className(),
            'recalc' => CategoryRecalcBehavior::className(),
            'relations' => [
                'class'     => SaveRelationsBehavior::class,
                'relations' => ['properties'],
            ],
        ];
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
}