<?php

namespace linex\modules\catalog\models;


use linex\modules\catalog\models\propertyindex\Facet;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class bkp
{
    public $category;
    public $facet;
    public $request;

    private $items = null;

    public function __construct(Category $category, $properties)
    {
        $this->category = $category;
        $this->request = array($properties);
        $this->facet = new Facet($category);
    }

    public function setItems()
    {
        if ($this->items === null) {
            foreach (CategoryProperty::getArray($this->category) as $PID => $prop) {
                if ($prop['filtrable']) {
                    $this->items[$PID] = [
                        'id'     => $prop['id'],
                        'name'   => $prop['name'],
                        'slug'   => $prop['slug'],
                        'sort'   => $prop['sort'],
                        'values' => [],
                    ];
                }
            }
        }
    }

    public function normalizeItems()
    {
        $slugs = ArrayHelper::map($this->items, 'slug', 'id');

        foreach ($this->request as $type => $request) {
            if ($type === 'sef') {
                foreach ($request as $pcode => $value) {
                    if (!array_key_exists($pcode, $slugs)) {
                        throw new NotFoundHttpException();
                    }

                    if (null === PropertyValue::getPropertyValue($slugs[$pcode], $value)) {
                        throw new NotFoundHttpException();
                    }
                }
            }
        }
    }

    public function fillItems()
    {
        $rows = [];
        foreach ($this->facet->query()->each() as $row) {
            $rows[$row['property_id']][$row['value']] = $row;
        }

        foreach ($rows as $PID => $v) {
            $values = ArrayHelper::getColumn($v, 'value');
            foreach (PropertyValue::find()->select(['id', 'name', 'slug'])->where(['property_id' => $PID, 'id' => $values])->active()->orderBy('sort')->limit(20)->each() as $val) {
                $this->items[$PID]['values'][] = [
                    'name'  => $val->name,
                    'slug'  => $val->slug,
                    'count' => $v[$val['id']]['count']
                ];
            }
        }

        //echo '<pre>';print_r($rows);echo '</pre>';
    }
}