<?php

namespace linex\modules\catalog\models;

/**
 * Class EventTrait
 * @package linex\modules\catalog\models
 */
trait EventTrait
{
    private $events = [];

    protected function recordEvent($event)
    {
        $this->events[] = $event;
    }

    public function releaseEvents()
    {
        $events = $this->events;
        $this->events = [];

        return $events;
    }
}