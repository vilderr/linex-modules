<?php

namespace linex\modules\catalog\models;

use Yii;
use linex\modules\catalog\models\query\ProductValueQuery;

/**
 * This is the model class for table "catalog_product_value".
 *
 * @property integer       $property_id
 * @property integer       $product_id
 * @property integer       $value_id
 *
 * @property Product       $product
 * @property Property      $property
 * @property PropertyValue $value
 */
class ProductValue extends \yii\db\ActiveRecord
{
    /**
     * @param $property_id
     * @param $value
     *
     * @return static
     */
    public static function create($property_id, $value)
    {
        return new static([
            'property_id' => $property_id,
            'value_id'    => $value,
        ]);
    }

    /**
     * @param $property_id
     *
     * @return bool
     */
    public function forProperty($property_id)
    {
        return $this->property_id == $property_id;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product_value}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['property_id', 'product_id', 'value_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
            [['value_id'], 'exist', 'skipOnError' => true, 'targetClass' => PropertyValue::className(), 'targetAttribute' => ['value_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'property_id' => 'Property ID',
            'product_id'  => 'Product ID',
            'value_id'    => 'Value ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValue()
    {
        return $this->hasOne(PropertyValue::className(), ['id' => 'value_id']);
    }
}
