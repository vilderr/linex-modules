<?php

namespace linex\modules\catalog\models;

use linex\modules\catalog\models\query\CategoryPropertyQuery;
use linex\modules\main\helpers\ModelHelper;
use Yii;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Property;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%catalog_category_property}}".
 *
 * @property integer  $category_id
 * @property integer  $property_id
 * @property integer  $filtrable
 *
 * @property Category $category
 * @property Property $property
 */
class CategoryProperty extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_category_property}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'property_id'], 'required'],
            [['category_id', 'property_id', 'filtrable'], 'integer'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['property_id'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['property_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'property_id' => 'Property ID',
            'filtrable'   => 'Filtrable',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'property_id']);
    }

    /**
     * @param Category|null $category
     * @param bool          $onlyFiltrable
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getArray(Category $category = null, $onlyFiltrable = false)
    {
        $q1 = Property::find()->select([
            'id property_id',
            'name',
            'slug',
            'filtrable',
            'sef',
            'sort',
        ]);


        $result = $q1->orderBy('sort')->asArray()->indexBy('property_id')->all();

        if ($category) {
            $q2 = Property::find()->alias('P')->select([
                'P.id property_id',
                'P.name name',
                'P.slug slug',
                'CP.filtrable filtrable',
                'P.sef sef',
                'P.sort sort',
            ]);

            $q2->leftJoin(Category::tableName() . ' C0', 'C0.lft <= :lft AND C0.rgt >= :rgt', [':lft' => $category->lft, ':rgt' => $category->rgt]);
            $q2->innerJoin(static::tableName() . ' CP', 'CP.category_id = C0.id AND CP.property_id = P.id');

            $q2->orderBy('P.sort ASC')->asArray();

            foreach ($q2->each() as $row)
            {
                $row['variants'] = [];
                $result[$row['property_id']] = $row;
            }
        }

        $result = static::resort($result);

        if ($onlyFiltrable)
        {
             foreach ($result as $k => $row)
             {
                 if (!$row['filtrable'])
                 {
                     unset($result[$k]);
                 }
             }
        }

        return $result;
    }

    /**
     * @return CategoryPropertyQuery
     */
    public static function find()
    {
        return new CategoryPropertyQuery(static::class);
    }

    private static function resort($array)
    {
        ArrayHelper::multisort($array, function ($item) {
            return $item['sort'];
        });

        return ArrayHelper::index($array, 'property_id');
    }
}
