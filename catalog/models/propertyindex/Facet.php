<?php

namespace linex\modules\catalog\models\propertyindex;

use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Product;
use yii\db\Query;

/**
 * Class Facet
 * @package linex\modules\catalog\models\facet\Facet
 */
class Facet
{
    protected $storage;
    protected $category;

    public function __construct(Category $category)
    {
        $this->storage = new Storage();
        $this->category = $category;
    }

    public function query()
    {
        $query = new Query();

        $query->select([
            'F.property_id property_id',
            'F.value value',
            'COUNT(DISTINCT F.product_id) count'
        ]);
        $query->from(Product::tableName() . 'P');
        $query->innerJoin(Category::tableName().'C', 'P.category_id = C.id');
        $query->innerJoin($this->storage->tableName().'F on F.product_id = P.id');
        $query->where([
            'C.global_active' => 1,
        ]);
        $query->andWhere('C.lft >= :lft', [':lft' => $this->category->lft]);
        $query->andWhere('C.rgt <= :rgt', [':rgt' => $this->category->rgt]);
        $query->groupBy(['F.property_id', 'F.value']);
        $query->orderBy('count DESC');

        return $query;
    }
}