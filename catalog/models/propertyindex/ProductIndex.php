<?php

namespace linex\modules\catalog\models\propertyindex;

use Yii;

/**
 * This is the model class for table "{{%product_index}}".
 *
 * @property integer $category_id
 * @property integer $product_id
 * @property integer $property_id
 * @property integer $value
 */
class ProductIndex extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%product_index}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category_id', 'product_id', 'property_id', 'value'], 'required'],
            [['category_id', 'product_id', 'property_id', 'value'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'category_id' => 'Category ID',
            'product_id' => 'Element ID',
            'property_id' => 'Property ID',
            'value' => 'Value',
        ];
    }
}
