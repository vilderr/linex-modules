<?php
namespace linex\modules\catalog\models\propertyindex;

use linex\modules\catalog\models\Product;
use yii\base\Model;

/**
 * Class Product
 * @package linex\modules\catalog\models\propertyindex
 */
class Element extends Model
{
    protected $product;
    protected $productValues = null;

    /**
     * Element constructor.
     *
     * @param Product $product
     * @param array   $config
     */
    public function __construct(Product $product, array $config = [])
    {
        $this->product = $product;
        parent::__construct($config);
    }

    /**
     * @param $property_id
     *
     * @return array|mixed
     */
    public function getPropertyValues($property_id)
    {
        $values = $this->getProductValues();

        if (isset($values[$property_id]))
            return $values[$property_id];

        return [];
    }

    /**
     * @return array
     */
    protected function getProductValues()
    {
        if ($this->productValues === null)
        {
            $this->productValues = [];
            foreach ($this->product->values as $v)
            {
                $this->productValues[$v->property_id][] = $v->value_id;
            }
        }

        return $this->productValues;
    }
}