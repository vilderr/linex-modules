<?php

namespace linex\modules\catalog\models\propertyindex;

use yii\db\ColumnSchemaBuilder;
use yii\db\Query;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\PropertyValue;

/**
 * Class Storage
 * @package linex\modules\catalog\models\propertyindex
 */
class Storage
{
    /**
     * @return string
     */
    public function tableName()
    {
        return ProductIndex::tableName();
    }

    /**
     * @param $category_id
     * @param $product_id
     * @param $property_id
     * @param $value
     *
     * @return bool
     */
    public function addIndex($category_id, $product_id, $property_id, $value)
    {
        $model = new ProductIndex([
            'category_id' => $category_id,
            'product_id'  => $product_id,
            'property_id' => $property_id,
            'value'       => $value,
        ]);

        if ($model->validate()) {
            $model->save();

            return true;
        }

        return false;
    }

    /**
     * @param $element_id
     * @return void
     */
    public function deleteIndex($element_id)
    {
        ProductIndex::deleteAll(['product_id' => $element_id]);
    }

    /**
     * drop table for index
     * @return void
     */
    public function remove()
    {
        $query = new Query();
        $query->createCommand()->dropTable(ProductIndex::tableName())->query();
    }

    /**
     * create table for index
     * @return void
     */
    public function create()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';


        $q = (new Query())->createCommand();
        $q->createTable(ProductIndex::tableName(), [
            'category_id' => (new ColumnSchemaBuilder('integer'))->notNull(),
            'product_id'  => (new ColumnSchemaBuilder('integer'))->notNull(),
            'property_id' => (new ColumnSchemaBuilder('integer'))->notNull(),
            'value'       => (new ColumnSchemaBuilder('integer'))->notNull(),
        ], $tableOptions)->query();


        $q->addPrimaryKey('{{%pk-product-index}}', ProductIndex::tableName(), ['category_id', 'product_id', 'property_id', 'value'])->query();
        $q->createIndex('{{%idx-product-index-category}}', ProductIndex::tableName(), 'category_id')->query();
        $q->createIndex('{{%idx-product-index-product}}', ProductIndex::tableName(), 'product_id')->query();

        $q->addForeignKey('{{%fk-product-index-product_id}}', ProductIndex::tableName(), 'product_id', Product::tableName(), 'id', 'CASCADE')->query();
        $q->addForeignKey('{{%fk-product-index-property_id}}', ProductIndex::tableName(), 'property_id', Property::tableName(), 'id', 'CASCADE')->query();
        $q->addForeignKey('{{%fk-product-index-value}}', ProductIndex::tableName(), 'value', PropertyValue::tableName(), 'id', 'CASCADE')->query();
    }
}