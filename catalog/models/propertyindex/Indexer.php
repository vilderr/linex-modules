<?php

namespace linex\modules\catalog\models\propertyindex;

use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Property;

class Indexer
{
    public $storage;
    protected $propertyFilter = null;

    public function __construct()
    {
        $this->storage = new Storage();
    }

    /**
     * Index one product
     * @param Product $product
     *
     * @return bool
     */
    public function addProductIndex(Product $product)
    {
        $element = new Element($product);
        $category = $product->category;
        $elementIndexValues = $this->getElementIndexEntries($element);

        foreach ($elementIndexValues as $property_id => $values)
        {
            foreach ($values as $value)
            {
                $this->storage->addIndex(
                    $category->id,
                    $product->id,
                    $property_id,
                    $value["VALUE"]
                );
            }
        }

        return true;
    }

    /**
     * @param Product $product
     * @return void
     */
    public function updateProductIndex(Product $product)
    {
        $this->deleteProductIndex($product);
        if ($product->active && $product->category_id > 1)
            $this->addProductIndex($product);
    }

    /**
     * @param Product $product
     * @return void
     */
    public function deleteProductIndex(Product $product)
    {
        $this->storage->deleteIndex($product->id);
    }

    /**
     * @param Element $element
     *
     * @return array
     */
    protected function getElementIndexEntries(Element $element)
    {
        $result = [];

        foreach ($this->getFilterProperty() as $property)
        {
            $result[$property->id] = [];

            $values = $element->getPropertyValues($property->id);

            foreach ($values as $v)
            {
                $value = intval($v);
                $result[$property->id][$value] = [
                    'VALUE' => $value
                ];
            }
        }

        return array_filter($result, "count");
    }

    /**
     * @return array
     */
    protected function getFilterProperty()
    {
        if ($this->propertyFilter === null)
        {
            $this->propertyFilter = Property::find()->filtrable()->all();
        }

        return (array)$this->propertyFilter;
    }
}