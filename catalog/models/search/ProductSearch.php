<?php

namespace linex\modules\catalog\models\search;

use Yii;
use linex\modules\catalog\models\collections\ProductCollection;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class ProductSearch
 * @package linex\modules\catalog\models\search
 */
class ProductSearch extends Model
{
    public $id;
    public $name;
    public $current_section;
    public $active;
    public $current_props;
    public $category_id;
    public $price_from;
    public $price_to;

    /**
     * @var $collection ProductCollection
     */
    public $collection;

    public function init()
    {
        parent::init();

        $this->collection = new ProductCollection();
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'current_section', 'current_props'], 'safe'],
            [['id', 'category_id', 'active', 'price_from', 'price_to'], 'integer'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = Product::find()->with('category');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0 = 1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'category_id' => $this->category_id,
            'active'      => $this->active,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'current_section', $this->current_section]);
        $query->andFilterWhere(['like', 'current_props', $this->current_props]);

        $query->andFilterWhere(['>=', 'price', $this->price_from]);
        $query->andFilterWhere(['<', 'price', $this->price_to]);

        return $dataProvider;
    }

    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('app', 'ID'),
            'name'            => Yii::t('app', 'Name'),
            'category_id'     => CatalogModule::t('Category ID'),
            'active'          => Yii::t('app', 'Active'),
            'current_section' => CatalogModule::t('Current Section'),
            'current_props'   => CatalogModule::t('Current Props'),
            'price_from'      => CatalogModule::t('Price From'),
            'price_to'        => CatalogModule::t('Price To'),
        ];
    }
}