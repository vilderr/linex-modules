<?php

namespace linex\modules\catalog\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use linex\modules\catalog\models\Tag;

/**
 * Class TagSearch
 * @package linex\modules\catalog\models\search
 */
class TagSearch extends Model
{
    public $name;
    public $slug;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['name', 'slug'], 'safe'],
        ];
    }

    public function search(array $params)
    {
        $query = Tag::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query
            ->andFilterWhere(['like', 'name', $this->name]);
        $query
            ->andFilterWhere(['like', 'slug', $this->name]);

        return $dataProvider;
    }
}