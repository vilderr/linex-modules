<?php

namespace linex\modules\catalog\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use linex\modules\catalog\models\Property;

/**
 * Class PropertySearch
 * @package linex\modules\catalog\models\search
 */
class PropertySearch extends Model
{
    public $id;
    public $name;
    public $slug;
    public $xml_id;
    public $active;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'active'], 'integer'],
            [['name', 'slug', 'xml_id'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = Property::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'     => $this->id,
            'active' => $this->active,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->name])
            ->andFilterWhere(['like', 'xml_id', $this->name]);

        return $dataProvider;
    }
}