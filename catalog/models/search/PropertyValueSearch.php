<?php

namespace linex\modules\catalog\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use linex\modules\catalog\models\PropertyValue;

/**
 * Class PropertyValueSearch
 * @package linex\modules\catalog\models\search
 */
class PropertyValueSearch extends Model
{
    public $id;
    public $property_id;
    public $name;
    public $slug;
    public $xml_id;
    public $sort;
    public $active;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'property_id', 'sort', 'active'], 'integer'],
            [['name', 'slug', 'xml_id'], 'safe'],
        ];
    }

    /**
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search(array $params)
    {
        $query = PropertyValue::find()->with('property');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => [
                'defaultOrder' => ['id' => SORT_ASC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        $query->andFilterWhere([
            'id'          => $this->id,
            'property_id' => $this->property_id,
            'xml_id'      => $this->xml_id,
            'sort'        => $this->sort,
            'active'      => $this->active,
        ]);

        $query
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'slug', $this->slug]);

        return $dataProvider;
    }
}