<?php

namespace linex\modules\catalog\models;

use linex\modules\catalog\behaviors\PropertyUpdateBehavior;
use Yii;
use linex\modules\catalog\models\query\PropertyQuery;
use linex\modules\catalog\Module as CatalogModule;

/**
 * This is the model class for table "catalog_property".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property string  $xml_id
 * @property integer $sort
 * @property integer $active
 * @property integer $filtrable
 * @property integer $sef
 */
class Property extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 1;
    const DEFAULT_FILTRABLE = 0;
    const DEFAULT_SEF = 1;

    /**
     * @param $name
     * @param $slug
     * @param $xml_id
     * @param $sort
     * @param $active
     * @param $filtrable
     *
     * @return static
     */
    public static function create($name, $slug, $xml_id, $sort, $active, $filtrable, $sef)
    {
        return new static([
            'name'      => $name,
            'slug'      => $slug,
            'xml_id'    => $xml_id,
            'sort'      => $sort,
            'active'    => $active,
            'filtrable' => $filtrable,
            'sef'       => $sef,
        ]);
    }

    /**
     * @param $name
     * @param $slug
     * @param $xml_id
     * @param $sort
     * @param $active
     * @param $filtrable
     *
     * @return  void
     */
    public function edit($name, $slug, $xml_id, $sort, $active, $filtrable, $sef)
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->xml_id = $xml_id;
        $this->sort = $sort;
        $this->active = $active;
        $this->filtrable = $filtrable;
        $this->sef = $sef;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_property}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'xml_id'], 'required'],
            [['sort'], 'integer'],
            [['active', 'filtrable', 'sef'], 'boolean'],
            [['name', 'slug', 'xml_id'], 'string', 'max' => 255],
            [['slug'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => 'ID',
            'name'      => Yii::t('app', 'Name'),
            'slug'      => Yii::t('app', 'Slug'),
            'xml_id'    => CatalogModule::t('Xml Id'),
            'sort'      => Yii::t('app', 'Sort'),
            'active'    => Yii::t('app', 'Active'),
            'filtrable' => CatalogModule::t('Use in smart filter'),
            'sef'       => CatalogModule::t('Turn in sef url'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(PropertyValue::class, ['property_id' => 'id']);
    }

    public function getCategoryValues()
    {
        return $this->hasMany(CategoryProperty::class, ['property_id' => 'id']);
    }

    /**
     * @return PropertyQuery
     */
    public static function find()
    {
        return new PropertyQuery(static::class);
    }

    public function behaviors()
    {
        return [
            PropertyUpdateBehavior::class,
        ];
    }
}
