<?php

namespace linex\modules\catalog\models;

use linex\modules\catalog\behaviors\ProductIndexerBehavior;
use linex\modules\catalog\models\query\ProductQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use linex\modules\users\models\User;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\models\product\TagAssignment;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * This is the model class for table "catalog_product".
 *
 * @property integer         $id
 * @property integer         $category_id
 * @property string          $name
 * @property string          $xml_id
 * @property string          $tmp_id
 * @property string          $description
 * @property string          $current_props
 * @property string          $current_section
 * @property string          $url
 * @property integer         $created_at
 * @property integer         $updated_at
 * @property integer         $price
 * @property integer         $old_price
 * @property integer         $discount
 * @property integer         $sort
 * @property integer         $active
 * @property integer         $created_by
 *
 * @property Category        $category
 * @property ProductValue[]  $values
 * @property TagAssignment[] $tagAssignments
 * @property Tag[]           $tags
 */
class Product extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 0;

    public $_newValues = [];

    /**
     * @param $category_id
     * @param $name
     * @param $xml_id
     * @param $description
     * @param $sort
     * @param $active
     * @param $created_by
     *
     * @return static
     */
    public static function create($category_id, $name, $xml_id, $tmp_id, $description, $current_props, $current_section, $url, $price, $old_price, $discount, $sort, $active, $created_by)
    {
        return new static([
            'category_id'     => $category_id,
            'name'            => $name,
            'xml_id'          => $xml_id,
            'tmp_id'          => $tmp_id,
            'description'     => $description,
            'current_props'   => $current_props,
            'current_section' => $current_section,
            'url'             => $url,
            'price'           => $price,
            'old_price'       => $old_price,
            'discount'        => $discount,
            'sort'            => $sort,
            'active'          => $active,
            'created_by'      => $created_by,
        ]);
    }

    /**
     * @param $category_id
     * @param $name
     * @param $xml_id
     * @param $description
     * @param $sort
     * @param $active
     * @param $created_by
     *
     * @return void
     */
    public function edit($category_id, $name, $xml_id, $tmp_id, $description, $current_props, $current_section, $url, $price, $old_price, $discount, $sort, $active, $created_by)
    {
        $this->category_id = $category_id;
        $this->name = $name;
        $this->xml_id = $xml_id;
        $this->tmp_id = $tmp_id;
        $this->description = $description;
        $this->current_props = $current_props;
        $this->current_section = $current_section;
        $this->url = $url;
        $this->price = $price;
        $this->old_price = $old_price;
        $this->discount = $discount;
        $this->sort = $sort;
        $this->active = $active;
        $this->created_by = $created_by;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_product}}';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => 'ID',
            'category_id'     => CatalogModule::t('Category ID'),
            'name'            => Yii::t('app', 'Name'),
            'xml_id'          => CatalogModule::t('Xml Id'),
            'description'     => Yii::t('app', 'Description'),
            'current_props'   => CatalogModule::t('Current Props'),
            'current_section' => CatalogModule::t('Current Section'),
            'url'             => CatalogModule::t('Vendor Url'),
            'created_at'      => Yii::t('app', 'Created At'),
            'updated_at'      => Yii::t('app', 'Updated At'),
            'price'           => CatalogModule::t('Price'),
            'old_price'       => CatalogModule::t('Old Price'),
            'discount'        => CatalogModule::t('Discount'),
            'sort'            => Yii::t('app', 'Sort'),
            'active'          => Yii::t('app', 'Active'),
            'created_by'      => 'Created By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getPropertyValues($property_id)
    {
        $values = [];
        foreach ($this->values as $val) {
            if ($val->forProperty($property_id)) {
                $values[$val->value_id] = $val;
            }
        }

        return $values;
    }

    /**
     * @param $property_id
     * @param $values
     */
    public function collectValues($property_id, $values)
    {
        $dbVals = $this->getPropertyValues($property_id);

        foreach ($values as $value) {
            if (ArrayHelper::keyExists($value, $dbVals)) {
                $this->_newValues[] = $dbVals[$value];
            } else {
                $this->_newValues[] = ProductValue::create($property_id, $value);
            }
        }
    }

    /**
     * @return void
     */
    public function setValue()
    {
        $this->values = $this->_newValues;
    }

    public function getValues()
    {
        return $this->hasMany(ProductValue::className(), ['product_id' => 'id']);
    }

    public function revokeTags()
    {
        $this->tagAssignments = [];
    }

    public function assignTag($id)
    {
        /**
         * @var $assignments TagAssignment[]
         */
        $assignments = $this->tagAssignments;
        foreach ($assignments as $assignment) {
            if ($assignment->isForTag($id)) {
                return;
            }
        }

        $assignments[] = TagAssignment::create($id);
        $this->tagAssignments = $assignments;
    }

    public function revokeTag($id)
    {
        /**
         * @var $assignments TagAssignment[]
         */
        $assignments = $this->tagAssignments;
        foreach ($assignments as $i => $assignment) {
            if ($assignment->isForTag($id)) {
                unset($assignments[$i]);
                $this->tagAssignments = $assignments;

                return;
            }
        }
        throw new \DomainException('Assignment is not found.');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTagAssignments()
    {
        return $this->hasMany(TagAssignment::class, ['product_id' => 'id']);
    }

    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'tag_id'])->via('tagAssignments');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::className(),
            //'indexer' => ProductIndexerBehavior::className(),
            'relations' => [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['values', 'tagAssignments'],
            ],
        ];
    }

    /**
     * @return ProductQuery
     */
    public static function find()
    {
        return new ProductQuery(static::class);
    }

    /**
     * @return array
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }
}
