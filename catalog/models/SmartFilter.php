<?php

namespace linex\modules\catalog\models;

use Yii;
use linex\modules\catalog\managers\frontend\PropertyManager;
use linex\modules\main\helpers\ModelHelper;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

/**
 * Class SmartFilter
 * @package linex\modules\catalog\models
 */
class SmartFilter extends Model
{
    public $items;

    protected $route;
    protected $manager;

    private $_orderItems;
    private $_revertOrder;


    public function __construct(PropertyManager $manager, array $config = [])
    {
        $this->manager = $manager;
        $this->items = $this->manager->items;
        $this->route = '/' . Yii::$app->controller->route;

        parent::__construct($config);
    }

    public function getCategory()
    {
        return $this->manager->category;
    }

    public function init()
    {
        $this->_orderItems = $this->getSefItemsOrder($this->items);
        $this->checkRequestOrder($this->_orderItems);
        $this->setItems();
    }

    private function setItems()
    {
        foreach ($this->manager->getCategoryValues()->each(10000) as $value) {
            $this->items[$value['property_id']]['variants'][$value['value']] = $value;
        }

        foreach ($this->items as $PID => &$item) {
            $checked = false;

            $item['skimpy'] = (count($item['variants']) > 20) ? true : false;

            // Выбираем первые 20 элементов при обычном запросе страницы
            // Если передан AJAX запрос, выбираем все значения
            $item['variants'] = array_slice($item['variants'], 0, 20, true);
            $iteration = PropertyValue::find()
                ->select([
                    'id',
                    'name',
                    'slug',
                ])
                ->where([
                    'property_id' => $PID,
                    'id'          => ArrayHelper::getColumn($item['variants'], 'value'),
                    'active'      => ModelHelper::STATUS_ACTIVE,
                ])->asArray()->indexBy('id');

            foreach ($iteration->each() as $value) {
                $value['checked'] = false;

                // делаем проверку до тех пор пока не появится первое положительное значение
                if (!$checked && (isset($this->manager->request['all'][$PID]) && $value['id'] == $this->manager->request['all'][$PID]['id'])) {
                    $item['checked'] = $value['checked'] = $checked = true;
                }
                $value['link'] = $this->collectUrl($item['slug'], $value);
                $item['variants'][$value['id']] = ArrayHelper::merge($item['variants'][$value['id']], $value);
            }
        }
    }

    private function collectUrl($slug, array $value)
    {
        $request = $this->_orderItems;
        $queryRequest = [];
        $params = [
            $this->route,
            'code' => $this->category->slug,
        ];

        if (ArrayHelper::keyExists($slug, $request)) {
            $request[$slug] = $value['slug'];
        } else {
            $queryRequest[$slug] = $value['slug'];
        }

        foreach ($this->manager->request['sef'] as $PS => $v) {
            if ($PS == $slug) {
                if ($value['slug'] == $v) {
                    unset($request[$slug]);
                }
            } else {
                $request[$this->items[$this->manager->slugs[$PS]]['slug']] = $v;
            }
        }

        foreach ($this->manager->request['query'] as $PS => $v) {
            if ($PS == $slug) {
                if ($value['slug'] == $v) {
                    unset($queryRequest[$slug]);
                }
            } else {
                $queryRequest[$this->items[$this->manager->slugs[$PS]]['slug']] = $v;
            }
        }

        foreach ($request as $s => $v) {
            if ($s == $v)
                unset($request[$s]);
        }

        $params['sef'] = $request;
        $params['query'] = $queryRequest;

        return Url::toRoute($params);
    }

    private function getSefItemsOrder(array $items)
    {
        $r = ArrayHelper::getColumn($this->items, function ($element) {
            return $element['sef'] ? $element : null;
        });
        ArrayHelper::removeValue($r, null);
        $r = ArrayHelper::map($r, 'slug', 'slug');

        return $r;
    }

    private function checkRequestOrder(array $itemsOrder)
    {
        $requestOrder = $this->manager->request['sef'];
        foreach ($itemsOrder as $slug => $o) {
            $key = $this->manager->slugs[$slug];
            if (isset($requestOrder[$slug])) {
                $requestOrder[$slug] = $this->items[$key]['slug'];
            } else {
                unset($itemsOrder[$slug]);
            }
        }

        if ($requestOrder !== $itemsOrder) {
            throw new NotFoundHttpException();
        }
    }
}