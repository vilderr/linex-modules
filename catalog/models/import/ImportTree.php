<?php

namespace linex\modules\catalog\models\import;

use Yii;

/**
 * This is the model class for table "{{%import_tree}}".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property integer $element_id
 */
class ImportTree extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%import_tree}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'element_id'], 'required'],
            [['profile_id', 'element_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'profile_id' => 'Profile ID',
            'element_id' => 'Element ID',
        ];
    }

    /**
     * @inheritdoc
     * @return ImportTreeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImportTreeQuery(get_called_class());
    }
}
