<?php

namespace linex\modules\catalog\models\import;

use Yii;
use linex\modules\main\Module as MainModule;
use linex\modules\catalog\Module as CatalogModule;
use lhs\Yii2SaveRelationsBehavior\SaveRelationsBehavior;

/**
 * This is the model class for table "{{%catalog_import_profile}}".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $sections_url
 * @property string  $products_url
 * @property string  $api_key
 * @property integer $sort
 * @property integer $active
 */
class Profile extends \yii\db\ActiveRecord
{
    const STATUS_DRAFT = 0;
    const STATUS_ACTIVE = 1;

    const DEFAULT_SORT = 500;
    const DEFAULT_ACTIVE = 1;


    /**
     * @param $name
     * @param $sections_url
     * @param $products_url
     * @param $api_key
     * @param $sort
     * @param $active
     *
     * @return static
     */
    public static function create($name, $sections_url, $products_url, $api_key, $sort, $active)
    {
        return new static([
            'name'         => $name,
            'sections_url' => $sections_url,
            'products_url' => $products_url,
            'api_key'      => $api_key,
            'sort'         => $sort,
            'active'       => $active,
        ]);
    }

    /**
     * @param $name
     * @param $sections_url
     * @param $products_url
     * @param $api_key
     * @param $sort
     * @param $active
     *
     * @return void
     */
    public function edit($name, $sections_url, $products_url, $api_key, $sort, $active)
    {
        $this->name = $name;
        $this->sections_url = $sections_url;
        $this->products_url = $products_url;
        $this->api_key = $api_key;
        $this->sort = $sort;
        $this->active = $active;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_import_profile}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'sections_url', 'products_url', 'api_key'], 'required'],
            [['sort'], 'integer'],
            [['active'], 'boolean'],
            [['name', 'sections_url', 'products_url', 'api_key'], 'string', 'max' => 255],
            [['sections_url', 'products_url'], 'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'           => 'ID',
            'name'         => Yii::t('app', 'Name'),
            'sections_url' => CatalogModule::t('Rest Sections Url'),
            'products_url' => CatalogModule::t('Rest Products Url'),
            'api_key'      => CatalogModule::t('Api Key'),
            'sort'         => Yii::t('app', 'Sort'),
            'active'       => Yii::t('app', 'Active'),
        ];
    }

    public function getSections()
    {
        return $this->hasMany(Sections::class, ['profile_id' => 'id']);
    }

    /**
     * @param $id
     *
     * @return void
     */
    public function assignSection($id)
    {
        $sections = $this->sections;
        $sections[] = Sections::create($this->id, $id);

        $this->sections = $sections;
    }

    /**
     * @return void
     */
    public function revokeSections()
    {
        $this->sections = [];
    }

    public function behaviors()
    {
        return [
            [
                'class'     => SaveRelationsBehavior::className(),
                'relations' => ['sections'],
            ],
        ];
    }
}
