<?php

namespace linex\modules\catalog\models\import;

use yii\db\ActiveRecord;

/**
 * Class Tree
 * @package linex\modules\catalog\models\import
 */
class Tree extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%import_tree}}';
    }
}