<?php

namespace linex\modules\catalog\models\import;

use linex\modules\catalog\repositories\TagRepository;
use Yii;
use yii\helpers\Json;
use linex\modules\main\helpers\StringHelper;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\collections\ProductCollection;
use linex\modules\catalog\models\collections\PropertyCollection;
use linex\modules\catalog\models\collections\PropertyValueCollection;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\PropertyValue;
use linex\modules\catalog\managers\PropertyManager;
use linex\modules\catalog\managers\PropertyValueManager;
use linex\modules\catalog\repositories\ProductRepository;
use linex\modules\catalog\repositories\PropertyRepository;
use linex\modules\catalog\repositories\PropertyValueRepository;
use linex\modules\catalog\helpers\ImportProfileHelper;
use linex\modules\catalog\managers\ProductManager;

/**
 * Class Parser
 * @package linex\modules\catalog\models\import
 */
final class Parser
{
    private $_ns;

    private $_productManager;
    private $_propertyManager;
    private $_propertyValueManager;

    /**
     * Parser constructor.
     *
     * @param $ns
     */
    public function __construct(&$ns)
    {
        $this->_ns = &$ns;

        $productRepository = new ProductRepository();
        $propertyRepository = new PropertyRepository();
        $propertyValueRepository = new PropertyValueRepository();
        $tagRepository = new TagRepository();

        $this->_productManager = new ProductManager($productRepository, $tagRepository);
        $this->_propertyManager = new PropertyManager($propertyRepository);
        $this->_propertyValueManager = new PropertyValueManager($propertyValueRepository);
    }

    /**
     * @param $start_time
     * @param $interval
     *
     * @return array
     */
    public function importElements($start_time, $interval)
    {
        $counter = [
            'add' => 0,
            'upd' => 0,
            'err' => 0,
            'crc' => 0,
        ];

        $r = ImportProfileHelper::sendRestRequest($this->_ns['url'], $this->_ns['token'], ['last_id' => $this->_ns['last_id'], 'limit' => 50, 'sections' => Json::encode($this->_ns['sections'])]);
        $result = Json::decode($r);

        if (is_array($result)) {
            foreach ($result as $item) {
                $counter['crc']++;

                if ($ID = $this->importElement($item, $counter)) {
                    //TreeTable::add(['ELEMENT_ID' => $ID]);
                }

                $this->_ns['last_id'] = $item['id'];

                if ($interval > 0 && (time() - $start_time) > $interval)
                    break;
            }
        }

        return $counter;
    }

    /**
     * @param $item
     * @param $counter
     *
     * @return bool|int
     */
    protected function importElement($item, &$counter)
    {
        $result = false;
        $arProduct = [];

        $bMatch = false;
        $xml_id = $item['xml_id'];
        $tmp_id = $this->getArrayCrc($item);

        if ($product = Product::findOne(['xml_id' => $xml_id])) {
            $bMatch = ($tmp_id == $product->tmp_id);
        }

        if ($bMatch) {
            return $product->id;
        }

        $collection = $product ? new ProductCollection($product) : new  ProductCollection();

        $arProduct['ProductCollection'] = [
            'name'            => $item['name'],
            'xml_id'          => $xml_id,
            'tmp_id'          => $tmp_id,
            'description'     => null,
            'current_props'   => $item['current_props'],
            'current_section' => $item['current_section'],
            'url'             => $item['url'],
            'price'           => $item['price'],
            'old_price'       => $item['old_price'],
            'discount'        => $item['discount'],
        ];

        if ($item['brand']) {
            $property = $this->checkProperty('brand');

            $value = $this->checkPropertyValue($property, $item['brand']);
            $arProduct['ProductValueCollection'][$property->xml_id]['items'][] = $value->id;
        }

        foreach ($item['properties'] as $name => $values) {
            $property = $this->checkProperty($name);
            $arValues = [];

            foreach ($values as $val) {
                $arValues[] = $this->checkPropertyValue($property, $val)->id;
            }

            $arProduct['ProductValueCollection'][$property->xml_id]['items'] = $arValues;
        }

        $arProduct['TagsCollection'] = $collection->tags;

        try {
            if ($collection->load($arProduct)) {
                if ($product) {
                    $this->_productManager->edit($product->id, $collection);
                    $counter['upd']++;
                } else {
                    $product = $this->_productManager->create($collection);
                    $counter['add']++;
                }

                $result = $product->id;
            }
        } catch (\DomainException $e) {
            $counter['err']++;

            Yii::$app->errorHandler->logException($e);
        }

        return $result;
    }

    /**
     * @param $name
     *
     * @return Property
     */
    protected function checkProperty($name)
    {

        if (null === ($property = Property::findOne(['xml_id' => $name]))) {
            $collection = new PropertyCollection(null, [
                'name'   => $name,
                'slug'   => $name,
                'xml_id' => $name,
            ]);

            $property = $this->_propertyManager->create($collection);
        }

        return $property;
    }

    /**
     * @param Property $property
     * @param          $name
     *
     * @return PropertyValue
     */
    protected function checkPropertyValue(Property $property, $name)
    {
        $xml_id = StringHelper::translit($name) . '-' . $property->xml_id;

        if (null === ($value = PropertyValue::findOne(['xml_id' => $xml_id]))) {
            $collection = new PropertyValueCollection(null, [
                'property_id' => $property->id,
                'name'        => $name,
                'slug'        => StringHelper::translit($name),
                'xml_id'      => $xml_id,
            ]);

            $value = $this->_propertyValueManager->create($collection);
        }

        return $value;
    }

    /**
     * @param $array
     *
     * @return int
     */
    protected function getArrayCrc(array $array)
    {
        $c = crc32(print_r($array, true));
        if ($c > 0x7FFFFFFF)
            $c = -(0xFFFFFFFF - $c + 1);

        return $c;
    }
}