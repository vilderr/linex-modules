<?php

namespace linex\modules\catalog\models\import;

/**
 * This is the ActiveQuery class for [[ImportTree]].
 *
 * @see ImportTree
 */
class ImportTreeQuery extends \yii\db\ActiveQuery
{
    public function forProfile($profile_id)
    {
        return $this->andWhere('[[profile_id]]=' . $profile_id);
    }

    /**
     * @inheritdoc
     * @return ImportTree[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ImportTree|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
