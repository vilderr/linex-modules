<?php

namespace linex\modules\catalog\models\import;

use Yii;

/**
 * This is the model class for table "{{%catalog_import_profile_sections}}".
 *
 * @property integer $profile_id
 * @property integer $value
 *
 * @property Profile $profile
 */
class Sections extends \yii\db\ActiveRecord
{
    /**
     * @param $profile_id
     * @param $value
     *
     * @return static
     */
    public static function create($profile_id, $value)
    {
        return new static([
            'profile_id' => $profile_id,
            'value'      => $value,
        ]);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%catalog_import_profile_sections}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profile_id', 'value'], 'required'],
            [['profile_id', 'value'], 'integer'],
            [['profile_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['profile_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'profile_id' => 'Profile ID',
            'value'      => 'Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['id' => 'profile_id']);
    }
}
