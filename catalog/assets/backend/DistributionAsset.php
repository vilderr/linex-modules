<?php

namespace linex\modules\catalog\assets\backend;

use yii\web\AssetBundle;

/**
 * Class DistributionAsset
 * @package linex\modules\catalog\assets\backend
 */
class DistributionAsset extends AssetBundle
{
    public $sourcePath = '@linex/modules/catalog/assets/backend/dist';

    public $js = [
        'js/distribution.js',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}