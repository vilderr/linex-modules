(function ($) {
    'use strict';

    $.fn.distribution = function (method) {
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === 'object' || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error('Метод с именем ' + method + ' не существует для jQuery.distribution');
        }
    };

    var methods = {
        init: function (options) {
            return this.each(function () {
                var settings = $.extend({}, {}, options || {});

                switch (settings.type) {
                    case 'part':
                        $(this).on('click', '.remove-btn', {
                            'id': settings.id,
                            'distribution_id': settings.distribution_id,
                            'url': settings.url
                        }, $.proxy(methods.removePart, this));
                        break;

                    case 'list':
                        $(this).on('click', '.add-part-btn', {
                            'parent': settings.id,
                            'distribution_id': settings.distribution_id,
                            'url': settings.url
                        }, $.proxy(methods.addPart, this));
                        break;
                }
            });
        },
        removePart: function (event) {
            var data = event.data;

            var request = $.ajax({
                url: data.url,
                data: {
                    'id': data.id,
                    'distribution_id': data.distribution_id
                },
                type: 'get'
            });

            request.done($.proxy(function () {
                this.remove();
            }, this));
        },
        addPart: function (event) {
            var data = event.data;
            var parent = $('#' + data.parent);

            var request = $.ajax({
                url: data.url,
                data: {
                    id: data.distribution_id
                },
                type: 'get'
            });

            request.done(function (result) {
                parent.append(result);
            });
        }
    };

})(jQuery);

/*
 (function (window) {
 "use strict";

 window.DistributionPart = function (params) {
 this.visual = {
 list_id: '',
 id: ''
 };

 this.obList = null;
 this.obPart = null;
 this.url = '';

 this.error = 0;

 if ('object' === typeof params) {
 this.visual = params.visual;
 this.url = params.url;
 if (this.url.length <= 0) {
 this.error = -2;
 }
 }
 else {
 this.error = -1;
 }

 if (0 === this.error) {
 $.proxy(this.Init(), this);
 }
 };

 window.DistributionPart.prototype.Init = function () {
 this.obList = $(this.visual.list_id);
 if (!this.obList.length) {
 this.error = -5;
 }
 this.obPart = $(this.visual.id);
 if (!this.obPart.length) {
 this.error = -10;
 }

 if (0 === this.error) {
 this.obPart.on('click', '.remove-btn', $.proxy(this.RemovePart, this));
 $(document).on('click', '#iteration-add-btn', $.proxy(this.AddPart, this));
 }

 console.log(this);
 };

 window.DistributionPart.prototype.AddPart = function () {
 var request = $.ajax({
 url: this.url,
 type: 'post'
 })
 ;
 var parent = this.obList;

 $(document).off('click', '#iteration-add-btn');

 request.done(function (result) {
 parent.append(result);
 });
 };

 window.DistributionPart.prototype.RemovePart = function () {
 this.obPart.remove();
 };

 })(window);
 */