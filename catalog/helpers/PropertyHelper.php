<?php

namespace linex\modules\catalog\helpers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class PropertyHelper
 * @package linex\modules\catalog\helpers
 */
class PropertyHelper
{
    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            Property::STATUS_DRAFT  => CatalogModule::t('Status Draft'),
            Property::STATUS_ACTIVE => CatalogModule::t('Status Active'),
        ];
    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     *
     * @return string
     */
    public static function statusLabel($status)
    {
        switch ($status) {
            case Property::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case Property::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}