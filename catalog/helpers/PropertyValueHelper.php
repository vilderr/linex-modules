<?php

namespace linex\modules\catalog\helpers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use linex\modules\catalog\models\PropertyValue;

/**
 * Class PropertyValueHelper
 * @package linex\modules\catalog\helpers
 */
class PropertyValueHelper
{
    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            PropertyValue::STATUS_DRAFT  => 'Draft',
            PropertyValue::STATUS_ACTIVE => 'Active',
        ];
    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     *
     * @return string
     */
    public static function statusLabel($status)
    {
        switch ($status) {
            case PropertyValue::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case PropertyValue::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}