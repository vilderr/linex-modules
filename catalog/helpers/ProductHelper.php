<?php

namespace linex\modules\catalog\helpers;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class ProductHelper
 * @package linex\modules\catalog\helpers
 */
class ProductHelper
{
    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            Product::STATUS_ACTIVE => CatalogModule::t('Status Active'),
            Product::STATUS_DRAFT  => CatalogModule::t('Status Draft'),
        ];
    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     *
     * @return string
     */
    public static function statusLabel($status)
    {
        switch ($status) {
            case Product::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case Product::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}