<?php

namespace linex\modules\catalog\helpers;

use yii\helpers\ArrayHelper;
use linex\modules\catalog\models\Category;

/**
 * Class CategoryHelper
 * @package linex\modules\catalog\helpers
 */
final class CategoryHelper
{
    public static function tree()
    {
        return ArrayHelper::map(Category::find()->orderBy('lft')->asArray()->all(), 'id', function (array $category) {
            return ($category['depth'] > 0 ? str_repeat('-- ', $category['depth']) . ' ' : '') . $category['name'];
        });
    }
}