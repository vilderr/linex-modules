<?php

namespace linex\modules\catalog\helpers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use linex\modules\catalog\models\import\Profile;
use yii\httpclient\Client;
use linex\modules\catalog\Module as CatalogModule;
use yii\httpclient\Exception;

/**
 * Class ImportProfileHelper
 * @package linex\modules\catalog\helpers
 */
class ImportProfileHelper
{
    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            Profile::STATUS_DRAFT  => 'Draft',
            Profile::STATUS_ACTIVE => 'Active',
        ];
    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     *
     * @return string
     */
    public static function statusLabel($status)
    {
        switch ($status) {
            case Profile::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case Profile::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }

    /**
     * @param $url
     * @param $token
     *
     * @return string
     * @throws Exception
     */
    public static function sendRestRequest($url, $token, $params = [])
    {
        $client = new Client([
            'transport' => 'yii\httpclient\CurlTransport',
        ]);

        $data = [
            'access-token' => $token,
        ];

        foreach ($params as $name => $param) {
            $data[$name] = $param;
        }

        $response = $client->createRequest()
            ->setMethod('get')
            ->setUrl($url)
            ->setData($data)
            ->setHeaders([
                'Accept' => 'application/json',
            ])->send();

        if ($response->isOk) {
            return $response->content;
        } else {
            Yii::info(print_r($response, 1), 'info');
            throw new Exception(CatalogModule::t('Invalid remote server response'));
        }
    }

    /**
     * @param $url
     * @param $token
     *
     * @return array
     */
    public static function getRemoteSections($url, $token)
    {
        $collect = [];
        $sections = [];

        $array = json_decode(self::sendRestRequest($url, $token));
        $array = ArrayHelper::toArray($array);

        return $array;
    }
}