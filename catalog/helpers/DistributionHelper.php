<?php

namespace linex\modules\catalog\helpers;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class DistributionHelper
 * @package linex\modules\catalog\helpers
 */
class DistributionHelper
{
    /**
     * @return array
     */
    public static function statusList()
    {
        return [
            Distribution::STATUS_DRAFT  => CatalogModule::t('Status Draft'),
            Distribution::STATUS_ACTIVE => CatalogModule::t('Status Active'),
        ];
    }

    /**
     * @param $status
     *
     * @return mixed
     */
    public static function statusName($status)
    {
        return ArrayHelper::getValue(self::statusList(), $status);
    }

    /**
     * @param $status
     *
     * @return string
     */
    public static function statusLabel($status)
    {
        switch ($status) {
            case Distribution::STATUS_DRAFT:
                $class = 'label label-default';
                break;
            case Distribution::STATUS_ACTIVE:
                $class = 'label label-success';
                break;
            default:
                $class = 'label label-default';
        }

        return Html::tag('span', ArrayHelper::getValue(self::statusList(), $status), [
            'class' => $class,
        ]);
    }
}