<?php

namespace linex\modules\catalog;

use Yii;

use linex\modules\main\components\BaseModule;

/**
 * Class Module
 * @package linex\modules\catalog
 */
class Module extends BaseModule
{
    /**
     * @param \yii\base\Application $app
     */
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/catalog/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/catalog/messages',
            'fileMap'          => [
                'modules/catalog/module' => 'module.php',
            ],
        ];
    }

    /**
     * @param        $message
     * @param array  $params
     * @param string $category
     * @param null   $language
     *
     * @return string
     */
    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/catalog/' . $category, $message, $params, $language);
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        return [
            'bootstrap' => [
                'catalog',
            ],
            'module' => [
                'class' => 'linex\\modules\\catalog\\Module',
                'controllerNamespace' => 'linex\\modules\\catalog\\controllers\\frontend',
                'layoutPath'          => '@app/views/layouts',
                'viewPath'            => '@app/views/modules/catalog',
            ],
            'rules' => [
                'backend' => [
                    'class' => 'yii\\web\\GroupUrlRule',
                    'prefix' => 'dashboard',
                    'routePrefix' => 'dashboard',
                    'rules' => [
                        'catalog' => 'catalog/default/index',
                        'catalog/category' => 'catalog/category/index',
                        'catalog/products' => 'catalog/product/index',
                        'catalog/property' => 'catalog/property/index',
                        'catalog/property-values' => 'catalog/property-values/index',
                        'catalog/tag' => 'catalog/tag/index',
                        'catalog/import' => 'catalog/import/index',
                        'catalog/distribution' => 'catalog/distribution/index',
                        'catalog/product/<_a:(add|edit|delete)>' => 'catalog/product/<_a>',
                        'catalog/category/<_a:(add|edit|delete)>' => 'catalog/category/<_a>',
                        'catalog/property/<_a:(add|edit|delete)>' => 'catalog/property/<_a>',
                        'catalog/property-values/<_a:(add|edit|delete)>' => 'catalog/property-values/<_a>',
                        'catalog/tag/<_a:(add|edit|delete)>' => 'catalog/tag/<_a>',
                        'catalog/import/<_a:(add|edit|run|delete)>' => 'catalog/import/<_a>',
                        'catalog/distribution/<_a:(add|edit|run|delete|add-part|delete-part)>' => 'catalog/distribution/<_a>',
                        'catalog/facet' => 'catalog/facet/index',
                    ],
                ],
                'catalog' => [
                    'class' => 'linex\\modules\\catalog\\components\\frontend\\CatalogRules',
                ],
                'tags' => [
                    'class' => 'linex\\modules\\catalog\\components\\frontend\\TagRules',
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return self::t('Module Name');
    }
}