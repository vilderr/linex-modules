<?php

use yii\db\Migration;
use linex\modules\catalog\models\Category;
use linex\modules\users\models\User;

class m170630_105846_category extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        mb_internal_encoding("UTF-8");
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        $this->createTable(Category::tableName(), [
            'id'            => $this->primaryKey(),
            'name'          => $this->string()->notNull(),
            'slug'          => $this->string()->notNull(),
            'slug_path'     => $this->string()->notNull(),
            'xml_id'        => $this->string(),
            'description'   => $this->text(),
            'created_at'    => $this->integer()->notNull(),
            'updated_at'    => $this->integer()->notNull(),
            'lft'           => $this->integer()->notNull(),
            'rgt'           => $this->integer()->notNull(),
            'depth'         => $this->integer()->notNull(),
            'sort'          => $this->integer()->defaultValue(500),
            'active'        => $this->smallInteger(1)->defaultValue(1),
            'global_active' => $this->smallInteger(1)->defaultValue(1),
            'created_by'    => $this->smallInteger()->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('{{%idx-catalog_category-slug_path}}', Category::tableName(), 'slug', true);
        $this->createIndex('{{%idx-catalog_category-margins}}', Category::tableName(), ['lft', 'rgt']);
        $this->createIndex('{{%idx-catalog_category-active}}', Category::tableName(), 'active');
        $this->createIndex('{{%idx-catalog_category-global_active}}', Category::tableName(), 'global_active');

        $this->insert(Category::tableName(), [
            'id'            => 1,
            'name'          => Yii::t('app', 'Top level'),
            'slug'          => 'root',
            'slug_path'     => 'root',
            'description'   => null,
            'created_at'    => time(),
            'updated_at'    => time(),
            'lft'           => 1,
            'rgt'           => 8,
            'depth'         => 0,
            'sort'          => 500,
            'active'        => 1,
            'global_active' => 1,
            'created_by'    => 1,
        ]);

        $this->batchInsert(
            Category::tableName(),
            ['id', 'name', 'slug', 'slug_path', 'description', 'created_at', 'updated_at', 'lft', 'rgt', 'depth', 'sort', 'active', 'global_active', 'created_by'],
            [
                [2, Yii::t('app', 'Wear'), 'odezhda', 'odezhda', null, time(), time(), 2, 3, 1, 100, 1, 1, 1],
                [3, Yii::t('app', 'Shoes'), 'obuv', 'obuv', null, time(), time(), 4, 5, 1, 200, 1, 1, 1],
                [4, Yii::t('app', 'Accessories'), 'aksessuary', 'aksessuary', null, time(), time(), 6, 7, 1, 300, 1, 1, 1],
            ]
        );
    }

    public function down()
    {
        echo "m170630_105846_category cannot be reverted.\n";

        return false;
    }
}
