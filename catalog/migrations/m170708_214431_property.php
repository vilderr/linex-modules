<?php

use yii\db\Migration;
use linex\modules\catalog\models\Property;

class m170708_214431_property extends Migration
{

    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Property::tableName(), [
            'id'        => $this->primaryKey(),
            'name'      => $this->string(255)->notNull(),
            'slug'      => $this->string(255)->notNull(),
            'xml_id'    => $this->string(255)->notNull(),
            'sort'      => $this->integer()->defaultValue(500),
            'active'    => $this->smallInteger(1)->defaultValue(1),
            'filtrable' => $this->smallInteger(1)->defaultValue(0),
            'sef'       => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('idx-catalog-property-slug', Property::tableName(), 'slug', true);
        $this->createIndex('idx-catalog-property-active', Property::tableName(), 'active');
        $this->createIndex('idx-catalog-property-filtrable', Property::tableName(), 'filtrable');
    }

    public function down()
    {
        echo "m170708_214431_property cannot be reverted.\n";

        return false;
    }
}
