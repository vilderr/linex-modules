<?php

use yii\db\Migration;
use linex\modules\catalog\models\Tag;

class m170726_195850_product_tags extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Tag::tableName(), [
            'id'         => $this->primaryKey(),
            'name'       => $this->string(255)->notNull(),
            'slug'       => $this->string()->notNull(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
            'sort'       => $this->integer()->defaultValue(500),
            'active'     => $this->integer(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('{{%idx-product_tag-slug}}', Tag::tableName(), 'slug', true);
    }

    public function down()
    {
        echo "m170726_195850_product_tags cannot be reverted.\n";

        return false;
    }
}
