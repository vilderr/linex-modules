<?php

use yii\db\Migration;
use linex\modules\catalog\models\import\Profile;

class m170709_084507_import_profile extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Profile::tableName(), [
            'id'           => $this->primaryKey(),
            'name'         => $this->string(255)->notNull(),
            'sections_url' => $this->string(255)->notNull(),
            'products_url' => $this->string(255)->notNull(),
            'api_key'      => $this->string(255)->notNull(),
            'sort'         => $this->integer()->defaultValue(500),
            'active'       => $this->integer(1)->defaultValue(1),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170709_084507_import_profile cannot be reverted.\n";

        return false;
    }
}
