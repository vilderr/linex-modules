<?php

use yii\db\Migration;
use linex\modules\catalog\models\import\Profile;
use linex\modules\catalog\models\import\Sections;

class m170709_174906_import_profile_sections extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Sections::tableName(), [
            'profile_id' => $this->integer()->notNull(),
            'value'      => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-catalog_import_profile_sections}}', Sections::tableName(), ['profile_id', 'value']);
        $this->addForeignKey('{{%fk-catalog_import_profile_sections-profile_id}}', Sections::tableName(), 'profile_id', Profile::tableName(), 'id', 'CASCADE');
        $this->createIndex('{{%idx-catalog_import_profile_sections-profile_id}}', Sections::tableName(), 'profile_id');
    }

    public function down()
    {
        echo "m170709_174906_import_profile_sections cannot be reverted.\n";

        return false;
    }
}
