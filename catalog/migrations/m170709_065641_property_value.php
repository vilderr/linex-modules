<?php

use yii\db\Migration;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\PropertyValue;

class m170709_065641_property_value extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(PropertyValue::tableName(), [
            'id'          => $this->primaryKey(),
            'property_id' => $this->integer(),
            'name'        => $this->string(255)->notNull(),
            'slug'        => $this->string(255)->notNull(),
            'xml_id'      => $this->string(255)->notNull(),
            'sort'        => $this->integer()->defaultValue(500),
            'active'      => $this->integer(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('idx-catalog-property-value-property_id', PropertyValue::tableName(), 'property_id');
        $this->createIndex('idx-catalog-property-value-active', PropertyValue::tableName(), 'active');

        $this->addForeignKey('fk-catalog_property_value-property_id', PropertyValue::tableName(), 'property_id', Property::tableName(), 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m170709_065641_property_value cannot be reverted.\n";

        return false;
    }
}