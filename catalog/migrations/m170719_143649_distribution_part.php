<?php

use yii\db\Migration;
use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\models\distribution\Part;

class m170719_143649_distribution_part extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Part::tableName(), [
            'id'              => $this->primaryKey(),
            'distribution_id' => $this->integer()->notNull(),
            'filter_json'     => $this->text(),
            'operation_json'  => $this->text(),
            'sort'            => $this->integer()->defaultValue(500),
            'active'          => $this->smallInteger(1)->defaultValue(1),
        ], $tableOptions);

        $this->createIndex('{{%idx-distribution_part-distribution_id}}', Part::tableName(), 'distribution_id');
        $this->addForeignKey('{{%fk-distribution_part-distribution_id}}', Part::tableName(), 'distribution_id', Distribution::tableName(), 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170719_143649_distribution_part cannot be reverted.\n";

        return false;
    }
}
