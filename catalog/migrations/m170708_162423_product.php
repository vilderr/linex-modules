<?php

use yii\db\Migration;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Category;

class m170708_162423_product extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Product::tableName(), [
            'id'              => $this->primaryKey(),
            'category_id'     => $this->integer()->notNull(),
            'name'            => $this->string()->notNull(),
            'xml_id'          => $this->string(),
            'tmp_id'          => $this->string(),
            'description'     => $this->text(),
            'current_props'   => $this->text(),
            'current_section' => $this->string(),
            'url'             => $this->string(),
            'created_at'      => $this->integer()->unsigned()->notNull(),
            'updated_at'      => $this->integer()->unsigned()->notNull(),
            'price'           => $this->integer(),
            'old_price'       => $this->integer(),
            'discount'        => $this->integer(),
            'sort'            => $this->integer()->defaultValue(500),
            'active'          => $this->smallInteger(1)->defaultValue(1),
            'created_by'      => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-catalog_product-xml_id}}', Product::tableName(), 'xml_id', true);
        $this->createIndex('{{%idx-catalog_product-category_id}}', Product::tableName(), 'category_id');
        $this->createIndex('{{%idx-catalog_product-active}}', Product::tableName(), 'active');

        $this->addForeignKey('{{%fk-catalog_product-category_id}}', Product::tableName(), 'category_id', Category::tableName(), 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170708_162423_product cannot be reverted.\n";

        return false;
    }
}
