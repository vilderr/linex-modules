<?php

use yii\db\Migration;
use linex\modules\catalog\models\distribution\Distribution;

class m170719_120531_distribution extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Distribution::tableName(), [
            'id'          => $this->primaryKey(),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->string(),
            'created_at'  => $this->integer()->unsigned()->notNull(),
            'updated_at'  => $this->integer()->unsigned()->notNull(),
            'sort'        => $this->integer()->defaultValue(500),
            'active'      => $this->integer(1)->defaultValue(1),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m170719_120531_distribution cannot be reverted.\n";

        return false;
    }
}
