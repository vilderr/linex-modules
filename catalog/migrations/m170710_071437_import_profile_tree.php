<?php

use yii\db\Migration;
use linex\modules\catalog\models\import\Tree;

class m170710_071437_import_profile_tree extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(Tree::tableName(), [
            'id'         => $this->primaryKey(),
            'profile_id' => $this->integer()->notNull(),
            'element_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->createIndex('{{%idx-import_tree-profile_id}}', Tree::tableName(), 'profile_id');
    }

    public function down()
    {
        echo "m170710_071437_catalog_import_profile_tree cannot be reverted.\n";

        return false;
    }
}
