<?php

use yii\db\Migration;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\ProductValue;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\PropertyValue;

class m170717_151710_product_value extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(ProductValue::tableName(), [
            'property_id' => $this->integer()->notNull(),
            'product_id'  => $this->integer()->notNull(),
            'value_id'    => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-product_value}}', ProductValue::tableName(), ['property_id', 'product_id', 'value_id']);
        $this->createIndex('{{%idx-product_value-property_id}}', ProductValue::tableName(), 'property_id');
        $this->createIndex('{{%idx-product_value-product_id}}', ProductValue::tableName(), 'product_id');

        $this->addForeignKey('{{%fk-product_value-property_id}}', ProductValue::tableName(), 'property_id', Property::tableName(), 'id', 'CASCADE');
        $this->addForeignKey('{{%fk-product_value-product_id}}', ProductValue::tableName(), 'product_id', Product::tableName(), 'id', 'CASCADE');
        $this->addForeignKey('{{%fk-product_value-value_id}}', ProductValue::tableName(), 'value_id', PropertyValue::tableName(), 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170717_151710_product_value cannot be reverted.\n";

        return false;
    }
}
