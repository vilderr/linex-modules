<?php

use yii\db\Migration;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Tag;
use linex\modules\catalog\models\product\TagAssignment;

class m170727_055048_product_tag_assignment extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(TagAssignment::tableName(), [
            'product_id' => $this->integer()->notNull(),
            'tag_id'     => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-product_tag_assignment}}', TagAssignment::tableName(), ['product_id', 'tag_id']);

        $this->createIndex('{{%idx-product_tag_assignment-product_id}}', TagAssignment::tableName(), 'product_id');
        $this->createIndex('{{%idx-product_tag_assignment-tag_id}}', TagAssignment::tableName(), 'tag_id');

        $this->addForeignKey('{{%fk-product_tag_assignment-product_id}}', TagAssignment::tableName(), 'product_id', Product::tableName(), 'id', 'CASCADE', 'RESTRICT');
        $this->addForeignKey('{{%fk-product_tag_assignment-tag_id}}', TagAssignment::tableName(), 'tag_id', Tag::tableName(), 'id', 'CASCADE', 'RESTRICT');
    }

    public function down()
    {
        echo "m170727_055048_product_tag_assignments cannot be reverted.\n";

        return false;
    }
}
