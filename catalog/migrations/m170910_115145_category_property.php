<?php

use yii\db\Migration;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\models\CategoryProperty;

class m170910_115145_category_property extends Migration
{
    public function init()
    {
        Yii::$app->language = 'ru-RU';
    }

    public function up()
    {
        $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';

        $this->createTable(CategoryProperty::tableName(), [
            'category_id' => $this->integer()->notNull(),
            'property_id' => $this->integer()->notNull(),
            'filtrable' => $this->integer(1)->defaultValue(1),
        ], $tableOptions);

        $this->addPrimaryKey('{{%pk-category_property}}', CategoryProperty::tableName(), ['category_id', 'property_id']);

        $this->createIndex('{{%idx-category_property-category_id}}', CategoryProperty::tableName(), 'category_id');
        $this->createIndex('{{%idx-category_property-property_id}}', CategoryProperty::tableName(), 'property_id');

        $this->addForeignKey('{{%fk-category_property-category_id}}', CategoryProperty::tableName(), 'category_id', Category::tableName(), 'id', 'CASCADE');
        $this->addForeignKey('{{%fk-category_property-property_id}}', CategoryProperty::tableName(), 'property_id', Property::tableName(), 'id', 'CASCADE');
    }

    public function down()
    {
        echo "m170910_115145_category_property cannot be reverted.\n";

        return false;
    }
}
