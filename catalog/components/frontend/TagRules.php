<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 31.07.17
 * Time: 11:00
 */

namespace linex\modules\catalog\components\frontend;


use yii\web\UrlRuleInterface;

class TagRules implements UrlRuleInterface
{
    public function parseRequest($manager, $request)
    {
        $url = $request->getPathInfo();
        if (!$url)
            return false;

        $arPath = explode('/', $url);
        $controller = array_shift($arPath);

        $code = '';
        if ('tags' !== $controller)
            return false;

        if (!empty($arPath)) {
            if (count($arPath) <> 1)
                return false;

            return [
                'catalog/tags/tag',
                ['slug' => $arPath[0]],
            ];
        }


        return [
            'catalog/tags/index',
            [],
        ];
    }

    public function createUrl($manager, $route, $params)
    {

    }
}