<?php

namespace linex\modules\catalog\components\frontend;

use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\UrlRuleInterface;

/**
 * Class Rules
 * @package linex\modules\catalog\components\frontend
 */
class CatalogRules implements UrlRuleInterface
{
    /**
     * @param \yii\web\UrlManager $manager
     * @param \yii\web\Request    $request
     *
     * @return array|bool
     */
    public function parseRequest($manager, $request)
    {
        $url = trim($request->pathInfo, '/');
        if (!$url)
            return false;

        $arPath = explode('/', $url);
        $controller = array_shift($arPath);

        if ('catalog' !== $controller)
            return false;

        if (!empty($arPath)) {
            if ($arPath[0] == 'product') {
                if (count($arPath) <> 2)
                    return false;

                return [
                    'catalog/default/product',
                    ['code' => array_pop($arPath)],
                ];
            } else {
                $params = [
                    'code' => array_shift($arPath),
                ];

                $properties = [];
                if (!empty($arPath)) {
                    foreach ($arPath as $path) {
                        //все что идет после раздела, разбиваем на две части (свойство-значение),
                        //если до и после удаления пустых значений количество элементов <> 2, ошибка
                        $ar = explode('-', $path, 2);
                        if (count($ar) <> 2 || count(array_diff($ar, [''])) <> 2)
                            return false;

                        if (!isset($properties[$ar[0]])) {
                            $properties[$ar[0]] = $ar[1];
                        } else {
                            return false;
                        }
                    }
                }

                // коллекция свойств, разделенная на ЧПУ и не ЧПУ пары SLUG-ID
                $params['propertiesJson'] = Json::encode([
                    'sef'   => $properties,
                    'query' => $request->queryParams,
                ]);

                //все что пришло в урл, передаем как параметры
                return [
                    'catalog/default/section',
                    $params,
                ];
            }
        }

        return [
            'catalog/default/index',
            [],
        ];
    }

    public function createUrl($manager, $route, $params)
    {
        if ($route === 'catalog/default/section') {
            if (isset($params['code'])) {
                $section = ArrayHelper::remove($params, 'code');
                $sef = ArrayHelper::remove($params, 'sef', []);
                $query = ArrayHelper::remove($params, 'query', []);
                $str = 'catalog/' . $section;

                if (!empty($sef)) {
                    foreach ($sef as $k => $v) {
                        $str .= '/' . $k . '-' . $v;
                    }
                }
                if (!empty($query)) {
                    $arQueryStr = [];

                    foreach ($query as $k => $v) {
                        $arQueryStr[] = $k . '=' . $v;
                    }

                    $str .= '?' . implode('&', $arQueryStr);
                }

                if (!empty($params)) {
                    $arParamsStr = [];

                    foreach ($params as $k => $v) {
                        $arParamsStr[] = $k . '=' . $v;
                    }

                    $str .= empty($query) ? '?' : '&';
                    $str .= implode('&', $arParamsStr);
                }

                return $str;
            }

            return false;
        }

        return false;
    }
}