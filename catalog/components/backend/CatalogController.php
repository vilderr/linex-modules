<?php

namespace linex\modules\catalog\components\backend;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use linex\modules\dashboard\components\DashboardController;

/**
 * Class CatalogController
 * @package linex\modules\catalog\components\backend
 */
class CatalogController extends DashboardController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['catalog manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }
}