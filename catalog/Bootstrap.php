<?php

namespace linex\modules\catalog;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package linex\modules\catalog
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @param \yii\base\Application $app
     *
     * @return void
     */
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/catalog/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/catalog/messages',
            'fileMap'          => [
                'modules/catalog/module' => 'module.php',
            ],
        ];
    }
}