<?php

namespace linex\modules\catalog\controllers\backend;

use Yii;
use yii\helpers\Url;
use linex\modules\catalog\models\collections\ProductCollection;
use linex\modules\catalog\managers\CategoryManager;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\managers\ProductManager;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\models\search\ProductSearch;

/**
 * Class ProductController
 * @package linex\modules\catalog\controllers\backend
 */
class ProductController extends CatalogController
{
    private $productManager;
    private $categoryManager;

    /**
     * ProductController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param ProductManager   $productManager
     * @param CategoryManager  $categoryManager
     * @param array            $config
     */
    public function __construct($id, $module, ProductManager $productManager, CategoryManager $categoryManager, array $config = [])
    {
        $this->productManager = $productManager;
        $this->categoryManager = $categoryManager;

        parent::__construct($id, $module, $config);
    }

    /**
     * @param int $category_id
     *
     * @return string
     */
    public function actionIndex($category_id = 1)
    {
        $category = $this->categoryManager->category->get($category_id);
        $tree = $category->parents;

        $searchModel = new ProductSearch(['category_id' => $category_id]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = CatalogModule::t('{parent}: elements', ['parent' => $category->name]);
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        foreach ($tree as $cat) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $cat->name,
                'url'   => Url::toRoute(['index', 'category_id' => $cat->id]),
            ];
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => $category->name,
            'url'   => Url::toRoute(['index', 'category_id' => $category->id]),
        ];
        $this->view->params['breadcrumbs'][] = CatalogModule::t('Elements');

        return $this->render('index', [
            'category'     => $category,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $category_id
     *
     * @return string
     */
    public function actionAdd($category_id)
    {
        $post = Yii::$app->request->post();
        $category = $this->categoryManager->category->get($category_id);
        $tree = $category->parents;

        $collection = new ProductCollection();
        $collection->category_id = $category->id;

        Yii::info(print_r($post, 1), 'info');
        if ($collection->load($post) && $collection->validate()) {

            try {
                $product = $this->productManager->create($collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'category_id' => $collection->category_id]));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $product->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Add element');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        foreach ($tree as $cat) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $cat->name,
                'url'   => Url::toRoute(['index', 'category_id' => $cat->id]),
            ];
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => $category->name,
            'url'   => Url::toRoute(['index', 'category_id' => $category->id]),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $post = Yii::$app->request->post();
        $product = $this->productManager->product->get($id);
        $tree = $product->category->parents;
        $collection = new ProductCollection($product);

        if ($collection->load($post) && $collection->validate()) {
            Yii::info(print_r($collection,1),'info');
            try {
                $this->productManager->edit($product->id, $collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'category_id' => $collection->category_id]));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $product->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit element');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        foreach ($tree as $cat) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $cat->name,
                'url'   => Url::toRoute(['index', 'category_id' => $cat->id]),
            ];
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => $product->category->name,
            'url'   => Url::toRoute(['index', 'category_id' => $product->category->id]),
        ];
        $this->view->params['breadcrumbs'][] = $product->name;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     * @param $category_id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id, $category_id)
    {
        try {
            $product = $this->productManager->product->get($id);
            $this->productManager->product->remove($product);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Url::toRoute(['index', 'category_id' => $category_id]));
    }
}