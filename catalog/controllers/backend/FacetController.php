<?php

namespace linex\modules\catalog\controllers\backend;

use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class FacetController
 * @package linex\modules\catalog\controllers\backend
 */
class FacetController extends CatalogController
{
    public function actionIndex()
    {
        $this->view->title = CatalogModule::t('Facet manage');
        return $this->render('index');
    }
}