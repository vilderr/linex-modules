<?php

namespace linex\modules\catalog\controllers\backend;

use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\managers\CategoryManager;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\models\collections\CategoryCollection;

/**
 * Class CategoryController
 * @package linex\modules\catalog\controllers\backend
 */
class CategoryController extends CatalogController
{
    private $manager;

    /**
     * CategoryController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param CategoryManager  $manager
     * @param array            $config
     */
    public function __construct($id, $module, CategoryManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
    }

    /**
     * @param int $id
     *
     * @return string
     */
    public function actionIndex($id = 1)
    {
        $category = $this->manager->category->get($id);
        $tree = $category->parents;

        $dataProvider = new ArrayDataProvider([
            'allModels'  => $category->children,
            'modelClass' => Category::className(),
            'sort'       => [
                'attributes' => ['id', 'name', 'slug', 'sort', 'active'],
            ],
        ]);

        $this->view->title = CatalogModule::t('Categories list');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        foreach ($tree as $cat) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $cat->name,
                'url'   => Url::toRoute(['index', 'id' => $cat->id]),
            ];
        }

        $this->view->params['breadcrumbs'][] = $category->name;

        return $this->render('index', [
            'category'     => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function actionAdd($id = 1)
    {
        $post = Yii::$app->request->post();
        $category = $this->manager->category->get($id);
        $tree = $category->parents;

        $collection = new CategoryCollection($category);

        if ($collection->load($post) && $collection->validate()) {
            Yii::info(print_r($collection, 1), 'info');
            try {
                $newCategory = $this->manager->create($collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $collection->parent_id]));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $newCategory->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('New category');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        foreach ($tree as $cat) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $cat->name,
                'url'   => Url::toRoute(['index', 'id' => $cat->id]),
            ];
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => $category->name,
            'url'   => Url::to(['index', 'id' => $category->id]),
        ];

        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $post = Yii::$app->request->post();
        $category = $this->manager->category->get($id);
        $parent = $category->parent;
        $tree = $category->parents;
        $collection = new CategoryCollection($parent, $category);

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($category->id, $collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $collection->parent_id]));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $category->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit category');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        foreach ($tree as $cat) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $cat->name,
                'url'   => Url::toRoute(['index', 'id' => $cat->id]),
            ];
        }
        $this->view->params['breadcrumbs'][] = [
            'label' => $category->name,
        ];

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     * @param $parent_id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id, $parent_id)
    {
        try {
            $this->manager->remove($id);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(['index', 'id' => $parent_id]);
    }
}