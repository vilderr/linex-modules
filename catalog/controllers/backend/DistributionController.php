<?php

namespace linex\modules\catalog\controllers\backend;

use Yii;
use yii\helpers\Url;
use yii\data\ArrayDataProvider;
use linex\modules\catalog\models\collections\distribution\DistributionCollection;
use linex\modules\catalog\models\collections\distribution\PartCollection;
use linex\modules\catalog\models\distribution\Part;
use linex\modules\dashboard\components\ActiveForm;
use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\managers\DistributionManager;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class DistributionController
 * @package linex\modules\catalog\controllers\backend
 */
class DistributionController extends CatalogController
{
    public $manager;

    /**
     * DistributionController constructor.
     *
     * @param string              $id
     * @param \yii\base\Module    $module
     * @param DistributionManager $manager
     * @param array               $config
     */
    public function __construct($id, $module, DistributionManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);

        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels'  => Distribution::find()->orderBy('sort')->all(),
            'modelClass' => Distribution::className(),
            'sort'       => [
                'attributes' => ['id', 'name', 'sort', 'active'],
            ],
        ]);

        $this->view->title = CatalogModule::t('Distribution Profiles');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $post = Yii::$app->request->post();
        $collection = new DistributionCollection();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $distribution = $this->manager->create($collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $distribution->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('New Profile');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Distribution Profiles'),
            'url'   => Url::toRoute('/dashboard/catalog/distribution'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $post = Yii::$app->request->post();
        $distribution = $this->manager->repository->get($id);

        $collection = new DistributionCollection($distribution);

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($distribution->id, $collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index']));
                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect([
                            'edit',
                            'id'        => $distribution->id,
                            'returnUrl' => $returnUrl,
                        ]);
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit profile');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Distribution Profiles'),
            'url'   => Url::toRoute('/dashboard/catalog/distribution'),
        ];
        $this->view->params['breadcrumbs'][] = $distribution->name;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    public function actionRun($id)
    {
        $distribution = $this->manager->repository->get($id);

        $this->view->title = CatalogModule::t('Distribution products');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Distribution Profiles'),
            'url'   => Url::toRoute('/dashboard/catalog/distribution'),
        ];
        $this->view->params['breadcrumbs'][] = $distribution->name;

        return $this->render('run', [
            'model' => $distribution,
        ]);
    }

    public function actionAddPart($id)
    {
        $distribution = $this->manager->repository->get($id);
        $part = new Part();
        $part->link('distribution', $distribution);

        $owner = new DistributionCollection($distribution);
        $collection = new PartCollection();
        $form = new ActiveForm();

        return $this->renderAjax('parts/_part', [
            'model' => $collection,
            'key'   => $part->id,
            'owner' => $owner,
            'form'  => $form,
        ]);
    }

    /**
     * @param $id
     * @param $distribution_id
     *
     * @return void
     */
    public function actionDeletePart($id, $distribution_id)
    {
        $distribution = $this->manager->repository->get($distribution_id);
        $part = Part::findOne($id);

        $part->unlink('distribution', $distribution, true);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        try {
            $this->manager->remove($id);

        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Url::toRoute(['index']));
    }
}