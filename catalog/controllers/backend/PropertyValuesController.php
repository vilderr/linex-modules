<?php

namespace linex\modules\catalog\controllers\backend;

use Yii;
use yii\helpers\Url;
use linex\modules\catalog\managers\PropertyManager;
use linex\modules\catalog\managers\PropertyValueManager;
use linex\modules\catalog\models\collections\PropertyValueCollection;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\models\search\PropertyValueSearch;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class PropertyValuesController
 * @package linex\modules\catalog\controllers\backend
 */
class PropertyValuesController extends CatalogController
{
    private $propertyManager;
    private $valueManager;

    /**
     * PropertyValuesController constructor.
     *
     * @param string               $id
     * @param \yii\base\Module     $module
     * @param PropertyValueManager $valueManager
     * @param PropertyManager      $propertyManager
     * @param array                $config
     */
    public function __construct($id, $module, PropertyValueManager $valueManager, PropertyManager $propertyManager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->valueManager = $valueManager;
        $this->propertyManager = $propertyManager;
    }

    /**
     * @param $property_id
     *
     * @return string
     */
    public function actionIndex($property_id)
    {
        $property = $this->propertyManager->property->get($property_id);
        $params = Yii::$app->request->queryParams;

        $searchModel = new PropertyValueSearch(['property_id' => $property->id]);
        $dataProvider = $searchModel->search($params);

        $this->view->title = CatalogModule::t('{parent}: elements', [
            'parent' => $property->name,
        ]);
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Properties'),
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = $property->name;

        return $this->render('index', [
            'property'     => $property,
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $property_id
     *
     * @return string|\yii\web\Response
     */
    public function actionAdd($property_id)
    {
        $property = $this->propertyManager->property->get($property_id);
        $post = Yii::$app->request->post();
        $collection = new PropertyValueCollection(null, ['property_id' => $property->id]);

        if ($collection->load($post) && $collection->validate()) {
            try {
                $value = $this->valueManager->create($collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index', 'property_id' => $value->property_id]);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $value->id,
                        ]);
                }
            } catch (\Exception $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Add element');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Properties'),
            'url'   => Url::toRoute(['/dashboard/catalog/property']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $property->name,
            'url'   => Url::toRoute(['index', 'property_id' => $property->id]),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $value = $this->valueManager->value->get($id);
        $collection = new PropertyValueCollection($value);
        $post = Yii::$app->request->post();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->valueManager->edit($value->id, $collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index', 'property_id' => $value->property_id]);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $value->id,
                        ]);
                }
            } catch (\Exception $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit element');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Properties'),
            'url'   => Url::toRoute(['/dashboard/catalog/property']),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $value->property->name,
            'url'   => Url::toRoute(['index', 'property_id' => $value->property->id]),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     * @param $property_id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id, $property_id)
    {
        try {
            $value = $this->valueManager->value->get($id);
            $this->valueManager->remove($value);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Url::toRoute(['index', 'property_id' => $property_id]));
    }
}