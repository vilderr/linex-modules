<?php

namespace linex\modules\catalog\controllers\backend;

use Yii;
use yii\helpers\Url;
use linex\modules\catalog\models\collections\TagCollection;
use linex\modules\catalog\models\search\TagSearch;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\managers\TagManager;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class TagController
 * @package linex\modules\catalog\controllers\backend
 */
class TagController extends CatalogController
{
    public $manager;

    public function __construct($id, $module, TagManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new TagSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $this->view->title = CatalogModule::t('Tags');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $post = Yii::$app->request->post();
        $collection = new TagCollection();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $tag = $this->manager->create($collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index']);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $tag->id,
                        ]);
                }

            } catch (\Exception $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Add element');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Tags'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $tag = $this->manager->repository->get($id);
        $collection = new TagCollection($tag);
        $post = Yii::$app->request->post();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($tag->id, $collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index']);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $tag->id,
                        ]);
                }

            } catch (\Exception $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit element');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Tags'),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $tag->name;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        try {
            $tag = $this->manager->repository->get($id);
            $this->manager->repository->remove($tag);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Url::toRoute(['index']));
    }
}