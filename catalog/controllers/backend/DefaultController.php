<?php

namespace linex\modules\catalog\controllers\backend;

use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use linex\modules\dashboard\components\DashboardController;

/**
 * Class DefaultController
 * @package linex\modules\catalog\controllers\backend
 */
class DefaultController extends DashboardController
{
    /**
     * @return array
     */
    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['catalog manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }

    public function actionIndex()
    {
        $this->view->title = "Каталог товаров";

        return $this->render('index');
    }
}