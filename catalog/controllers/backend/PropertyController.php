<?php

namespace linex\modules\catalog\controllers\backend;

use Yii;
use yii\web\NotFoundHttpException;
use yii\helpers\Url;
use linex\modules\catalog\models\collections\PropertyCollection;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\managers\PropertyManager;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\models\search\PropertySearch;

/**
 * Class PropertyController
 * @package linex\modules\catalog\controllers\backend
 */
class PropertyController extends CatalogController
{
    private $manager;

    /**
     * PropertyController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param PropertyManager  $manager
     * @param array            $config
     */
    public function __construct($id, $module, PropertyManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $params = Yii::$app->request->queryParams;
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->search($params);

        $this->view->title = CatalogModule::t('Properties');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $post = Yii::$app->request->post();
        $collection = new PropertyCollection();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $property = $this->manager->create($collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index']);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $property->id,
                        ]);
                }
            } catch (\Exception $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Add property');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Properties'),
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id)
    {
        $post = Yii::$app->request->post();
        $property = $this->manager->property->get($id);
        $collection = new PropertyCollection($property);

        if ($collection->load($post) && $collection->validate()) {
            try {

                $this->manager->edit($property->id, $collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index']);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $property->id,
                        ]);
                }
            } catch (\Exception $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit property');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Properties'),
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        try {
            $property = $this->manager->property->get($id);
            $this->manager->remove($property);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Url::toRoute(['index']));
    }
}