<?php

namespace linex\modules\catalog\controllers\backend;

use linex\modules\catalog\helpers\ImportProfileHelper;
use Yii;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use linex\modules\catalog\models\collections\import\ProfileCollection;
use linex\modules\catalog\models\import\Profile;
use linex\modules\catalog\components\backend\CatalogController;
use linex\modules\catalog\managers\ImportProfileManager;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class ImportController
 * @package linex\modules\catalog\controllers\backend
 */
class ImportController extends CatalogController
{
    private $manager;

    /**
     * ImportController constructor.
     *
     * @param string               $id
     * @param \yii\base\Module     $module
     * @param ImportProfileManager $manager
     * @param array                $config
     */
    public function __construct($id, $module, ImportProfileManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ArrayDataProvider([
            'allModels'  => Profile::find()->orderBy('id')->all(),
            'modelClass' => Profile::className(),
            'sort'       => [
                'attributes' => ['id', 'name', 'sort', 'active'],
            ],
        ]);

        $this->view->title = CatalogModule::t('Import Profiles');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionAdd()
    {
        $post = Yii::$app->request->post();
        $collection = new ProfileCollection();

        if ($collection->load($post) && $collection->validate()) {
            try {
                $profile = $this->manager->create($collection);

                return $this->redirect([
                    'edit',
                    'id'  => $profile->id,
                    'tab' => 'category',
                ]);

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('New Profile');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Import Profiles'),
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     */
    public function actionEdit($id, $tab = null)
    {
        $tab = $tab ? $tab : 'common';

        $profile = $this->manager->profile->get($id);
        $post = Yii::$app->request->post();
        $collection = new ProfileCollection($profile);
        $remoteSections = ImportProfileHelper::getRemoteSections($profile->sections_url, $profile->api_key);

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($profile->id, $collection);

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'save':
                        return $this->redirect(['index']);
                    default:
                        return $this->redirect([
                            'edit',
                            'id' => $profile->id,
                        ]);
                }

            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = CatalogModule::t('Edit profile');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Import Profiles'),
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = $profile->name;

        return $this->render('edit', [
            'model'          => $collection,
            'remoteSections' => $remoteSections,
            'tab'            => $tab,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     */
    public function actionRun($id)
    {
        $profile = $this->manager->profile->get($id);

        $this->view->title = CatalogModule::t('Import products');
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::getInstance()->getName(),
            'url'   => Url::toRoute('/dashboard/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => CatalogModule::t('Import Profiles'),
            'url'   => Url::toRoute(['index']),
        ];
        $this->view->params['breadcrumbs'][] = $profile->name;

        return $this->render('run', [
            'model' => $profile,
        ]);
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     */
    public function actionDelete($id)
    {
        try {
            $profile = $this->manager->profile->get($id);
            $this->manager->remove($profile);
        } catch (\DomainException $e) {
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Url::toRoute(['index']));
    }
}