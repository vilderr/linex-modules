<?php

namespace linex\modules\catalog\controllers\frontend;

use linex\modules\catalog\managers\frontend\PropertyManager;
use linex\modules\catalog\models\SmartFilter;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Json;
use linex\modules\catalog\models\CategoryProperty;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\repositories\frontend\ProductRepository;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\catalog\models\Category;

/**
 * Class DefaultController
 * @package linex\modules\catalog\controllers\frontend
 */
class DefaultController extends Controller
{
    private $products;

    public function __construct($id, $module, ProductRepository $products, array $config = [])
    {
        $this->products = $products;

        parent::__construct($id, $module, $config);
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @param string $code
     * @param string $propertiesJson
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionSection($code, $propertiesJson = '')
    {
        if ($code === 'root') {
            $this->redirect('/catalog');
        }

        if (null === $category = Category::findBySlug($code)) {
            throw new NotFoundHttpException;
        }

        $rp = (array)Json::decode($propertiesJson);
        $manager = new PropertyManager($category, $rp);
        $smartFilter = new SmartFilter($manager);

        $dataProvider = $this->products->getList($category, $manager->request);

        $this->view->title = $category->name;
        //$this->view->params['category']['product_count'] = $dataProvider->getTotalCount();
        $this->view->params['breadcrumbs'][] = [
            'label' => \Yii::t('app', 'Catalog'),
            'url'   => Url::to('/catalog'),
        ];
        $this->view->params['breadcrumbs'][] = [
            'label' => $category->name,
        ];


        return $this->render('category', [
            'smartFilter' => $smartFilter,
            'category'     => $category,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $code
     *
     * @return string
     */
    public function actionProduct($code)
    {
        $model = $this->productManager->product->getWithCategory($code);

        $this->view->title = $model->name;

        return $this->render('product', [
            'model' => $model,
        ]);
    }
}