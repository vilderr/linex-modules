<?php
/**
 * Created by PhpStorm.
 * User: vilderr
 * Date: 31.07.17
 * Time: 11:04
 */

namespace linex\modules\catalog\controllers\frontend;


use linex\modules\catalog\managers\TagManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use linex\modules\catalog\Module;

class TagsController extends Controller
{
    public $manager;

    public function __construct($id, $module, TagManager $manager, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->manager = $manager;
    }

    public function actionIndex()
    {
        $this->view->title = Module::t('Tags');

        return $this->render('index');
    }

    public function actionTag($slug)
    {
        if (null === ($model = $this->manager->repository->findBySlug($slug))) {
            throw new NotFoundHttpException(Module::t('Element is not found'));
        }

        $this->view->title = $model->name;

        return $this->render('tag', [
            'model' => $model,
        ]);
    }
}