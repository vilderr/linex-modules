<?php

namespace linex\modules\catalog\managers;

use linex\modules\catalog\models\distribution\Part;
use Yii;
use linex\modules\catalog\models\collections\distribution\DistributionCollection;
use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\repositories\distribution\DistributionRepository;

/**
 * Class DistributionManager
 * @package linex\modules\catalog\managers
 */
final class DistributionManager
{
    public $repository;

    /**
     * DistributionManager constructor.
     *
     * @param DistributionRepository $repository
     */
    public function __construct(DistributionRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param DistributionCollection $collection
     *
     * @return Distribution
     */
    public function create(DistributionCollection $collection)
    {
        $distribution = Distribution::create(
            $collection->name,
            $collection->description,
            $collection->sort,
            $collection->active
        );

        $this->repository->save($distribution);

        return $distribution;
    }

    /**
     * @param                        $id
     * @param DistributionCollection $collection
     */
    public function edit($id, DistributionCollection $collection)
    {
        $distribution = $this->repository->get($id);

        $distribution->edit(
            $collection->name,
            $collection->description,
            $collection->sort,
            $collection->active
        );

        $distribution->assignParts($collection->parts);

        $this->repository->save($distribution);
    }

    public function remove($id)
    {
        $distribution = $this->repository->get($id);
        $this->repository->remove($distribution);
    }
}