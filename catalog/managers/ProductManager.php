<?php

namespace linex\modules\catalog\managers;

use linex\modules\catalog\repositories\TagRepository;
use linex\modules\catalog\models\collections\ProductCollection;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\repositories\ProductRepository;

/**
 * Class ProductManager
 * @package linex\modules\catalog\managers
 */
final class ProductManager
{
    public $product;
    public $tags;

    public function __construct(ProductRepository $product, TagRepository $tags)
    {
        $this->product = $product;
        $this->tags = $tags;
    }

    /**
     * @param ProductCollection $collection
     *
     * @return Product
     */
    public function create(ProductCollection $collection)
    {
        $product = Product::create(
            $collection->category_id,
            $collection->name,
            $collection->xml_id,
            $collection->tmp_id,
            $collection->description,
            $collection->current_props,
            $collection->current_section,
            $collection->url,
            $collection->price,
            $collection->old_price,
            $collection->discount,
            $collection->sort,
            $collection->active,
            $collection->created_by
        );

        foreach ($collection->values as $value) {
            if ($value->items)
                $product->collectValues($value->property->id, $value->items);
        }
        $product->setValue();

        foreach ($collection->tags->values as $tagId) {
            $tag = $this->tags->get($tagId);
            $product->assignTag($tag->id);
        }

        $this->product->save($product);

        return $product;
    }

    /**
     * @param                   $id
     * @param ProductCollection $collection
     *
     * @return void
     */
    public function edit($id, ProductCollection $collection)
    {
        $product = $this->product->get($id);

        $product->edit(
            $collection->category_id,
            $collection->name,
            $collection->xml_id,
            $collection->tmp_id,
            $collection->description,
            $collection->current_props,
            $collection->current_section,
            $collection->url,
            $collection->price,
            $collection->old_price,
            $collection->discount,
            $collection->sort,
            $collection->active,
            $collection->created_by
        );

        $product->revokeTags();
        $this->product->save($product);

        foreach ($collection->values as $value) {
            if ($value->items)
                $product->collectValues($value->property->id, $value->items);
        }

        $product->setValue();

        foreach ($collection->tags->values as $tagId) {
            $tag = $this->tags->get($tagId);
            $product->assignTag($tag->id);
        }

        $this->product->save($product);
    }

    /**
     * @param $id
     *
     * @return void
     */
    public function remove($id)
    {
        $product = $this->product->get($id);
        $this->product->remove($product);
    }
}