<?php

namespace linex\modules\catalog\managers;

use linex\modules\catalog\models\collections\import\ProfileCollection;
use linex\modules\catalog\models\import\Profile;
use linex\modules\catalog\repositories\import\ProfileRepository;

/**
 * Class ImportProfileManager
 * @package linex\modules\catalog\managers
 */
class ImportProfileManager
{
    public $profile;

    /**
     * ImportProfileManager constructor.
     *
     * @param ProfileRepository $profile
     */
    public function __construct(ProfileRepository $profile)
    {
        $this->profile = $profile;
    }

    /**
     * @param ProfileCollection $collection
     *
     * @return Profile
     */
    public function create(ProfileCollection $collection)
    {
        $profile = Profile::create(
            $collection->name,
            $collection->sections_url,
            $collection->products_url,
            $collection->api_key,
            $collection->sort,
            $collection->active
        );

        $this->profile->save($profile);

        return $profile;
    }

    /**
     * @param                         $id
     * @param ProfileCollection       $collection
     *
     * @return void
     */
    public function edit($id, ProfileCollection $collection)
    {
        $profile = $this->profile->get($id);

        $profile->edit(
            $collection->name,
            $collection->sections_url,
            $collection->products_url,
            $collection->api_key,
            $collection->sort,
            $collection->active
        );

        $profile->revokeSections();
        $this->profile->save($profile);

        foreach ($collection->sections->values as $id) {
            $profile->assignSection($id);
        }

        $this->profile->save($profile);
    }

    public function remove(Profile $profile)
    {
        $this->profile->remove($profile);
    }
}