<?php

namespace linex\modules\catalog\managers;

use linex\modules\catalog\models\collections\PropertyValueCollection;
use linex\modules\catalog\models\PropertyValue;
use linex\modules\catalog\repositories\PropertyValueRepository;

/**
 * Class PropertyValueManager
 * @package linex\modules\catalog\managers
 */
class PropertyValueManager
{
    public $value;

    /**
     * PropertyValueManager constructor.
     *
     * @param PropertyValueRepository $value
     */
    public function __construct(PropertyValueRepository $value)
    {
        $this->value = $value;
    }

    /**
     * @param PropertyValueCollection $collection
     *
     * @return PropertyValue
     */
    public function create(PropertyValueCollection $collection)
    {
        $value = PropertyValue::create(
            $collection->property_id,
            $collection->name,
            $collection->slug,
            $collection->xml_id,
            $collection->sort,
            $collection->active
        );

        $this->value->save($value);

        return $value;
    }

    /**
     * @param                         $id
     * @param PropertyValueCollection $collection
     *
     * @return void
     */
    public function edit($id, PropertyValueCollection $collection)
    {
        $value = $this->value->get($id);

        $value->edit(
            $collection->name,
            $collection->slug,
            $collection->xml_id,
            $collection->sort,
            $collection->active
        );

        $this->value->save($value);
    }

    /**
     * @param PropertyValue $value
     *
     * @return void
     */
    public function remove(PropertyValue $value)
    {
        $this->value->remove($value);
    }
}