<?php

namespace linex\modules\catalog\managers\frontend;

use yii\base\Model;
use yii\db\ActiveQuery;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\CategoryProperty;
use linex\modules\catalog\models\PropertyValue;
use linex\modules\catalog\models\Product;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\catalog\models\ProductValue;

/**
 * Class PropertyManager
 * @package linex\modules\catalog\managers\frontend
 */
class PropertyManager extends Model
{
    /** @var Category */
    public $category;
    public $items;
    public $slugs;
    public $request;

    private $_requestProperties;

    public function __construct(Category $category, array $properties = [], array $config = [])
    {
        $this->category = $category;
        $this->request = [
            'sef'   => [],
            'query' => [],
            'all'   => [],
        ];

        $this->_requestProperties = $properties;

        parent::__construct($config);
    }

    public function init()
    {
        $this->items = CategoryProperty::getArray($this->category, true);
        $this->slugs = ArrayHelper::map($this->items, 'slug', 'property_id');
        $this->normalizeRequest();
    }

    private function normalizeRequest()
    {
        foreach ($this->_requestProperties as $type => $request) {
            switch ($type) {
                case 'sef':
                    foreach ($request as $slug => $value) {
                        if (!ArrayHelper::keyExists($slug, $this->slugs)) {
                            throw new NotFoundHttpException();
                        }

                        $pid = $this->slugs[$slug];
                        if (null === $property = PropertyValue::getPropertyValue($this->items[$pid]['property_id'], $value)) {
                            throw new NotFoundHttpException();
                        }

                        if (!$this->items[$pid]['sef']) {
                            throw new NotFoundHttpException();
                        }

                        $this->request['sef'][$slug] = $property['slug'];
                        $this->request['all'][$this->items[$pid]['property_id']] = $property;
                    }
                    break;

                case 'query':
                    foreach ($request as $slug => $value) {
                        if (ArrayHelper::keyExists($slug, $this->slugs)) {
                            $pid = $this->slugs[$slug];
                            $property = PropertyValue::getPropertyValue($this->items[$pid]['property_id'], $value);
                            if ($property && !$this->items[$pid]['sef']) {
                                $this->request['query'][$slug] = $property['slug'];
                                $this->request['all'][$this->items[$pid]['property_id']] = $property;
                            }
                        }
                    }
                    break;
            }
        }

        //echo '<pre>'; print_r($this->request); echo '</pre>';
    }


    /**
     * @return ActiveQuery
     */
    public function getCategoryValues()
    {
        $subQuery = (new Query())->select(['PP.id'])->from(Product::tableName() . ' PP');
        $subQuery->leftJoin(Category::tableName() . ' C', 'PP.category_id = C.id');
        $subQuery->where([
            'and',
            ['PP.active' => ModelHelper::STATUS_ACTIVE],
            ['C.global_active' => ModelHelper::STATUS_ACTIVE],
            ['>=', 'C.lft', $this->category->lft],
            ['<=', 'C.rgt', $this->category->rgt],
        ]);

        $query = ProductValue::find()->alias('PV');
        $query->select([
            'PV.property_id property_id',
            'PV.value_id value',
            'COUNT(DISTINCT PV.product_id) count',
        ]);
        $query->leftJoin(['P' => $subQuery], 'PV.product_id = P.id');
        $query->where(['PV.property_id' => array_keys($this->items)]);

        foreach ($this->request['all'] as $PID => $value) {
            $alias = 'PV' . $PID;

            $query->innerJoin(ProductValue::tableName() . $alias, $alias . '.product_id = P.id');
            $query->andWhere([$alias . '.property_id' => $PID, $alias . '.value_id' => $value['id']]);
        }

        $query->groupBy(['PV.property_id', 'PV.value_id']);
        $query->orderBy('count DESC');
        $query->asArray();

        return $query;
    }

    /*
    public function getCategoryValues()
    {
        $query = Product::find()->alias('PP');
        $query->select([
            'V.property_id property_id',
            'V.value_id value',
            'COUNT(DISTINCT V.product_id) count'
        ]);
        $query->joinWith('category C', false);
        $query->where([
            'and',
            ['PP.active' => ModelHelper::STATUS_ACTIVE],
            ['C.global_active' => ModelHelper::STATUS_ACTIVE],
            ['>=','C.lft', $this->category->lft],
            ['<=', 'C.rgt', $this->category->rgt],
        ]);
        $query->innerJoinWith('values V', false);
        $query->innerJoin(PropertyValue::tableName(). ' PV', 'V.value_id = PV.id');
        $query->leftJoin(Property::tableName(). ' P', 'V.property_id = P.id');
        $query->andWhere(['P.id' => array_keys($this->items), 'P.filtrable' => ModelHelper::STATUS_ACTIVE, 'PV.active' => 1]);

        $query->groupBy(['V.property_id', 'V.value_id']);
        $query->orderBy('count DESC');
        $query->asArray();

        return $query;
    }
    */

}