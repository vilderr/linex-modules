<?php

namespace linex\modules\catalog\managers;

use lhs\Yii2SaveRelationsBehavior\SaveRelationsTrait;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\CategoryProperty;
use linex\modules\catalog\models\collections\CategoryCollection;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\repositories\CategoryRepository;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\repositories\ProductRepository;
use yii\helpers\ArrayHelper;

/**
 * Class CategoryManager
 * @package linex\modules\catalog\manage
 */
final class CategoryManager
{
    public $category;
    public $product;

    /**
     * CategoryManager constructor.
     *
     * @param CategoryRepository $category
     * @param ProductRepository  $product
     */
    public function __construct(CategoryRepository $category, ProductRepository $product)
    {
        $this->category = $category;
        $this->product = $product;
    }

    /**
     * @param CategoryCollection $collection
     *
     * @return Category
     */
    public function create(CategoryCollection $collection)
    {
        $parent = $this->category->get($collection->parent_id);

        $category = Category::create(
            $collection->name,
            $collection->slug,
            $collection->xml_id,
            $collection->description,
            $collection->sort,
            $collection->active,
            $collection->global_active,
            $collection->created_by,
            $collection->parent_id
        );

        $category->appendTo($parent);

        $this->category->save($category);

        $ar = [];
        foreach ($collection->properties->values as $value)
        {
            $ar[] = ['category_id' => $category->id, 'property_id' => $value, 'filtrable' => 1];
        }

        $category->properties = $ar;
        $this->category->save($category);

        return $category;
    }

    /**
     * @param                    $id
     * @param CategoryCollection $collection
     *
     * @return void
     */
    public function edit($id, CategoryCollection $collection)
    {
        $category = $this->category->get($id);
        $this->checkIsNotRoot($category);

        $category->edit(
            $collection->name,
            $collection->slug,
            $collection->xml_id,
            $collection->description,
            $collection->sort,
            $collection->active,
            $collection->global_active,
            $collection->parent_id
        );

        if ($collection->parent_id !== $category->parent->id) {
            $parent = $this->category->get($collection->parent_id);
            $category->appendTo($parent);
        }

        $category->revokeProperties();
        $this->category->save($category);

        $ar = [];
        foreach (CategoryProperty::getArray($category) as $row) {
            $filtrable = 0;
            if (ArrayHelper::isIn($row['property_id'], (array)$collection->properties->values)) {
                $filtrable = 1;
            }

            $ar[] = ['category_id' => $category->id, 'property_id' => $row['property_id'], 'filtrable' => $filtrable];
        }

        $category->properties = $ar;

        $this->category->save($category);
    }

    /**
     * @param $id
     *
     * @return void
     */
    public function remove($id)
    {
        $category = $this->category->get($id);
        $this->checkIsNotRoot($category);

        $this->category->remove($category);
    }

    /**
     * @param Category $category
     *
     * @return void
     */
    private function checkIsNotRoot(Category $category)
    {
        if ($category->isRoot()) {
            throw new \DomainException(CatalogModule::t('Unable to manage the root category'));
        }
    }
}