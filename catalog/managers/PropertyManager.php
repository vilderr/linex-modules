<?php

namespace linex\modules\catalog\managers;

use linex\modules\catalog\models\collections\PropertyCollection;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\repositories\PropertyRepository;

/**
 * Class PropertyManager
 * @package linex\modules\catalog\managers
 */
final class PropertyManager
{
    public $property;

    /**
     * PropertyManager constructor.
     *
     * @param PropertyRepository $property
     */
    public function __construct(PropertyRepository $property)
    {
        $this->property = $property;
    }

    /**
     * @param PropertyCollection $collection
     *
     * @return Property
     */
    public function create(PropertyCollection $collection)
    {
        $property = Property::create(
            $collection->name,
            $collection->slug,
            $collection->xml_id,
            $collection->sort,
            $collection->active,
            $collection->filtrable,
            $collection->sef
        );

        $this->property->save($property);

        return $property;
    }

    /**
     * @param                    $id
     * @param PropertyCollection $collection
     *
     * @return void
     */
    public function edit($id, PropertyCollection $collection)
    {
        $property = $this->property->get($id);

        $property->edit(
            $collection->name,
            $collection->slug,
            $collection->xml_id,
            $collection->sort,
            $collection->active,
            $collection->filtrable,
            $collection->sef
        );

        $this->property->save($property);
    }

    /**
     * @param $id
     *
     * @return void
     */
    public function remove($id)
    {
        $product = $this->property->get($id);
        $this->property->remove($product);
    }
}