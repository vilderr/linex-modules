<?php

namespace linex\modules\catalog\managers;

use linex\modules\catalog\models\collections\TagCollection;
use linex\modules\catalog\models\Tag;
use linex\modules\catalog\repositories\TagRepository;

/**
 * Class TagManager
 * @package linex\modules\catalog\managers
 */
final class TagManager
{
    public $repository;

    /**
     * TagManager constructor.
     *
     * @param TagRepository $repository
     */
    public function __construct(TagRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param TagCollection $collection
     *
     * @return Tag
     */
    public function create(TagCollection $collection)
    {
        $tag = Tag::create(
            $collection->name,
            $collection->slug,
            $collection->sort,
            $collection->active
        );

        $this->repository->save($tag);

        return $tag;
    }

    /**
     * @param               $id
     * @param TagCollection $collection
     *
     * @return void
     */
    public function edit($id, TagCollection $collection)
    {
        $tag = $this->repository->get($id);

        $tag->edit(
            $collection->name,
            $collection->slug,
            $collection->sort,
            $collection->active
        );
        $this->repository->save($tag);
    }
}