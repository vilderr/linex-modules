<?php

namespace linex\modules\catalog\widgets\backend\filter;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use linex\modules\catalog\models\search\ProductSearch;

/**
 * Class Filter
 * @package linex\modules\catalog\widgets\backend\filter
 */
class Filter extends Widget
{
    public $model;
    public $category_id = null;

    private $_errors = [];
    private $_request;

    public function init()
    {
        parent::init();

        if (!$this->hasProductSearch()) {
            throw new InvalidConfigException("'Model' must be specified and will be valid 'ProductSearch' model object");
        }

        if (null === $this->category_id)
            $this->category_id = 1;
    }

    public function run()
    {
        $this->_request = Yii::$app->request;

        return $this->render('filter', [
            'model'       => $this->model,
            'category_id' => $this->category_id,
        ]);
    }

    /**
     * @return bool
     */
    protected function hasProductSearch()
    {
        return $this->model instanceof ProductSearch;
    }
}