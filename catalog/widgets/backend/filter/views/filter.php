<?php
use yii\helpers\Html;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\dashboard\components\ActiveForm;
use linex\modules\main\Module as MainModule;
use linex\modules\catalog\helpers\ProductHelper;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\search\ProductSearch
 * @var $category_id
 */
?>
<div class="row">
    <div class="col-lg-8 col-md-9">
        <div class="panel panel-default flat">
            <div class="panel-heading">
                <h4 class="panel-title"><?= Yii::t('app','Filter'); ?></h4>
            </div>
            <? $form = ActiveForm::begin([
                'id'         => 'catalog-product-filter',
                'type'       => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL],
                'method'     => 'get',
            ]); ?>
            <div class="panel-body">
                <?= $form->field($model, 'id')->textInput(); ?>
                <?= $form->field($model, 'name')->textInput(); ?>
                <?= $form->field($model, 'category_id')->dropDownList($model->collection->categoriesList()); ?>
                <?= $form->field($model, 'active')->dropDownList(ProductHelper::statusList(), ['prompt' => CatalogModule::t('Anyone')]); ?>
                <?= $form->field($model, 'current_section')->textInput(); ?>
                <?= $form->field($model, 'current_props')->textInput(); ?>
                <?= $form->field($model, 'price_from')->textInput(); ?>
                <?= $form->field($model, 'price_to')->textInput(); ?>
            </div>
            <div class="panel-footer">
                <?=
                Html::submitButton(
                    Yii::t('app', 'Apply'),
                    [
                        'class' => 'btn btn-info btn-flat',
                        'name'  => 'action',
                        'value' => 'apply',
                    ]
                )
                ?>
                <?=
                Html::a(Yii::t('app', 'Reset'), ['index', 'category_id' => $category_id], ['class' => 'btn btn-default btn-flat']); ?>
            </div>
            <? ActiveForm::end(); ?>
        </div>
    </div>
</div>
