<?php

namespace linex\modules\catalog\widgets\backend\distribution;

use linex\modules\catalog\models\collections\ProductCollection;
use linex\modules\catalog\repositories\TagRepository;
use yii\helpers\Json;
use linex\modules\catalog\managers\ProductManager;
use linex\modules\catalog\repositories\ProductRepository;
use linex\modules\catalog\managers\DistributionManager;
use linex\modules\catalog\repositories\distribution\DistributionRepository;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\distribution\Part;

/**
 * Class Parser
 * @package linex\modules\catalog\widgets\backend\distribution
 */
final class Parser
{
    public $model;

    private $_ns;

    /**
     * @var Part[]
     */
    private $parts;

    private $_distributionManager;
    private $_productManager;

    /**
     * Parser constructor.
     *
     * @param $ns
     */
    public function __construct(&$ns)
    {
        $this->_ns = &$ns;

        $distributionRepository = new DistributionRepository();
        $this->_distributionManager = new DistributionManager($distributionRepository);
        $this->model = $this->_distributionManager->repository->get($this->_ns['id']);

        $tagRepository = new TagRepository();
        $productRepository = new ProductRepository();
        $this->_productManager = new ProductManager($productRepository, $tagRepository);

        $this->parts = $this->model->getParts()->andWhere(['active' => 1])->all();
        $this->_ns['count'] = count($this->parts);
    }

    /**
     * @param     $key
     * @param     $start_time
     * @param int $interval
     *
     * @return array
     */
    public function parseElements($key, $start_time, $interval = 30)
    {
        $counter = [
            'upd' => 0,
            'err' => 0,
            'crc' => 0,
        ];

        $part = $this->parts[$key];
        $filter = Json::decode($part->filter_json);
        $operations = Json::decode($part->operation_json);

        $query = Product::find()
            ->limit(1000)
            ->where(['>', 'id', $this->_ns['last_id']]);

        foreach ($filter as $name => $value) {
            switch ($name) {
                case 'filter_name':
                    $query->andFilterWhere(['like', 'name', $value]);
                    break;
                case 'filter_category_id':
                    if ($value > 0) {
                        $query->andFilterWhere(['category_id' => $value]);
                    }
                    break;
                case 'filter_active':
                    if ($value != '') {
                        $query->andFilterWhere(['active' => $value]);
                    }
                    break;
                case 'filter_current_props':
                    $query->andFilterWhere(['like', 'current_props', $value]);
                    break;
                case 'filter_current_section':
                    $query->andFilterWhere(['like', 'current_section', $value]);
                    break;
                case 'filter_price_from':
                    $query->andFilterWhere(['>=', 'price', $value]);
                    break;
                case 'filter_price_to':
                    $query->andFilterWhere(['<', 'price', $value]);
                    break;
            }
        }

        $query->orderBy('id');

        foreach ($query->all() as $product) {
            $this->updateProduct($counter, $product, $operations);
            $counter['crc']++;
            $this->_ns['last_id'] = $product->id;
            if ($interval > 0 && (time() - $start_time) > $interval)
                break;
        }

        return $counter;
    }

    /**
     * @param $counter
     * @param $product
     * @param $operations
     *
     * @return void
     */
    private function updateProduct(&$counter, $product, $operations)
    {
        $collection = new ProductCollection($product);
        foreach ($operations as $name => $value) {
            switch ($name) {
                case 'operation_category_id':
                    if ($value > 0) {
                        $collection->category_id = $value;
                    }
                    break;
                case 'operation_active':
                    if ($value != '') {
                        $collection->active = $value;
                    }
                    break;
                case 'operation_tags':
                    if ($value != '')
                        $collection->tags->values = $value;
                    break;
            }
        }

        try {
            $this->_productManager->edit($product->id, $collection);
            $counter['upd']++;
        } catch (\DomainException $e) {
            $counter['err']++;
        }
    }
}