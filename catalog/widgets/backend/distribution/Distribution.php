<?php

namespace linex\modules\catalog\widgets\backend\distribution;

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;
use yii\helpers\Json;
use linex\modules\catalog\models\distribution\Distribution as Model;
use linex\modules\catalog\widgets\backend\distribution\assets\DistributionAsset;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class Distribution
 * @package linex\modules\catalog\widgets\backend\distribution
 */
class Distribution extends Widget
{
    public $model;

    private $_errors = [];
    private $_request;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (!$this->hasDistribution()) {
            throw new InvalidConfigException("'Model' must be specified and will be valid 'Distribution' object");
        }
    }

    public function run()
    {
        $view = $this->getView();
        DistributionAsset::register($view);

        $this->_request = Yii::$app->request;

        if ($this->_request->isPost && $this->_request->isAjax) {
            $post = $this->_request->post();

            if (isset($post['NS']) && is_array($post['NS'])) {
                $NS = $post['NS'];
            } else {
                $NS = [
                    'step'    => -1,
                    'id'      => $this->model->id,
                    'last_id' => 0,
                    'done'    => [
                        'upd' => 0,
                        'err' => 0,
                        'crc' => 0,
                    ],
                ];
            }

            $start_time = time();
            $parser = new Parser($NS);

            if ($NS['step'] < 0) {
                $NS['step']++;
            } elseif ($NS['step'] < $NS['count']) {
                $result = $parser->parseElements($NS['step'], $start_time);
                $counter = 0;
                foreach ($result as $key => $value) {
                    $NS['done'][$key] += $value;
                    $counter += $value;
                }

                if (!$counter) {
                    $NS['step']++;
                    $NS["last_id"] = 0;
                }
            }

            if (count($this->_errors) == 0) {
                if ($NS['step'] < $NS['count']) {
                    echo '<p class="bg-warning small notify"><span class="lead">' . CatalogModule::t('Distribution products in database...') . '</span><br><br>';
                    foreach ($NS['done'] as $name => $value) {
                        echo '<span><label>' . CatalogModule::t('distribution-' . $name . ': {value}', ['value' => $value]) . '</span><br>';
                    }
                    echo '</p>';
                    echo '<script>DoNext(' . Json::encode(["NS" => $NS]) . ');</script>';
                } else {
                    echo '<p class="bg-primary small notify"><span class="lead">' . CatalogModule::t('Distribution completed') . '</span><br><br>';
                    foreach ($NS['done'] as $name => $value) {
                        echo '<span><label>' . CatalogModule::t('distribution-' . $name . ': {value}', ['value' => $value]) . '</span><br>';
                    }
                    echo '</p>';
                    echo '<script>EndDistribution();</script>';
                }
            } else {
                echo '<script>EndDistribution();</script>';
            }

            Yii::$app->end();
        }

        return $this->render('distribution', [
            'model' => $this->model,
        ]);
    }

    /**
     * @return bool
     */
    protected function hasDistribution()
    {
        return $this->model instanceof Model;
    }
}