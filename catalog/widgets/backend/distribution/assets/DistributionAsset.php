<?php

namespace linex\modules\catalog\widgets\backend\distribution\assets;

use yii\web\AssetBundle;

/**
 * Class DistributionAsset
 * @package linex\modules\catalog\widgets\backend\distribution\assets
 */
class DistributionAsset extends AssetBundle
{
    public $sourcePath = '@linex/modules/catalog/widgets/backend/distribution/dist';

    public $js = [
        'js/distribution.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}