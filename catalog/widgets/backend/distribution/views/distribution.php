<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\main\Module as MainModule;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\distribution\Distribution
 */
?>
<div class="panel panel-default flat">
    <div class="panel-body" id="distribution-result-container">
        <p class="lead">Для начала распределения нажмите кнопку "Запустить".</p>
        <p>Не запускайте скрипт в нескольких вкладках браузера!<br>Вы можете остановить операцию во время распределения,
            нажав кнопку "Остановить".</p>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(CatalogModule::t('Start'), [
            'id'      => 'start-button',
            'class'   => 'btn btn-primary btn-flat',
            'onclick' => 'StartDistribution();',
        ]); ?>
        <?= Html::submitButton(CatalogModule::t('Stop'), [
            'class'   => 'btn btn-default btn-flat',
            'onclick' => 'EndDistribution();',
        ]); ?>
        <div class="pull-right">
            <?= Html::a(Icon::show('pencil') . Yii::t('app', 'Edit'), ['edit', 'id' => $model->id], ['class' => 'btn btn-default btn-flat']); ?>
            <?= Html::a(Icon::show('angle-left') . CatalogModule::t('Distribution Profiles'), ['index'], ['class' => 'btn btn-default btn-flat']); ?>
        </div>
    </div>
</div>
