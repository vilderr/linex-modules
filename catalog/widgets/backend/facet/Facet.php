<?php

namespace linex\modules\catalog\widgets\backend\facet;

use Yii;
use yii\base\Widget;
use yii\bootstrap\Alert;
use yii\bootstrap\Progress;
use yii\web\Request;
use yii\helpers\Json;

/**
 * Class Facet
 * @package linex\modules\catalog\widgets\backend\facet
 */
class Facet extends Widget
{
    /** @var Request */
    private $_request;
    private $_errors = [];

    public function init()
    {
        $this->_request = Yii::$app->request;
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        $view = $this->getView();
        assets\FacetAsset::register($view);

        if ($this->_request->isPost && $this->_request->isAjax) {
            $post = $this->_request->post();

            if (isset($post['NS']) && is_array($post['NS'])) {
                $NS = $post['NS'];
            } else {
                $NS = [
                    'step'    => 0,
                    'last_id' => 0,
                    'count'   => 0,
                    'indexed' => 0,
                ];
            }

            $start_time = time();
            $parser = new Parser($NS);

            if ($NS['step'] < 1) {
                $parser->dropStorage();
                $parser->createStorage();
                $NS['step']++;
            } elseif ($NS['step'] < 2) {
                $NS['count'] = $parser->getProductCount();
                if ($NS['count'] > 0)
                    $NS['step']++;
                else
                    $this->_errors[] = 'Отсутствуют подходящие товары для индексации, фасет создан не будет!';
            } elseif ($NS['step'] < 3) {
                $start_time = time();
                $limit = 30;
                $result = $parser->indexProducts($start_time, $limit);
                if ($result > 0) {
                    $NS['indexed'] += $result;
                } else {
                    $NS['step']++;
                }
            }

            foreach ($this->_errors as $error) {
                echo Alert::widget([
                    'options'     => [
                        'class' => 'alert-danger flat',
                    ],
                    'body'        => $error,
                    'closeButton' => false,
                ]);
            }

            if (count($this->_errors) == 0) {
                if ($NS['step'] < 3) {
                    if ($NS['step'] < 2) {

                    } elseif ($NS['step'] < 3) {
                        $percent = ceil(($NS['indexed'] * 100) / $NS['count']);
                        echo '<p>Для корректной работы каталога, дождитесь окончания работы скрипта...</p>';
                        echo Progress::widget([
                            'percent' => $percent,
                            'label'   => $percent . '%',
                            'options' => ['class' => 'flat'],
                        ]);
                    }
                    echo '<script>DoNext(' . Json::encode(["NS" => $NS]) . ');</script>';
                } else {
                    $percent = ceil(($NS['indexed'] * 100) / $NS['count']);
                    echo Alert::widget([
                        'options'     => [
                            'class' => 'alert-info flat',
                        ],
                        'body'        => 'Индексация элементов завершена!',
                        'closeButton' => false,
                    ]);
                    echo Progress::widget([
                        'percent' => $percent,
                        'label'   => $percent . '%',
                        'options' => ['class' => 'flat'],
                    ]);

                    echo '<script>EndIndex();</script>';
                }
            } else {
                echo '<script>EndIndex();</script>';
            }


            Yii::$app->end();
        }

        return $this->render('facet');
    }
}