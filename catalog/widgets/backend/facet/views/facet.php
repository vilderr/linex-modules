<?php
use yii\helpers\Html;
use linex\modules\catalog\Module as CatalogModule;

/**
 * @var $this \yii\web\View
 */
?>
<div class="panel panel-default flat">
    <div class="panel-body" id="facet-result-container">
        <p class="lead">Для начала индексации товаров нажмите кнопку "Запустить".</p>
        <p>Не запускайте скрипт в нескольких вкладках браузера!<br>Не закрывайте окно браузера до завершения индексации.</p>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(CatalogModule::t('Start'), [
            'id'      => 'start-button',
            'class'   => 'btn btn-primary btn-flat',
            'onclick' => 'StartIndex();',
        ]); ?>
    </div>
</div>
