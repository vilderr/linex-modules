var running = false;

function DoNext(NS) {
    "use strict";

    if (running) {
        var request = $.ajax({
            type: "POST",
            url: '',
            data: NS
        });

        request.done(function (result) {
            $('#facet-result-container').html(result);
        });
    }

}

StartIndex = function () {
    running = document.getElementById('start-button').disabled = true;
    DoNext();
};

EndIndex = function () {
    running = document.getElementById('start-button').disabled = false;
};