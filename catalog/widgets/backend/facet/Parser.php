<?php

namespace linex\modules\catalog\widgets\backend\facet;

use linex\modules\catalog\models\Product;
use linex\modules\catalog\models\Category;
use linex\modules\catalog\models\propertyindex\Indexer;

class Parser
{
    private $_ns;

    /** @var Indexer */
    protected $indexer;

    public function __construct(&$ns)
    {
        $this->_ns = &$ns;

        $this->indexer = new Indexer();
    }

    public function indexProducts($start_time, $time_limit, $limit = 1000)
    {
        $counter = 0;

        $res = Product::find()
            ->alias('P')
            ->innerJoin(Category::tableName() . 'C', 'C.id = P.category_id')
            ->where(['P.active' => 1])
            ->andWhere(['C.global_active' => 1])
            ->andWhere(['>', 'P.id', $this->_ns['last_id']])
            ->limit($limit)
            ->orderBy('P.id');

        foreach ($res->each(50) as $product) {
            if ($this->indexer->addProductIndex($product)) {
                $counter++;
            }

            $this->_ns['last_id'] = $product->id;
            if ($time_limit > 0 && (time() - $start_time) > $time_limit)
                break;
        }

        return $counter;
    }

    public function dropStorage()
    {
        $this->indexer->storage->remove();
    }

    public function createStorage()
    {
        $this->indexer->storage->create();
    }

    /**
     * @return int
     */
    public function getProductCount()
    {
        $count = Product::find()
            ->alias('P')
            ->innerJoin(Category::tableName() . 'C', 'C.id = P.category_id')
            ->where(['P.active' => 1])
            ->andWhere(['C.global_active' => 1])
            ->count();

        return intval($count);
    }
}