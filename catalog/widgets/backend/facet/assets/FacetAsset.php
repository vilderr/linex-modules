<?php

namespace linex\modules\catalog\widgets\backend\facet\assets;

use yii\web\AssetBundle;

/**
 * Class FacetAsset
 * @package linex\modules\catalog\widgets\backend\facet\assets
 */
class FacetAsset extends AssetBundle
{
    public $sourcePath = '@linex/modules/catalog/widgets/backend/facet/dist';

    public $js = [
        'js/facet.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}