<?php

namespace linex\modules\catalog\widgets\backend\assets;

use yii\web\AssetBundle;
/**
 * Class ImportAsset
 * @package linex\modules\catalog\widgets\backend\assets
 */
class ImportAsset extends AssetBundle
{
    public $sourcePath = '@linex/modules/catalog/widgets/backend/dist';

    public $js = [
        'js/import.js',
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset',
    ];

    public $publishOptions = [
        'forceCopy' => true,
    ];
}