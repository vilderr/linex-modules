<?php

use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\catalog\Module as CatalogModule;

/**
 * @var $this \yii\web\View
 */
?>
<div class="panel panel-default flat">
    <div class="panel-body" id="import-result-container">
        <p class="lead">Для начала импорта нажмите кнопку "Запустить".</p>
        <p>Не запускайте один профиль импорта в нескольких вкладках браузера!<br>Вы можете остановить операцию во время
            импорта, нажав кнопку "Остановить".</p>
    </div>
    <div class="panel-footer">
        <?= Html::submitButton(CatalogModule::t('Start'), [
            'id'      => 'start-button',
            'class'   => 'btn btn-primary btn-flat',
            'onclick' => 'StartImport();',
        ]); ?>
        <?= Html::submitButton(CatalogModule::t('Stop'), [
            'class'   => 'btn btn-default btn-flat',
            'onclick' => 'EndImport();',
        ]); ?>
        <?= Html::a(Icon::show('angle-left') . CatalogModule::t('Import Profiles'), ['index'], ['class' => 'btn btn-default btn-flat pull-right']); ?>
    </div>
</div>