<?php

namespace linex\modules\catalog\widgets\backend;

use Yii;
use yii\base\Model;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use linex\modules\catalog\models\import\ImportTree;
use linex\modules\catalog\models\import\Parser;
use linex\modules\catalog\widgets\backend\assets\ImportAsset;
use linex\modules\main\widgets\alert\Widget;
use linex\modules\catalog\Module as CatalogModule;

/**
 * Class Import
 * @package linex\modules\catalog\widgets\backend
 */
class Import extends Widget
{
    public $model;

    public $options = [];

    public $request;

    private $_errors = [];

    /**
     * widget initialize
     *
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!$this->hasModel()) {
            throw new InvalidConfigException("'Model' must be specified.");
        }
    }

    /**
     * @return string
     */
    public function run()
    {
        $view = $this->getView();
        ImportAsset::register($view);

        $this->request = Yii::$app->request;

        if ($this->request->isPost && $this->request->isAjax) {
            $post = $this->request->post();

            if (isset($post['NS']) && is_array($post['NS'])) {
                $NS = $post['NS'];
            } else {
                $NS = [
                    'step'       => 0,
                    'profile_id' => $this->model->id,
                    'url'        => $this->model->products_url,
                    'token'      => $this->model->api_key,
                    'sections'   => ArrayHelper::getColumn($this->model->sections, 'value'),
                    'last_id'    => 0,
                    'done'       => [
                        'add' => 0,
                        'upd' => 0,
                        'err' => 0,
                        'crc' => 0,
                    ],
                ];
            }

            if ($NS['step'] < 1) {
                ImportTree::deleteAll(['profile_id' => $this->model->id]);
                $NS['step']++;
            } elseif ($NS['step'] < 2) {
                $parser = new Parser($NS);
                $result = $parser->importElements(time(), 30);

                $counter = 0;
                foreach ($result as $key => $value) {
                    $NS['done'][$key] += $value;
                    $counter += $value;
                }
                if (!$counter)
                    $NS['step']++;
            }

            if (count($this->_errors) == 0) {
                if ($NS["step"] < 2) {
                    echo '<p class="bg-warning small notify"><span class="lead">' . CatalogModule::t('Import products from remote database') . '</span><br><br>';
                    foreach ($NS['done'] as $name => $value) {
                        echo '<span><label>' . CatalogModule::t('import-' . $name . ': {value}', ['value' => $value]) . '</span><br>';
                    }
                    echo '</p>';
                    echo '<script>DoNext(' . Json::encode(["NS" => $NS]) . ');</script>';
                } else {
                    echo '<p class="bg-primary small notify"><span class="lead">' . CatalogModule::t('Import completed') . '</span><br><br>';
                    foreach ($NS['done'] as $name => $value) {
                        echo '<span><label>' . CatalogModule::t('import-' . $name . ': {value}', ['value' => $value]) . '</span><br>';
                    }
                    echo '</p>';
                    echo '<script>EndImport();</script>';
                }
            }

            Yii::$app->end();
        }

        return $this->render('import');
    }

    /**
     * @return bool
     */
    protected function hasModel()
    {
        return $this->model instanceof Model;
    }
}