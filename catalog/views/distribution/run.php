<?php

use linex\modules\catalog\widgets\backend\distribution\Distribution;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\distribution\Distribution
 */
?>
<?= Distribution::widget([
    'model' => $model,
]); ?>
