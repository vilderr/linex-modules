<?php
use yii\helpers\Json;
use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\assets\backend\DistributionAsset;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\distribution\DistributionCollection
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $key   integer
 */

DistributionAsset::register($this);
?>
<div id="iterations-list">
    <? foreach ($model->parts as $key => $part): ?>
        <?= $this->render('_part', [
            'model' => $part,
            'key'   => $key,
            'form'  => $form,
            'owner' => $model
        ]); ?>
    <? endforeach; ?>
</div>
<div class="text-center">
    <?= Html::a(Icon::show('plus') . CatalogModule::t('Add iteration'), null, [
        'id'    => 'iteration-add-btn',
        'class' => 'btn btn-info btn-flat btn-lg add-part-btn',
    ]); ?>
</div>
<?
$arJSParams = [
    'type'            => 'list',
    'id'              => 'iterations-list',
    'distribution_id' => $model->distribution->id,
    'url'             => \yii\helpers\Url::toRoute('add-part'),
];
$this->registerJs('$("#iterations").distribution(' . Json::encode($arJSParams) . ');');
?>
