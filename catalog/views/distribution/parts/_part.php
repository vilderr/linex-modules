<?php
use yii\helpers\Html;
use yii\helpers\Json;
use kartik\icons\Icon;
use kartik\select2\Select2;
use linex\modules\catalog\helpers\CategoryHelper;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\main\Module as MainModule;
use linex\modules\catalog\helpers\DistributionHelper;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\distribution\PartCollection
 * @var $owner \linex\modules\catalog\models\collections\distribution\DistributionCollection
 * @var $key
 */
$part_id = 'distribution_part_' . $key;
?>
<div class="panel panel-default flat" id="<?= $part_id; ?>">
    <ul class="list-group">
        <li class="list-group-item">
            <h4>Фильтр</h4>
            <?= $form->field($model, '[' . $key . ']filter_name')->textInput(); ?>
            <?= $form->field($model, '[' . $key . ']filter_category_id')->dropDownList(CategoryHelper::tree(), ['prompt' => CatalogModule::t('Not choosed')]); ?>
            <?= $form->field($model, '[' . $key . ']filter_active')->dropDownList(DistributionHelper::statusList(), ['prompt' => CatalogModule::t('Anyone')]); ?>
            <?= $form->field($model, '[' . $key . ']filter_current_props')->textInput(); ?>
            <?= $form->field($model, '[' . $key . ']filter_current_section')->textInput(); ?>
            <div class="row">
                <div class="col-xs-6">
                    <?= $form->field($model, '[' . $key . ']filter_price_from')->textInput(); ?>
                </div>
                <div class="col-xs-6">
                    <?= $form->field($model, '[' . $key . ']filter_price_to')->textInput(); ?>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <h4>Действия</h4>
            <?= $form->field($model, '[' . $key . ']operation_category_id')->dropDownList(CategoryHelper::tree(), ['prompt' => CatalogModule::t('Not choosed')]); ?>
            <?= $form->field($model, '[' . $key . ']operation_active')->dropDownList(DistributionHelper::statusList(), ['prompt' => CatalogModule::t('Anyone')]); ?>
            <?= Html::hiddenInput($model->formName() . '[' . $key . '][operation_tags]'); ?>
            <label class="control-label"><?= $model->getAttributeLabel('operation_tags') ?></label>
            <?= Select2::widget([
                'name'          => $model->formName() . '[' . $key . '][operation_tags]',
                'value'         => $model->operation_tags,
                'data'          => $model->tagsList(),
                'maintainOrder' => true,
                'options'       => [
                    'multiple' => true,
                ],
                'pluginOptions' => [
                    'tags'               => true,
                    'maximumInputLength' => 10,
                ],
            ]);
            ?>
        </li>
        <li class="list-group-item">
            <h4>Дополнительно</h4>
            <?= $form->field($model, '[' . $key . ']sort')->textInput(); ?>
            <?= $form->field($model, '[' . $key . ']active')->checkbox(); ?>
        </li>
    </ul>
    <div class="panel-footer">
        <a href="javascript:void(0);"
           class="btn btn-danger btn-flat remove-btn"><?= Icon::show('trash-o') . Yii::t('app', 'Delete'); ?></a>
    </div>
</div>
<?
$arJSParams = [
    'type'            => 'part',
    'id'              => $key,
    'distribution_id' => $owner->distribution->id,
    'url'             => \yii\helpers\Url::toRoute('delete-part'),
];
$this->registerJs('$("#' . $part_id . '").distribution(' . Json::encode($arJSParams) . ');');
?>
