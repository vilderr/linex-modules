<?php
/**
 * @var $model \linex\modules\catalog\models\collections\distribution\DistributionCollection
 */
?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'description')->textarea(); ?>
<?= $form->field($model, 'sort')->textInput(); ?>
<?= $form->field($model, 'active')->checkbox(); ?>