<?php
use yii\helpers\Html;
use kartik\icons\Icon;
use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\main\Module as MainModule;
use linex\modules\catalog\models\distribution\Distribution;
use linex\modules\catalog\helpers\DistributionHelper;

/**
 * @var \yii\web\View
 * @var $dataProvider \yii\data\ArrayDataProvider
 */
?>
<div class="distribution-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'distribution-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'attribute' => 'name',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute([
                        'run',
                        'id' => $model->id,
                    ]));
                },
                'format'    => 'raw',
            ],
            'sort',
            [
                'attribute' => 'active',
                'filter'    => DistributionHelper::statusList(),
                'value'     => function (Distribution $model) {
                    return DistributionHelper::statusLabel($model->active);
                },
                'format'    => 'raw',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id, 'returnUrl' => \yii\helpers\Url::to(['index'])], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'heading' => CatalogModule::t('Distribution Profiles'),
                'before'  => Html::a(Icon::show('plus') . CatalogModule::t('Add profile'), ['add'], ['class' => 'btn btn-primary btn-flat']),
            ],

        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
