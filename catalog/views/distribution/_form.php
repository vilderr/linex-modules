<?php
use yii\helpers\Html;
use kartik\icons\Icon;
use yii\widgets\ActiveForm;
use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\catalog\Module as CatalogModule;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\distribution\DistributionCollection
 */

?>
<?php $form = ActiveForm::begin([
    'options' => [
        'id' => 'catalog-distribution-add',
    ],
]);
$tabs = [];
$tabs[] = [
    'label'   => CatalogModule::t('Common settings'),
    'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
    'active'  => true,
    'options' => [
        'id' => 'common',
    ],
];
if ($model->distribution) {
    $tabs[] = [
        'label'   => CatalogModule::t('Iterations settings'),
        'content' => $this->render('parts/iterations', ['form' => $form, 'model' => $model]),
        'options' => [
            'id' => 'iterations',
        ],
    ];
}
?>
<?= \yii\bootstrap\Tabs::widget([
    'id'          => 'distribution-form-tabs',
    'linkOptions' => [
        'class' => 'flat',
    ],
    'items'       => $tabs,
]); ?>
<div class="panel panel-default flat">
    <div class="panel-footer">
        <? if ($model->distribution): ?>
            <?=
            Html::submitButton(
                DashboardModule::t('{icon} Save', ['icon' => Icon::show('save')]),
                [
                    'class' => 'btn btn-primary btn-flat',
                    'name'  => 'action',
                    'value' => 'save',
                ]
            )
            ?>
        <? endif; ?>
        <? $btnClass = $model->distribution ? 'btn-default' : 'btn-primary'; ?>
        <?= Html::submitButton(
            DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn ' . $btnClass . ' btn-flat',
                'name'  => 'action',
                'value' => 'apply',
            ]);
        ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
