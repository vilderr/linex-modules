<?php
use kartik\select2\Select2;
use yii\helpers\Html;
/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\CategoryCollection
 */
?>
<div class="form-group">
    <?= Html::hiddenInput($model->properties->formName() . '[values]'); ?>
    <?= Select2::widget([
        'name'          => $model->properties->formName() . '[values]',
        'value'         => $model->properties->values,
        'data'          => $model->properties->variants(),
        'maintainOrder' => true,
        'options'       => [
            'multiple' => true,
        ],
        'pluginOptions' => [
            'tags'               => true,
            'maximumInputLength' => 10,
        ],
    ]);
    ?>
</div>
