<?php
/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\CategoryCollection
 */
?>
<?= $form->field($model, 'parent_id')->dropDownList($model->categoriesList()) ?>
