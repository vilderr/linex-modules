<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collecters\CategoryCollection
 */
?>
<div class="catalog-category-edit">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
