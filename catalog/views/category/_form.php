<?php

use yii\helpers\Html;
use kartik\icons\Icon;

use linex\modules\dashboard\components\ActiveForm;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\dashboard\Module as DashboardModule;

/**
 * @var $this  yii\web\View
 * @var $model \linex\modules\catalog\models\collections\CategoryCollection
 * @var $form  yii\widgets\ActiveForm
 */
//echo '<pre>'; print_r($model); echo '</pre>';
?>
<? $form = ActiveForm::begin(); ?>
<?
$items = [
    [
        'label'   => CatalogModule::t('Common settings'),
        'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
        'active'  => true,
        'options' => [
            'id' => 'common',
        ],
    ],
    [
        'label'   => CatalogModule::t('Categories'),
        'content' => $this->render('parts/category', ['form' => $form, 'model' => $model]),
        'options' => [
            'id' => 'category',
        ],
    ],
    [
        'label'   => CatalogModule::t('Properties'),
        'content' => $this->render('parts/property', ['form' => $form, 'model' => $model]),
        'options' => [
            'id' => 'property',
        ],
    ]
];
?>
<?= \yii\bootstrap\Tabs::widget([
    'id'          => 'category-form-tabs',
    'linkOptions' => [
        'class' => 'flat',
    ],
    'items'       => $items,
]);
?>
<div class="panel-footer">
    <?= Html::submitButton(DashboardModule::t('{icon} Save', ['icon' => Icon::show('save')]), [
        'class' => 'btn btn-primary btn-flat',
        'name'  => 'action',
        'value' => 'save',
    ]); ?>
    <?= Html::submitButton(DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]), [
        'class' => 'btn btn-default btn-flat',
        'name'  => 'action',
        'value' => 'apply',
    ]); ?>
</div>
<?php ActiveForm::end(); ?>
