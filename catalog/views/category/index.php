<?php
use yii\helpers\Html;

use kartik\icons\Icon;
use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;

use linex\modules\catalog\Module as CatalogModule;
use linex\modules\main\helpers\ModelHelper;
use linex\modules\catalog\models\Category;

/**
 * @var \yii\web\View
 * @var $category     \linex\modules\catalog\models\Category
 * @var $dataProvider \yii\data\ArrayDataProvider
 */
?>
<div class="catalog-category-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'catalog-category-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'attribute' => 'name',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute([
                        'index',
                        'id' => $model->id,
                    ]));
                },
                'format'    => 'raw',
            ],
            'sort',
            [
                'attribute' => 'active',
                'filter'    => ModelHelper::statusList(),
                'value'     => function (Category $model) {
                    return ModelHelper::statusLabel($model->active);
                },
                'format'    => 'raw',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id, 'returnUrl' => \yii\helpers\Url::to(['index', 'id' => $model->parent->id])], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id, 'parent_id' => $model->parent->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'heading' => CatalogModule::t('{parent}: categories list', ['parent' => $category->name]),
                'before'  => Html::a(Icon::show('plus') . CatalogModule::t('Add category'), ['add', 'id' => $category->id], ['class' => 'btn btn-primary btn-flat']) . ' '
                    . Html::a(CatalogModule::t('Elements'), ['/dashboard/catalog/product/index', 'category_id' => $category->id], ['class' => 'btn btn-default btn-flat']),
            ],

        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
