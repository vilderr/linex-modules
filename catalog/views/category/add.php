<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\CategoryCollection
 */
?>
<div class="catalog-category-add">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
