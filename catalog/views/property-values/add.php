<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\PropertyValueCollection
 */
?>
<div class="catalog-property-value-add">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>
