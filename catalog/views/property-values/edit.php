<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\forms\manage\PropertyValueForm
 */
?>
<div class="catalog-property-value-edit">
    <?= $this->render('_form', ['model' => $model]); ?>
</div>