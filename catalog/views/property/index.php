<?php
use yii\helpers\Html;
use kartik\dynagrid\DynaGrid;
use kartik\icons\Icon;
use kartik\grid\CheckboxColumn;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\models\Property;
use linex\modules\catalog\helpers\PropertyHelper;

/**
 * @var $this         \yii\web\View
 * @var $searchModel  \linex\modules\catalog\models\search\PropertySearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<div class="catalog-property-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'catalog-property-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'attribute' => 'name',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute([
                        '/dashboard/catalog/property-values/index',
                        'property_id' => $model->id,
                    ]));
                },
                'format'    => 'raw',
            ],
            'slug',
            'xml_id',
            'sort',
            [
                'attribute' => 'active',
                'filter'    => PropertyHelper::statusList(),
                'value'     => function (Property $model) {
                    return PropertyHelper::statusLabel($model->active);
                },
                'format'    => 'raw',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'heading' => CatalogModule::t('Properties list'),
                'before'  => Html::a(Icon::show('plus') . CatalogModule::t('Add property'), ['add'], ['class' => 'btn btn-primary btn-flat']),
            ],

        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
