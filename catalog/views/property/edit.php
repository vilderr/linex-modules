<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\forms\manage\PropertyForm
 */
?>
<div class="catalog-property-edit">
    <?= $this->render('_form', [
        'model' => $model,
    ]); ?>
</div>
