<?php
use yii\helpers\Html;
use kartik\icons\Icon;

use linex\modules\dashboard\Module as DashboardModule;
use linex\modules\dashboard\components\ActiveForm;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\PropertyCollection
 */
?>
<div class="property-form panel panel-default flat">
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <?= $form->field($model, 'name')->textInput(['id' => 'property-title']); ?>
        <?= $form->field($model, 'slug', [
            'makeSlug' => "#property-title",
        ]); ?>
        <?= $form->field($model, 'xml_id')->textInput(); ?>
        <?= $form->field($model, 'sort')->textInput(); ?>
        <?= $form->field($model, 'active')->checkbox(); ?>
        <?= $form->field($model, 'filtrable')->checkbox(); ?>
        <?= $form->field($model, 'sef')->checkbox(); ?>
    </div>
    <div class="panel-footer">
        <?=
        Html::submitButton(
            DashboardModule::t('{icon} Save', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        )
        ?>
        <?= Html::submitButton(
            DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-default btn-flat',
                'name'  => 'action',
                'value' => 'apply',
            ]);
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
