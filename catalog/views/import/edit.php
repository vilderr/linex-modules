<?php
use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\dashboard\components\ActiveForm;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\dashboard\Module as DashboardModule;

/**
 * @var $this           \yii\web\View
 * @var $model          \linex\modules\catalog\models\collections\import\ProfileCollection
 * @var $tab            string
 * @var $remoteSections array
 */
?>
<div class="catalog-import-profile-add">
    <?php $form = ActiveForm::begin(); ?>
    <?= \yii\bootstrap\Tabs::widget([
        'id'          => 'catalog-import-profile-tabs',
        'linkOptions' => [
            'class' => 'flat',
        ],
        'items'       => [
            [
                'label'   => CatalogModule::t('Common settings'),
                'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
                'active'  => $tab == 'common' ? true : false,
                'options' => [
                    'id' => 'common',
                ],
            ],
            [
                'label'   => CatalogModule::t('Categories'),
                'content' => $this->render('parts/category', ['form' => $form, 'model' => $model, 'sections' => $remoteSections]),
                'active'  => $tab == 'category' ? true : false,
                'options' => [
                    'id' => 'category',
                ],
            ],
            [
                'label'   => CatalogModule::t('Advanced'),
                'content' => $this->render('parts/advanced', ['form' => $form, 'model' => $model]),
                'active'  => $tab == 'advanced' ? true : false,
                'options' => [
                    'id' => 'advanced',
                ],
            ],
        ],
    ]); ?>
    <div class="panel-footer">
        <?= Html::submitButton(
            DashboardModule::t('{icon} Save', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        )
        ?>
        <?= Html::submitButton(DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]), [
            'class' => 'btn btn-default btn-flat',
            'name'  => 'action',
            'value' => 'apply',
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
