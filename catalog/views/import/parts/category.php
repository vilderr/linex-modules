<?php

use linex\modules\dashboard\widgets\TreeCheckboxList;

/**
 * @var $form     \linex\modules\dashboard\components\ActiveForm
 * @var $model    \linex\modules\catalog\models\collections\import\ProfileCollection
 * @var $sections array
 */

?>
<?= $form->field($model->sections, 'values')->widget(TreeCheckboxList::className(), [
    'values' => $sections,
]); ?>
