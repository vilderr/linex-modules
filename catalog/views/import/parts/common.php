<?php
/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\import\ProfileCollection
 */
?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'sections_url')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'products_url')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'api_key')->textInput(['maxlength' => true]); ?>
<?= $form->field($model, 'sort')->textInput(); ?>
<?= $form->field($model, 'active')->checkbox(); ?>
