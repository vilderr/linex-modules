<?php
use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\dashboard\components\ActiveForm;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\dashboard\Module as DashboardModule;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\import\ProfileCollection
 */
?>
<div class="catalog-import-profile-add">
    <?php $form = ActiveForm::begin(); ?>
    <?= \yii\bootstrap\Tabs::widget([
        'id'          => 'catalog-import-profile-tabs',
        'linkOptions' => [
            'class' => 'flat',
        ],
        'items'       => [
            [
                'label'   => CatalogModule::t('Common settings'),
                'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
                'active'  => true,
                'options' => [
                    'id' => 'common',
                ],
            ],
        ],
    ]); ?>
    <div class="panel-footer">
        <?= Html::submitButton(DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]), [
            'class' => 'btn btn-default btn-flat',
            'name'  => 'action',
            'value' => 'apply',
        ]); ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
