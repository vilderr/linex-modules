<?php
use linex\modules\catalog\widgets\backend\Import;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\import\Profile
 */
?>
<?= Import::Widget([
    'model' => $model,
]); ?>
