<?php
use yii\helpers\Html;

use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;
use kartik\icons\Icon;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\main\Module as MainModule;

/**
 * @var $this         \yii\web\View
 * @var $searchModel  \linex\modules\catalog\models\search\TagSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<div class="catalog-tag-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'catalog-tag-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'attribute' => 'name',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute([
                        'edit',
                        'id'        => $model->id,
                        'returnUrl' => \yii\helpers\Url::to(['index']),
                    ]));
                },
                'format'    => 'raw',
            ],
            'slug',
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id, 'returnUrl' => \yii\helpers\Url::to(['index'])], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'heading' => CatalogModule::t('Tags'),
                'before'  => Html::a(Icon::show('plus') . CatalogModule::t('Add element'), ['add'], ['class' => 'btn btn-primary btn-flat']),
            ],

        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
