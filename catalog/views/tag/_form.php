<?php
use yii\helpers\Html;
use kartik\icons\Icon;
use linex\modules\dashboard\components\ActiveForm;
use linex\modules\dashboard\Module as DashboardModule;

/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\TagCollection
 */
?>
<div class="tag-form panel panel-default flat">
    <?php $form = ActiveForm::begin(); ?>
    <div class="panel-body">
        <?= $form->field($model, 'name')->textInput(['id' => 'tag-name']); ?>
        <?= $form->field($model, 'slug', [
            'makeSlug' => "#tag-name",
        ]); ?>
        <?= $form->field($model, 'sort')->textInput(); ?>
        <?= $form->field($model, 'active')->checkbox(); ?>
    </div>
    <div class="panel-footer">
        <?=
        Html::submitButton(
            DashboardModule::t('{icon} Save', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        )
        ?>
        <?= Html::submitButton(
            DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-default btn-flat',
                'name'  => 'action',
                'value' => 'apply',
            ]);
        ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
