<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\TagCollection
 */
?>
<?= $this->render('_form', [
    'model' => $model,
]); ?>
