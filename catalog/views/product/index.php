<?php
use yii\helpers\Html;

use kartik\dynagrid\DynaGrid;
use kartik\grid\CheckboxColumn;
use kartik\icons\Icon;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\catalog\models\Product;
use linex\modules\catalog\helpers\ProductHelper;
use linex\modules\catalog\widgets\backend\filter\Filter;

/**
 * @var $this         \yii\web\View
 * @var $category     \linex\modules\catalog\models\Category
 * @var $searchModel  \linex\modules\catalog\models\search\ProductSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
?>
<?= Filter::widget([
    'model'       => $searchModel,
    'category_id' => $category->id,
]); ?>
<div class="catalog-product-index">
    <?= DynaGrid::widget([
        'options'           => [
            'id' => 'catalog-category-grid',
        ],
        'columns'           => [
            [
                'class'   => CheckboxColumn::className(),
                'options' => [
                    'width' => '10px',
                ],
            ],
            [
                'class'     => 'yii\grid\DataColumn',
                'attribute' => 'id',
            ],
            [
                'attribute' => 'name',
                'value'     => function ($model) {
                    return Html::a($model->name, \yii\helpers\Url::toRoute([
                        'edit',
                        'id'        => $model->id,
                        'returnUrl' => \yii\helpers\Url::to(['index', 'category_id' => $model->category->id]),
                    ]));
                },
                'format'    => 'raw',
            ],
            [
                'attribute' => 'updated_at',
                'format'    => 'datetime',
            ],
            'price',
            'old_price',
            'discount',
            'sort',
            [
                'attribute' => 'active',
                'filter'    => ProductHelper::statusList(),
                'value'     => function (Product $model) {
                    return ProductHelper::statusLabel($model->active);
                },
                'format'    => 'raw',
            ],
            [
                'class'          => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'class' => 'text-center',
                ],
                'template'       => '<div class="btn-group">{update}{delete}</div>',
                'buttons'        => [
                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil"></i>', ['edit', 'id' => $model->id, 'returnUrl' => \yii\helpers\Url::to(['index', 'category_id' => $model->category->id])], ['class' => 'btn btn-primary btn-sm', 'title' => Yii::t('app', 'Edit')]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<i class="fa fa-trash"></i>', ['delete', 'id' => $model->id, 'category_id' => $model->category_id], ['class' => 'btn btn-danger btn-sm', 'data-method' => 'post', 'title' => Yii::t('app', 'Delete')]);
                    },
                ],
                'options'        => [
                    'width' => '90px',
                ],
            ],
        ],
        'theme'             => 'panel-default',
        'gridOptions'       => [
            'dataProvider' => $dataProvider,
            'hover'        => true,
            'panel'        => [
                'heading' => CatalogModule::t('{parent}: elements', ['parent' => $category->name]),
                'before'  => Html::a(Icon::show('plus') . CatalogModule::t('Add element'), ['add', 'category_id' => $category->id], ['class' => 'btn btn-primary btn-flat']) . ' '
                    . Html::a(CatalogModule::t('Categories'), ['/dashboard/catalog/category/index', 'id' => $category->id], ['class' => 'btn btn-default btn-flat']),
            ],

        ],
        'allowSortSetting'  => false,
        'allowThemeSetting' => false,
    ]); ?>
</div>
