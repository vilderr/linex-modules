<?php
use yii\helpers\Html;
use kartik\icons\Icon;

use linex\modules\dashboard\components\ActiveForm;
use linex\modules\catalog\Module as CatalogModule;
use linex\modules\dashboard\Module as DashboardModule;

/**
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>

<?php $form = ActiveForm::begin([
    'options' => [
        'id'      => 'catalog-product-add',
        'enctype' => 'multipart/form-data',
    ],
]); ?>

<?= \yii\bootstrap\Tabs::widget([
    'id'          => 'product-form-tabs',
    'linkOptions' => [
        'class' => 'flat',
    ],
    'items'       => [
        [
            'label'   => CatalogModule::t('Common settings'),
            'content' => $this->render('parts/common', ['form' => $form, 'model' => $model]),
            'active'  => true,
            'options' => [
                'id' => 'common',
            ],
        ],
        [
            'label'   => CatalogModule::t('Categories'),
            'content' => $this->render('parts/category', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'category',
            ],
        ],
        [
            'label'   => CatalogModule::t('Prices'),
            'content' => $this->render('parts/prices', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'prices',
            ],
        ],
        [
            'label'   => CatalogModule::t('Properties'),
            'content' => $this->render('parts/properties', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'properties',
            ],
        ],
        [
            'label'   => CatalogModule::t('Tags'),
            'content' => $this->render('parts/tags', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'tags',
            ],
        ],
        [
            'label'   => CatalogModule::t('Advanced'),
            'content' => $this->render('parts/advanced', ['form' => $form, 'model' => $model]),
            'options' => [
                'id' => 'advanced',
            ],
        ],
    ],
]); ?>
<div class="panel panel-default flat">
    <div class="panel-footer">
        <?=
        Html::submitButton(
            DashboardModule::t('{icon} Save', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        )
        ?>
        <?= Html::submitButton(
            DashboardModule::t('{icon} Apply', ['icon' => Icon::show('save')]),
            [
                'class' => 'btn btn-default btn-flat',
                'name'  => 'action',
                'value' => 'apply',
            ]);
        ?>
    </div>
</div>

<?php ActiveForm::end(); ?>

