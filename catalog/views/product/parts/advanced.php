<?php
/**
 * @var $form \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<?= $form->field($model, 'url')->textInput(['disabled' => true]); ?>