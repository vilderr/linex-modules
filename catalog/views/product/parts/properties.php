<?php
use yii\helpers\Html;
use kartik\select2\Select2;

/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<? foreach ($model->values as $id => $value): ?>
    <?= Html::hiddenInput($value->formName() . '[' . $id . '][items]'); ?>
    <div class="form-group">
        <label class="control-label"><?= $value->getAttributeLabel('items') ?></label>
        <?= Select2::widget([
            'name'          => $value->formName() . '[' . $id . '][items]',
            'value'         => $value->items,
            'data'          => $value->variants(),
            'maintainOrder' => true,
            'options'       => [
                'multiple' => true,
            ],
            'pluginOptions' => [
                'tags'               => true,
                'maximumInputLength' => 10,
            ],
        ]);
        ?>
    </div>
<? endforeach; ?>
<?= $form->field($model, 'current_props')->textInput(['disabled' => true]); ?>

