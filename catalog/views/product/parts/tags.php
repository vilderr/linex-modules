<?php
use yii\helpers\Html;
use kartik\select2\Select2;

/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<div class="form-group">
    <?= Html::hiddenInput($model->tags->formName() . '[values]'); ?>
    <label class="control-label"><?= $model->tags->getAttributeLabel('values') ?></label>
    <?= Select2::widget([
        'name'          => $model->tags->formName() . '[values]',
        'value'         => $model->tags->values,
        'data'          => $model->tags->tagsList(),
        'maintainOrder' => true,
        'options'       => [
            'multiple' => true,
        ],
        'pluginOptions' => [
            'tags'               => true,
            'maximumInputLength' => 10,
        ],
    ]);
    ?>
</div>