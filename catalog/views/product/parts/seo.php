<?php
/**
 * @var $model \linex\modules\catalog\forms\manage\product\ProductForm
 */
?>
<?= $form->field($model->meta, 'h1')->textInput() ?>
<?= $form->field($model->meta, 'title')->textInput() ?>
<?= $form->field($model->meta, 'description')->textarea(['rows' => 2]) ?>
<?= $form->field($model->meta, 'keywords')->textInput() ?>
