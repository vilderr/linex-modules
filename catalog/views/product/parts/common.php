<?php
use yii\helpers\Url;
use linex\modules\dashboard\widgets\Redactor;

/**
 * @var $form  \linex\modules\dashboard\components\ActiveForm;
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
<?= $form->field($model->image, 'files[]')->fileInput(); ?>
<?= $form->field($model, 'description')->widget(Redactor::className(), [
    'settings' => [
        'minHeight'        => 300,
        'maxHeight'        => 500,
        'imageManagerJson' => Url::to(['/dashboard/redactor/images-get']),
        'imageUpload'      => Url::to(['/dashboard/redactor/image-upload']),
        'plugins'          => [
            'imagemanager',
            'fullscreen',
        ],
    ],
]); ?>
<?= $form->field($model, 'xml_id')->textInput(); ?>
<?= $form->field($model, 'sort')->textInput(); ?>
<?= $form->field($model, 'active')->checkbox(); ?>
