<?php
/**
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<?= $form->field($model, 'price')->textInput(); ?>
<?= $form->field($model, 'old_price')->textInput(); ?>
<?= $form->field($model, 'discount')->textInput(); ?>