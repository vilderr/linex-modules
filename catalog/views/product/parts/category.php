<?php
/**
 * @var $form \linex\modules\dashboard\components\ActiveForm
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<?= $form->field($model, 'category_id')->dropDownList($model->categoriesList()) ?>
<?= $form->field($model, 'current_section')->textInput(['disabled' => true]); ?>
