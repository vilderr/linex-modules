<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\catalog\models\collections\ProductCollection
 */
?>
<?
//echo '<pre>';print_r($model);echo'</pre>';
?>
<div class="catalog-product">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
