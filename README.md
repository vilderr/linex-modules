Linex CMS modules
=================
Linex CMS modules package

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist linex/modules "*"
```

or add

```
"linex/modules": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \linex\modules\AutoloadExample::widget(); ?>```