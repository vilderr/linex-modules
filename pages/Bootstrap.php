<?php

namespace linex\modules\pages;

use yii\base\BootstrapInterface;

/**
 * Class Bootstrap
 * @package linex\modules\pages
 */
class Bootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations['modules/pages/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/pages/messages',
            'fileMap'          => [
                'modules/pages/module' => 'module.php',
            ],
        ];
    }
}