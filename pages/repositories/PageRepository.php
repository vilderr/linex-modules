<?php

namespace linex\modules\pages\repositories;

use linex\modules\pages\models\Page;
use linex\modules\pages\Module as PagesModule;
use yii\db\ActiveRecord;
use yii\data\ActiveDataProvider;

class PageRepository
{
    /**
     * @param $id
     *
     * @return Page
     */
    public function get($id)
    {
        if (!$category = Page::findOne($id)) {
            throw new \DomainException(PagesModule::t('Page is not found'));
        }

        return $category;
    }

    /**
     * @param Page $page
     */
    public function save(Page $page)
    {
        if (!$page->save()) {
            throw new \RuntimeException(PagesModule::t('Saving error'));
        }
    }

    /**
     * @param Page $page
     */
    public function remove(Page $page)
    {
        if (!$page->deleteWithChildren()) {
            throw new \RuntimeException(PagesModule::t('Removing error'));
        }
    }

    /**
     * @param $parent
     *
     * @return \yii\db\ActiveQuery
     */
    public function getQuery($parent)
    {
        if ($parent && $parent instanceof ActiveRecord) {
            $ids = $parent->getChildren()->select('id')->column();

            return Page::find()->where(['id' => $ids]);
        }

        return Page::find()->where(['lft' => 1]);
    }

    /**
     * @param $parent
     *
     * @return ActiveDataProvider
     */
    public function getProvider($parent)
    {
        return new ActiveDataProvider([
            'query'      => $this->getQuery($parent),
            'pagination' => [
                'forcePageParam'  => false,
                'defaultPageSize' => 30,
                'pageSizeLimit'   => [1, 50],
            ],
        ]);
    }
}