<?php

namespace linex\modules\pages\components;

use Yii;
use yii\web\UrlRuleInterface;
use linex\modules\pages\models\Page;

/**
 * Class Rules
 * @package linex\modules\pages\components
 */
class Rules implements UrlRuleInterface
{
    /**
     * @inheritdoc
     */
    public function createUrl($manager, $route, $params)
    {
        /** @var Page $model */
        if ($route == 'page/page/index') {
            $model=null;
            if (isset($params['model'])) {
                $model = $params['model'];
                unset($params['model']);
            } else {
                if (isset($params['id'])) {
                    $model = Page::findById($params['id']);
                    unset($params['id']);
                }
            }
            if (null !== $model) {
                $url = ($model->slug === 'mainpage') ? '' : $model->slug_path;
                $_query = http_build_query($params);
                $url = (!empty($_query)) ? $url . '?' . $_query : $url;
                return $url;
            }
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        if ($request->serverName == Yii::$app->getModule('main')->serverName
            && $request->port == Yii::$app->getModule('main')->serverPort
        ) {
            $_path = $request->getPathInfo();
        } else {
            $_path = $request->absoluteUrl;
        }

        $_path = !empty($_path) ? $_path : 'mainpage';

        if (null !== $model = Page::getByUrlPath($_path)) {
            return [
                '/pages/page/index',
                ['id' => $model['id']],
            ];
        }

        return false;
    }
}