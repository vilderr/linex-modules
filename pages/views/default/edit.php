<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\pages\models\Page
 */
?>
<div class="page-edit">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>