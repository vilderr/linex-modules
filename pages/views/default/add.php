<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\pages\models\collections\PageCollection
 */
?>
<div class="page-add">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>