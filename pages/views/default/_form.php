<?php
/**
 * @var $this  \yii\web\View
 * @var $model \linex\modules\pages\models\collections\PageCollection
 */

use yii\helpers\Url;
use yii\helpers\Html;

use kartik\icons\Icon;

use linex\modules\dashboard\components\ActiveForm;
use linex\modules\dashboard\widgets\Redactor;

?>
<div class="panel panel-default flat">
    <?php $form = ActiveForm::begin([
        'id'   => 'page-form',
        'type' => ActiveForm::TYPE_HORIZONTAL,
    ]); ?>
    <div class="panel-body">
        <?= $form->field($model, 'name')->textInput(['id' => 'page-title']); ?>
        <?= $form->field($model, 'slug', [
            'makeSlug' => "#page-title",
        ]); ?>
        <?= $form->field($model, 'content')->widget(Redactor::className(), [
            'settings' => [
                'minHeight'        => 300,
                'maxHeight'        => 500,
                'imageManagerJson' => Url::to(['/catalog/redactor/images-get']),
                'imageUpload'      => Url::to(['/catalog/redactor/image-upload']),
                'plugins'          => [
                    'imagemanager',
                    'fullscreen',
                ],
            ],
        ]); ?>
        <?= $form->field($model, 'published')->checkbox() ?>
        <?= $form->field($model, 'sort'); ?>
    </div>
    <div class="panel-footer">
        <?=
        Html::a(
            Icon::show('arrow-circle-left') . Yii::t('app', 'Back'),
            Yii::$app->request->get('returnUrl', ['/dashboard/pages']),
            ['class' => 'btn btn-danger btn-flat']
        )
        ?>
        <?=
        Html::submitButton(
            Icon::show('save') . Yii::t('app', 'Save'),
            [
                'class' => 'btn btn-primary btn-flat',
                'name'  => 'action',
                'value' => 'back',
            ]
        )
        ?>
        <?= Html::submitButton(
            Icon::show('save') . Yii::t('app', 'Apply'),
            [
                'class' => 'btn btn-warning btn-flat',
                'name'  => 'action',
                'value' => 'save',
            ]
        ); ?>
    </div>
    <? ActiveForm::end(); ?>
</div>
