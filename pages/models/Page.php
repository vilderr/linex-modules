<?php

namespace linex\modules\pages\models;

use Yii;
use yii\caching\TagDependency;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use linex\modules\pages\Module as PagesModule;
use linex\modules\main\helpers\ActiveRecordHelper;
use linex\modules\pages\behaviors\PagesRecalcBehavior;
use paulzi\nestedsets\NestedSetsBehavior;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string  $name
 * @property string  $slug
 * @property string  $slug_path
 * @property integer $published
 * @property string  $content
 * @property integer $sort
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property integer $global_published
 */
class Page extends \yii\db\ActiveRecord
{
    const DEFAULT_SORT = 500;
    const DEFAULT_PUBLISHED = 1;

    public static $SLUG_PATTERN = '/^[0-9a-z-]{0,60}$/';
    private static $identity_map = [];

    /**
     * @param $name
     * @param $slug
     * @param $content
     * @param $
     * @param $published
     * @param $global_published
     * @param $sort
     *
     * @return static
     */
    public static function create($name, $slug, $slug_path, $content, $published, $global_published, $sort)
    {
        return new static([
            'name'             => $name,
            'slug'             => $slug,
            'slug_path'        => $slug_path,
            'content'          => $content,
            'published'        => $published,
            'global_published' => $global_published,
            'sort'             => $sort,
        ]);
    }

    public function edit($name, $slug, $slug_path, $content, $published, $global_published, $sort)
    {
        $this->name = $name;
        $this->slug = $slug;
        $this->slug_path = $slug_path;
        $this->content = $content;
        $this->published = $published;
        $this->global_published = $global_published;
        $this->sort = $sort;
    }

    public function getProvider()
    {
        $query = self::find()->where(['parent_id' => $this->id]);

        $dataProvider = new ActiveDataProvider(
            [
                'query'      => $query,
                'sort'       => ['defaultOrder' => ['sort' => SORT_ASC]],
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]
        );

        return $dataProvider;
    }

    /**
     * @param int $id
     * @param int $is_published
     *
     * @return mixed
     */
    public static function findById($id, $is_published = 1)
    {
        if (!is_numeric($id)) {
            return null;
        }
        if (!isset(static::$identity_map[$id])) {

            $cacheKey = "Page:$id:$is_published";
            static::$identity_map[$id] = Yii::$app->cache->get($cacheKey);
            if (!is_object(static::$identity_map[$id])) {
                static::$identity_map[$id] = Page::find()->where(['id' => $id])->one();

                if (is_object(static::$identity_map[$id])) {
                    Yii::$app->cache->set(
                        $cacheKey,
                        static::$identity_map[$id],
                        86400,
                        new TagDependency([
                            'tags' => [
                                ActiveRecordHelper::getCommonTag(static::className()),
                            ],
                        ])
                    );
                }
            }
        }

        return static::$identity_map[$id];
    }

    /**
     * @param null $path
     *
     * @return array|mixed|null|\yii\db\ActiveRecord
     */
    public static function getByUrlPath($path = null)
    {
        if ((null === $path) || !is_string($path)) {
            return null;
        }

        $cacheKey = "Page:$path";
        if (($page = Yii::$app->cache->get($cacheKey)) === false) {
            $page = static::find()->where(['slug_path' => $path])->asArray()->one();
            $duration = 86400;
            if (!is_array($page)) {
                $duration = 3600;
            }

            Yii::$app->cache->set(
                $cacheKey,
                $page,
                $duration,
                new TagDependency([
                    'tags' => [
                        ActiveRecordHelper::getCommonTag(static::className()),
                    ],
                ])
            );
        }

        return $page;
    }

    public function beforeSave($insert)
    {
        if (!$insert) {
            // reset a cache tag to get a new parent model below
            TagDependency::invalidate(Yii::$app->cache, [ActiveRecordHelper::getCommonTag(self::className())]);
        }

        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('app', 'ID'),
            'slug'       => Yii::t('app', 'Slug'),
            'slug_path'  => PagesModule::t('Slug Path'),
            'published'  => PagesModule::t('Published'),
            'name'       => Yii::t('app', 'Name'),
            'content'    => PagesModule::t('Content'),
            'sort'       => Yii::t('app', 'Sort'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            'tags'   => [
                'class' => ActiveRecordHelper::className(),
            ],
            'tree'   => [
                'class'         => NestedSetsBehavior::class,
                'treeAttribute' => 'tree',
            ],
            'recalc' => PagesRecalcBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }
}