<?php

namespace linex\modules\pages\models\collections;

use Yii;
use linex\modules\main\components\BaseCollection;
use linex\modules\pages\models\Page;
use linex\modules\pages\Module as PagesModule;

class PageCollection extends BaseCollection
{
    public $name;
    public $slug;
    public $published;
    public $global_published;
    public $content;
    public $sort;

    /** @var Page $parent */
    public $parent;
    /** @var Page $page */
    public $page;

    public function __construct(Page $parent = null, Page $page = null, array $config = [])
    {
        if ($page)
        {
            $this->name = $page->name;
            $this->slug = $page->slug;
            $this->published = $page->published;
            $this->content = $page->content;
            $this->sort = $page->sort;
        }
        else
        {
            $this->sort = Page::DEFAULT_SORT;
            $this->published = Page::DEFAULT_PUBLISHED;
            $this->global_published = ($parent && $parent->global_published) ? 1 : 0;
        }

        $this->parent = $parent;
        $this->page = $page;

        parent::__construct($config);
    }

    public function rules()
    {
        return [
            [['slug', 'name'], 'required'],
            [['published', 'sort', 'global_published'], 'integer'],
            [['content'], 'string'],
            ['slug', 'match', 'pattern' => Page::$SLUG_PATTERN, 'message' => PagesModule::t('Slug can contain only 0-9, a-z and "-" characters (max: 60).')],
            [['slug'], 'unique', 'targetClass' => Page::class, 'filter' => $this->page ? ['<>', 'id', $this->page->id] : null],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return (new Page())->attributeLabels();
    }

    public function internalCollections()
    {
        return [];
    }
}