<?php

use yii\db\Migration;
use linex\modules\pages\models\Page;

class m170629_113857_pages extends Migration
{
    public function up()
    {
        mb_internal_encoding("UTF-8");
        $tableOptions = $this->db->driverName === 'mysql'
            ? 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
            : null;

        // Page data
        $this->createTable(
            Page::tableName(),
            [
                'id'               => $this->primaryKey(),
                'name'             => $this->string(255)->notNull(),
                'slug'             => $this->string(80)->notNull(),
                'slug_path'        => $this->string(255)->notNull()->defaultValue(''),
                'published'        => $this->smallInteger(1)->defaultValue(1),
                'created_at'       => $this->integer()->notNull(),
                'updated_at'       => $this->integer()->notNull(),
                'content'          => $this->text(),
                'sort'             => $this->integer()->defaultValue(100),
                'lft'              => $this->integer()->notNull(),
                'rgt'              => $this->integer()->notNull(),
                'depth'            => $this->integer()->notNull(),
                'tree'             => $this->integer(),
                'global_published' => $this->integer(1)->defaultValue(1),
            ],
            $tableOptions
        );

        $this->createIndex('{{%idx-pages-slug_path}}', Page::tableName(), 'slug_path');
        $this->createIndex('{{%idx-pages-published}}', Page::tableName(), 'published');
        $this->createIndex('{{%idx-pages-lft}}', Page::tableName(), ['tree', 'lft', 'rgt']);
        $this->createIndex('{{%idx-pages-rgt}}', Page::tableName(), ['tree', 'rgt']);
    }

    public function down()
    {
        echo "m170629_113857_pages cannot be reverted.\n";

        return false;
    }
}
