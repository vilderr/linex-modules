<?php

namespace linex\modules\pages\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\base\Event;
use yii\db\Query;
use linex\modules\pages\models\Page;

/**
 * Class PagesRecalcBehavior
 * @package linex\modules\pages\behaviors
 */
class PagesRecalcBehavior extends Behavior
{
    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_UPDATE  => 'onAfterUpdate',
        ];
    }

    /**
     * @param Event $event
     */
    public function onAfterUpdate(Event $event)
    {
        /** @var Page $page */
        $page = $event->sender;

        $this->recalcGlobalPublished($page);
    }

    private function recalcGlobalPublished(Page $page)
    {
        Yii::$app->db->createCommand()->update($page::tableName(), ['global_published' => 1], 'lft >= :lft AND rgt <= :rgt', [':lft' => $page->lft, ':rgt' => $page->rgt])->execute();

        $q = new Query();
        $q
            ->select([
                'lft',
                'rgt',
            ])
            ->from(Page::tableName())
            ->where('lft >= :lft', [':lft' => $page->lft])
            ->andWhere('rgt <= :rgt', [':rgt' => $page->rgt])
            ->andWhere(['published' => 0])
            ->orderBy('lft');

        $upd = [];
        $prev_rgt = 0;
        foreach ($q->each() as $child) {
            /** @var Page $child */
            if ($child['rgt'] > $prev_rgt) {
                $prev_rgt = $child['rgt'];
                $upd[] = '(lft >= ' . $child['lft'] . ' AND rgt <= ' . $child['rgt'] . ')';
            }
        }

        if (count($upd) > 0) {
            Yii::$app->db->createCommand()->update(Page::tableName(), ['global_published' => 0], implode(' OR ', $upd))->execute();
        }
    }
}