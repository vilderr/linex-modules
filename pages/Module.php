<?php

namespace linex\modules\pages;

use Yii;
use linex\modules\main\components\BaseModule;

/**
 * Class Module
 * @package linex\modules\pages
 */
class Module extends BaseModule
{
    public $version = '0.0.1';

    public function init()
    {
        parent::init();
    }

    public function bootstrap($app)
    {
        $app->i18n->translations['modules/pages/*'] = [
            'class'            => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath'         => '@vendor/linex/modules/pages/messages',
            'fileMap'          => [
                'modules/pages/module' => 'module.php',
            ],
        ];
    }

    public static function t($message, $params = [], $category = 'module', $language = null)
    {
        return Yii::t('modules/pages/' . $category, $message, $params, $language);
    }

    public function getConfig()
    {
        return [
            'bootstrap' => [
                'pages',
            ],
            'module'    => [
                'class'               => 'linex\\modules\\pages\\Module',
                'controllerNamespace' => 'linex\\modules\\pages\\controllers\\frontend',
                'layoutPath'          => '@app/views/layouts',
                'viewPath'            => '@app/views/modules/pages',
            ],
            'rules'     => [
                'backend'  => [
                    'class'       => 'yii\\web\\GroupUrlRule',
                    'prefix'      => 'dashboard',
                    'routePrefix' => 'dashboard',
                    'rules'       => [
                        'pages'                                   => 'pages/default/index',
                        'pages/<_a:(add|edit|delete|delete-all)>' => 'pages/default/<_a>',
                    ],
                ],
                'frontend' => [
                    'class' => 'linex\\modules\\pages\\components\\Rules',
                ],
            ],
        ];
    }

    public function getName()
    {
        return self::t('Module Name');
    }
}