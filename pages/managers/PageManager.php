<?php

namespace linex\modules\pages\managers;

use linex\modules\pages\models\collections\PageCollection;
use linex\modules\pages\models\Page;
use linex\modules\pages\repositories\PageRepository;

/**
 * Class PageManager
 * @package linex\modules\pages\managers
 */
final class PageManager
{
    public $page;

    /**
     * PageManager constructor.
     *
     * @param PageRepository $page
     */
    public function __construct(PageRepository $page)
    {
        $this->page = $page;
    }

    /**
     * @param PageCollection $collection
     *
     * @return Page
     */
    public function create(PageCollection $collection)
    {
        $page = Page::create(
            $collection->name,
            $collection->slug,
            $this->getSlugPath($collection),
            $collection->content,
            $collection->published,
            $collection->global_published,
            $collection->sort
        );

        if ($collection->parent) {
            $page->appendTo($collection->parent);
        } else {
            $page->makeRoot();
        }

        $this->page->save($page);

        return $page;
    }

    /**
     * @param         integer $id
     * @param PageCollection  $collection
     */
    public function edit($id, PageCollection $collection)
    {
        $page = $this->page->get($id);

        $page->edit(
            $collection->name,
            $collection->slug,
            $this->getSlugPath($collection),
            $collection->content,
            $collection->published,
            $collection->global_published,
            $collection->sort
        );

        $this->page->save($page);
    }

    /**
     * @param PageCollection $collection
     *
     * @return string
     */
    protected function getSlugPath(PageCollection $collection)
    {
        return $collection->parent ? $collection->parent->slug_path . '/' . $collection->slug : $collection->slug;
    }
}