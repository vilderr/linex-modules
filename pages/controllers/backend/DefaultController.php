<?php

namespace linex\modules\pages\controllers\backend;

use linex\modules\pages\managers\PageManager;
use linex\modules\pages\models\collections\PageCollection;
use Yii;
use yii\base\DynamicModel;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;
use yii\helpers\Url;
use yii\web\NotFoundHttpException;

use linex\modules\main\Module as MainModule;
use linex\modules\pages\Module as PagesModule;
use linex\modules\dashboard\components\DashboardController;
use linex\modules\pages\models\Page;

/**
 * Class DefaultController
 * @package linex\modules\pages\controllers\backend
 */
class DefaultController extends DashboardController
{
    private $manager;

    /**
     * DefaultController constructor.
     *
     * @param string           $id
     * @param \yii\base\Module $module
     * @param PageManager      $manager
     * @param array            $config
     */
    public function __construct($id, $module, PageManager $manager, array $config = [])
    {
        $this->manager = $manager;

        parent::__construct($id, $module, $config);
    }

    public function behaviors()
    {
        $behaviors = [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['content manage'],
                    ],
                ],
            ],
        ];

        return ArrayHelper::merge(
            parent::behaviors(),
            $behaviors
        );
    }

    /**
     * @param int $parent_id
     *
     * @return string
     */
    public function actionIndex($parent_id = 0)
    {
        $parent = null;
        if ($parent_id) {
            $parent = $this->manager->page->get($parent_id);
        } else {
            $parent = new DynamicModel([
                'id' => 0,
            ]);
        }

        $dataProvider = $this->manager->page->getProvider($parent);

        $this->view->title = PagesModule::getInstance()->getName();
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model'        => $parent,
        ]);
    }

    /**
     * @param int $parent_id
     *
     * @return string|\yii\web\Response
     * @throws ServerErrorHttpException
     */
    public function actionAdd($parent_id = null)
    {
        $parent = null;
        $parent_id = intval($parent_id);
        if ($parent_id > 0) {
            $parent = $this->manager->page->get($parent_id);
        }

        $collection = new PageCollection($parent);

        $post = Yii::$app->request->post();
        if ($collection->load($post) && $collection->validate()) {
            try {
                $page = $this->manager->create($collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'parent_id' => $parent ? $parent->id : 0]));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'back':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect(
                            Url::toRoute(
                                [
                                    'edit',
                                    'id'        => $page->id,
                                    'returnUrl' => $returnUrl,
                                ]
                            )
                        );
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        // META TAGS
        $this->view->title = PagesModule::t('Add page');
        $this->view->params['breadcrumbs'][] = [
            'label' => PagesModule::getInstance()->getName(),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('add', [
            'model' => $collection,
        ]);
    }

    public function actionEdit($id)
    {
        $post = Yii::$app->request->post();
        $page = $this->manager->page->get($id);
        $parent = $page->parent;

        $collection = new PageCollection($parent, $page);

        if ($collection->load($post) && $collection->validate()) {
            try {
                $this->manager->edit($page->id, $collection);

                $returnUrl = Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'id' => $page->id]));

                switch (Yii::$app->request->post('action', 'save')) {
                    case 'back':
                        return $this->redirect($returnUrl);
                    default:
                        return $this->redirect(
                            Url::toRoute(
                                [
                                    'edit',
                                    'id'        => $page->id,
                                    'returnUrl' => $returnUrl,
                                ]
                            )
                        );
                }
            } catch (\DomainException $e) {
                Yii::$app->errorHandler->logException($e);
                Yii::$app->session->setFlash('error', $e->getMessage());
            }
        }

        $this->view->title = PagesModule::t('Edit page');
        $this->view->params['breadcrumbs'][] = [
            'label' => PagesModule::getInstance()->getName(),
            'url'   => Url::toRoute('index'),
        ];
        $this->view->params['breadcrumbs'][] = $this->view->title;

        return $this->render('edit', [
            'model' => $collection,
        ]);
    }

    public function actionDelete($id)
    {
        $parent = null;
        try {
            $page = $this->manager->page->get($id);
            $parent = $page->parent;
            $this->manager->page->remove($page);
        } catch (\DomainException $e) {
            Yii::$app->errorHandler->logException($e);
            Yii::$app->session->setFlash('error', $e->getMessage());
        }

        return $this->redirect(Yii::$app->request->get('returnUrl', Url::toRoute(['index', 'parent_id' => $parent ? $parent->id : 0])));
    }
}