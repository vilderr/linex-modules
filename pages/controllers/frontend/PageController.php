<?php

namespace linex\modules\pages\controllers\frontend;

use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use linex\modules\pages\models\Page;

/**
 * Class PageController
 * @package linex\modules\pages\controllers\frontend
 */
class PageController extends Controller
{
    public function actionIndex($id)
    {
        /** @var $page Page */
        if (null === $page = Page::findById($id)) {
            throw new NotFoundHttpException;
        }

        $this->view->title = $page->name;

        /** @var Page $parent */
        foreach ($page->parents as $parent) {
            $this->view->params['breadcrumbs'][] = [
                'label' => $parent->name,
                'url'   => Url::to('/' . $parent->slug_path),
            ];
        }

        $this->view->params['breadcrumbs'][] = $page->name;

        return $this->render('index', [
            'model' => $page,
        ]);
    }
}