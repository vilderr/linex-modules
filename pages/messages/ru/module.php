<?php
return [
    'Module Name' => 'Страницы',

    'Add page'  => 'Добавить страницу',
    'Edit page' => 'Редактировать страницу',

    'Slug'      => 'Символьный код',
    'Slug Path' => 'Путь из символьных кодов',
    'Sort'      => 'Сортировка',
    'Published' => 'Опубликовано',
    'Content'   => 'Контент',

    'Are you sure you want to delete this item with childrens?' => 'Удалятся все внутренние страницы! Вы действительно хотите продолжить?'
];